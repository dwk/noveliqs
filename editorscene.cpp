#include "editorscene.h"
#include <QKeyEvent>
#include <QGraphicsSceneMouseEvent>
#include "editorview.h"
#include "nvli.h"

EditorScene::EditorScene(QWidget *parent) : QGraphicsScene(parent)
{
}

//void EditorScene::setup()
//{
//}

void EditorScene::addTimeline(double timelineScale)
{
	etl=new EditorTimeline(timelineScale, this);
	addItem(etl);
}

void EditorScene::clearFrags()
{
	for(auto it=editorFrags.begin(); it!=editorFrags.end(); ++it)
	{
		removeItem(it->second);
		it->second->deleteLater();
	}
	editorFrags.clear();
}

void EditorScene::addFrag(EditorFragment *ef)
{
	editorFrags.insert(EditorFragmentMISP::value_type(ef->getPk(), ef));
	addItem(ef);
}

QPointF EditorScene::getFragPos(int fragPk) const
{
	auto it=editorFrags.find(fragPk);
	if(it==editorFrags.end() || it->second==nullptr) // || it->second->getPk()<0)
		return QPointF();
	return it->second->scenePos();
}

EditorFragment *EditorScene::getFrag(int fragPk) const
{
	auto it=editorFrags.find(fragPk);
	if(it==editorFrags.end())
		return nullptr;
	return it->second;
}

void EditorScene::removeFrag(int fragPk)
{
	auto fit=editorFrags.find(fragPk);
	if(fit!=editorFrags.end())
	{
		EditorFragment * ef=fit->second;
		editorFrags.erase(fit);
		removeItem(ef);
		ef->deleteLater();
	}
}

void EditorScene::selectFrag(int fragPk, QVector<int> more)
{
	QList<QGraphicsItem *> sits=selectedItems();
	foreach(QGraphicsItem *gi, sits)
	{
		gi->setSelected(false);
	}
	auto fit=editorFrags.find(fragPk);
	if(fit!=editorFrags.end())
	{
		fit->second->blockSignals(true);
		fit->second->setSelected(true);
		fit->second->blockSignals(false);
	}
	foreach(int pk, more)
	{
		auto fit=editorFrags.find(pk);
		if(fit!=editorFrags.end())
		{
			fit->second->blockSignals(true);
			fit->second->setSelected(true);
			fit->second->blockSignals(false);
		}
	}
}

void EditorScene::updateFrag(int fragPk)
{
	EditorFragment *ef=getFrag(fragPk);
	ef->updatData();
}

void EditorScene::clearPers()
{
	for(auto it=editorPerss.begin(); it!=editorPerss.end(); ++it)
	{
		removeItem(it->second);
		it->second->deleteLater();
	}
	editorPerss.clear();
}

void EditorScene::addPers(EditorPerson *ep)
{
	editorPerss.insert(EditorPersonMISP::value_type(ep->getPk(), ep));
	addItem(ep);
	ep->setupGraphic();
}

void EditorScene::removePers(int persPk)
{
	auto pit=editorPerss.find(persPk);
	if(pit!=editorPerss.end())
	{
		EditorPerson *ep=pit->second;
		editorPerss.erase(pit);
		removeItem(ep);
		ep->deleteLater();
	}
}

void EditorScene::selectPers(int persPk)
{
	QList<QGraphicsItem *> sits=selectedItems();
	foreach(QGraphicsItem *gi, sits)
	{
		gi->setSelected(false);
	}
	auto pit=editorPerss.find(persPk);
	if(pit!=editorPerss.end())
		pit->second->setSelected(true);
}

void EditorScene::updatePers(int persPk)
{
	auto pit=editorPerss.find(persPk);
	if(pit!=editorPerss.end())
	{
		pit->second->persChangeSl(); //fastRepos();
	}
}

void EditorScene::rubberBand(const QPointF &start)
{
	rubberStart=start;
	if(start.isNull())
	{
		rubberBandCancelSl();
		return;
	}
	delete rubBand;
	rubBand=new QGraphicsLineItem();
	rubBand->setZValue(1000000.);
	addItem(rubBand);
}

QString EditorScene::scaleChanged(double timelineScale)
{
	QString res=etl->scaleChanged(timelineScale);
	for(auto fit=editorFrags.begin(); fit!=editorFrags.end(); ++fit)
	{
		if(fit->second)
		{
			fit->second->fastRepos();
		}
	}
	for(auto pit=editorPerss.begin(); pit!=editorPerss.end(); ++pit)
	{
		if(pit->second)
		{
			pit->second->fastRepos();
		}
	}
	setSceneRect(itemsBoundingRect()); // bug? otherwise the scene misses reductions in size...
	return res;
}

void EditorScene::rubberBandCancelSl()
{
	delete rubBand;
	rubBand=nullptr;
}

void EditorScene::perssVisSl(bool checked)
{
	for(auto pit=editorPerss.begin(); pit!=editorPerss.end(); ++pit)
		pit->second->setVisible(checked);
}

void EditorScene::perssDecoVisSl(bool checked)
{
	for(auto pit=editorPerss.begin(); pit!=editorPerss.end(); ++pit)
		pit->second->setDecoVisbility(checked);
}

void EditorScene::fragsVisSl(bool checked)
{
	for(auto fit=editorFrags.begin(); fit!=editorFrags.end(); ++fit)
		fit->second->setVisible(checked);
}

void EditorScene::fragsDecoVisSl(bool checked)
{
	for(auto fit=editorFrags.begin(); fit!=editorFrags.end(); ++fit)
		fit->second->setDecoVisbility(checked);
}

void EditorScene::keyPressEvent(QKeyEvent *keyEvent)
{
	if(keyEvent->key()==Qt::Key_Escape)
	{
		emit rubberBandCancelSig();
		keyEvent->accept();
	}
	QGraphicsScene::keyPressEvent(keyEvent);
}

void EditorScene::mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent)
{
	if(rubBand)
	{
		QLineF line(rubberStart, mouseEvent->scenePos());
		//qDebug()<<"updating rubber band"<<line;
		rubBand->setLine(line);
		//rubBand->update();
	}
	QGraphicsScene::mouseMoveEvent(mouseEvent);
}
