#include "datacontainer.h"
#include <QFileDialog>
#include <QProcess>
#include <QDateTime>
#include <QSqlQuery>
#include <QSqlError>
#include "nvli.h"

Q_LOGGING_CATEGORY(datacontainer, "datacontainer")

QString DataContainer::toSqlString(const QString &text, bool empty2Null)
{
	if(empty2Null && (text.isEmpty() || text.isNull()))
		return QStringLiteral("NULL");
	QString v=text;
	if(text.contains('\''))
		v.replace('\'', QStringLiteral(u"''"));
	//if(text.contains('\n'))
	//	v.replace('\n', QStringLiteral(u"<br>")); // ... rest will be taken care by Qt
	return QStringLiteral(u"'")+v+QStringLiteral(u"'");
}

QString DataContainer::toSqlString(const QDateTime &dt, bool invalid2Null)
{
	if(invalid2Null && !dt.isValid())
		return QStringLiteral("NULL");
	return QStringLiteral(u"'")+dt.toString(Qt::ISODate)+QStringLiteral(u"'");
}

QString DataContainer::toSqlString(const QDate &dt, bool invalid2Null)
{
	if(invalid2Null && !dt.isValid())
		return QStringLiteral("NULL");
	return QStringLiteral(u"'")+dt.toString(Qt::ISODate)+QStringLiteral(u"'");
}

DataContainer::DataContainer(QObject *parent) : QObject(parent)
{
	qCDebug(datacontainer)<<"DataContainer ctor";
	setObjectName(QStringLiteral("DataContainer"));
}

DataContainer::~DataContainer()
{
}

void DataContainer::connectAll(ObjectMSP &otherActors)
{
	Q_UNUSED(otherActors)
	// we get the information through method calls
}

void DataContainer::setup()
{
	Fragment::fromDb();
	Person::fromDb();
	Attend::fromDb();
}

void DataContainer::setup(MainWindow *mw)
{
	this->mw=mw;
	readPrefsSl();
	openDbSl();
}

void DataContainer::shutdown()
{
	delete dbw;
	dbw=nullptr;
}

Fragment *DataContainer::getFragment(int fragPk, bool warnIfNotFound)
{
	auto it=frags.get<FragmentPk>().find(fragPk);
	if(it!=frags.get<FragmentPk>().end())
		return *it;
	if(warnIfNotFound)
		qCritical()<<"getFragment invalid pk"<<fragPk;
	return nullptr;
}

Fragment *DataContainer::getFragment(int index, DataContainer::FragmentIndex pidx)
{
	switch(pidx)
	{
	case FragmentIndex::PK:
	{
		auto &idx=frags.get<FragmentPk>();
		auto it=idx.find(index);
		if(it==idx.end())
			return nullptr;
		else
			return *it;
		break;
	}
	case FragmentIndex::SEQ:
	{
		auto &idx=frags.get<FragmentSeq>();
		auto it=idx.find(index);
		if(it==idx.end())
			return nullptr;
		else
			return *it;
		break;
	}
	}
	return nullptr;
}

std::set<int> DataContainer::getFragmentPks() const
{
	std::set<int> res;
	auto &ind=frags.get<FragmentPk>();
	for(auto fit=ind.begin(); fit!=ind.end(); ++fit)
		res.insert((*fit)->getPk());
	return res;
}

Fragment *DataContainer::getProlog() const
{
	auto &idx=frags.get<FragmentSeq>();
	for(auto fit=idx.begin(); fit!=idx.end(); ++fit)
	{
		if((*fit)->getFunction()==Function::PROLOG)
			return *fit;
		if((*fit)->getFunction()==Function::FRAGMENT)
			break; // prolog can't follow first fragment
	}
	return nullptr;
}

Fragment *DataContainer::getEpilog() const
{
	auto &idx=frags.get<FragmentSeq>();
	for(auto fit=idx.rbegin(); fit!=idx.rend(); ++fit)
	{
		if((*fit)->getFunction()==Function::EPILOG)
			return *fit;
		if((*fit)->getFunction()==Function::FRAGMENT)
			break; // epilog can't preceed last fragment
	}
	return nullptr;
}

bool DataContainer::canBeProlog(int sequ)
{
	bool res=true;
	auto &ind=frags.get<FragmentSeq>();
	for(auto fit=ind.begin(); fit!=ind.end() && res; ++fit)
	{
		if((*fit)->getSequence()>=sequ)
			break;
		Function f=(*fit)->getFunction();
		if(f==Function::CHAPTER || f==Function::FRAGMENT || f==Function::PROLOG || f==Function::EPILOG)
			res=false; // there is at least one printable fragment before sequ
	}
	return res;
}

bool DataContainer::canBeEpilog(int sequ)
{
	bool res=true;
	auto &ind=frags.get<FragmentSeq>();
	for(auto fit=ind.rbegin(); fit!=ind.rend() && res; ++fit)
	{
		if((*fit)->getSequence()<=sequ)
			break;
		Function f=(*fit)->getFunction();
		if(f==Function::CHAPTER || f==Function::FRAGMENT || f==Function::PROLOG || f==Function::EPILOG)
			res=false; // there is at least one printable fragment after sequ
	}
	return res;
}

void DataContainer::iterateFragments(QObject *target, const char *slot, QVariant hint, DataContainer::FragmentIndex pidx)
{
	// DCON.iterateFragments(this, SLOT(tabFragFillRowSl(QObject*,int,IterateObjectHint,QVariant)), QVariant(true), DataContainer::FragmentIndex::SEQ);
	QMetaObject::Connection con=connect(this, SIGNAL(iterateSig(QObject*,int,IterateObjectHint,QVariant)), target, slot, Qt::DirectConnection);
	if(!con)
	{
		qCritical()<<"connection missed, dumping object info";
		target->dumpObjectInfo();
		const QMetaObject *mo=target->metaObject();
		for(int i = mo->methodOffset(); i < mo->methodCount(); ++i)
			qDebug() << QString::fromLatin1(mo->method(i).methodSignature());
		return;
	}
	//Fragment * prolog=(hasProlog_?(*(frags.get<FragmentSeq>().begin())):nullptr);
	//Fragment * epilog=(hasEpilog_?(*(frags.get<FragmentSeq>().rbegin())):nullptr);
	int r=0;
	switch(pidx)
	{
	case FragmentIndex::PK:
	{
		auto &idx=frags.get<FragmentPk>();
		for(auto fit=idx.begin(); fit!=idx.end(); ++fit)
		{
			//emit iterateSig(*fit, r, *fit==prolog?IterateObjectHint::PROLOG:(*fit==epilog?IterateObjectHint::EPILOG:IterateObjectHint::FRAGMENT), hint);
			emit iterateSig(*fit, r, IterateObjectHint::FRAGMENT, hint);
			++r;
		}
		break;
	}
	case FragmentIndex::SEQ:
	{
		auto &idx=frags.get<FragmentSeq>();
		for(auto fit=idx.begin(); fit!=idx.end(); ++fit)
		{
			//emit iterateSig(*fit, r, *fit==prolog?IterateObjectHint::PROLOG:(*fit==epilog?IterateObjectHint::EPILOG:IterateObjectHint::FRAGMENT), hint);
			emit iterateSig(*fit, r, IterateObjectHint::FRAGMENT, hint);
			++r;
		}
		break;
	}
	}
	disconnect(con);
}

void DataContainer::insertFragment(Fragment *frag, bool minMaxUpdate)
{
	if(!frag)
	{
		if(minMaxUpdate)
			calcMinMaxDate();
		return;
	}
	auto it=frags.get<FragmentPk>().find(frag->getPk()); // pk is stable
	bool isnew=(it==frags.get<FragmentPk>().end());
	if(isnew)
	{
		// shift all the higher sequence numbers
		int sequ=frag->getSequence();
		auto &idx=frags.get<FragmentSeq>();
		vector<Fragment*> toShift;
		for(auto sit=idx.rbegin(); sit!=idx.rend(); ++sit)
		{
			if((*sit)->getSequence()>=sequ)
				toShift.push_back((*sit));
			else
				break; // we know all frags to shift
		}
		for(int i=0, im=toShift.size(); i<im; ++i)
		{
			auto pit=frags.get<FragmentPk>().find(toShift[i]->getPk());
			frags.erase(pit);
			toShift[i]->setSequence(toShift[i]->getSequence()+1);
			toShift[i]->commit(true, false); // we handle the sequence signaling...
		}
		for(int i=0, im=toShift.size(); i<im; ++i)
		{
			frags.insert(toShift[i]);
			emit fragmentSig(toShift[i]->getPk(), FragmentAction::SEQCHANGED, QVector<int>()); // no otherPk indicates "does not chnage effective sequence"
		}
		// now insert with unique sequence
		frags.insert(frag);
		connect(frag, &Fragment::dataChangedSig, this, &DataContainer::fragChangedSl);
		emit fragmentSig(frag->getPk(), FragmentAction::CREATED, QVector<int>());
	}
	else
	{
		// somehow simpler that template<typename Modifier> bool modify(iterator position,Modifier mod)
		frags.erase(it);
		frags.insert(frag);
		// change sig was already emitted - commit is not our business!
	}
	if(minMaxUpdate)
		calcMinMaxDate();
}

void DataContainer::deleteFragment(Fragment *frag, bool deleteFromDb)
{
	auto it=frags.get<FragmentPk>().find(frag->getPk());
	if(it!=frags.get<FragmentPk>().end())
		frags.erase(it);
	set<int> px=getAttendPersPks(frag->getPk());
	for(auto pit=px.begin(); pit!=px.end(); ++pit)
	{
		deleteAttend(frag->getPk(), *pit, false); // db cascades anyway
	}
	emit fragmentSig(frag->getPk(), FragmentAction::DELETED, QVector<int>());
	if(deleteFromDb)
		frag->deleteInDb();
	delete frag;
	calcMinMaxDate();
}

void DataContainer::deleteFragment(int fragPk, bool deleteFromDb)
{
	auto it=frags.get<FragmentPk>().find(fragPk);
	if(it!=frags.get<FragmentPk>().end())
	{
		Fragment *frag=(*it);
		frags.erase(it);
		set<int> px=getAttendPersPks(fragPk);
		for(auto pit=px.begin(); pit!=px.end(); ++pit)
		{
			deleteAttend(fragPk, *pit, false); // db cascades anyway
		}
		emit fragmentSig(fragPk, FragmentAction::DELETED, QVector<int>());
		if(deleteFromDb)
			frag->deleteInDb();
		delete frag;
		calcMinMaxDate();
	}
}

void DataContainer::fragSwapSequ(int frag1Pk, int frag2Pk)
{
	auto it1=frags.get<FragmentPk>().find(frag1Pk);
	auto it2=frags.get<FragmentPk>().find(frag2Pk);
	if(it1==frags.get<FragmentPk>().end() || it2==frags.get<FragmentPk>().end())
		return;
	Fragment * f1=(*it1);
	Fragment * f2=(*it2);
	frags.erase(it1);
	frags.erase(it2);
	int sequ=f1->getSequence();
	f1->setSequence(f2->getSequence());
	f1->commit(true, false);
	f2->setSequence(sequ);
	f2->commit(true, false);
	frags.insert(f1);
	frags.insert(f2);
	emit fragmentSig(frag1Pk, FragmentAction::SEQCHANGED, QVector<int>()<<frag2Pk);
}

Person *DataContainer::getPerson(int persPk, bool warnIfNotFound)
{
	auto it=perss.get<PersonPk>().find(persPk);
	if(it!=perss.get<PersonPk>().end())
		return *it;
	if(warnIfNotFound)
		qCritical()<<"getPerson invalid pk"<<persPk;
	return nullptr;
}

std::set<int> DataContainer::getPersonPks() const
{
	std::set<int> res;
	auto &ind=perss.get<PersonPk>();
	for(auto fit=ind.begin(); fit!=ind.end(); ++fit)
		res.insert((*fit)->getPk());
	return res;
}

void DataContainer::iteratePersons(QObject *target, const char *slot, QVariant hint, DataContainer::PersonIndex pidx)
{
	QMetaObject::Connection con=connect(this, SIGNAL(iterateSig(QObject*,int,IterateObjectHint,QVariant)), target, slot, Qt::DirectConnection);
	int r=0;
	switch(pidx)
	{
	case PersonIndex::PK:
	{
		auto &idx=perss.get<PersonPk>();
		for(auto fit=idx.begin(); fit!=idx.end(); ++fit)
		{
			emit iterateSig(*fit, r, IterateObjectHint::PERSON, hint);
			++r;
		}
		break;
	}
	case PersonIndex::NAME:
	{
		auto &idx=perss.get<PersonName>();
		for(auto fit=idx.begin(); fit!=idx.end(); ++fit)
		{
			emit iterateSig(*fit, r, IterateObjectHint::PERSON, hint);
			++r;
		}
		break;
	}
	}
	disconnect(con);
}

void DataContainer::insertPerson(Person *pers)
{
	auto it=perss.get<PersonPk>().find(pers->getPk()); // pk is stable
	bool isnew=(it==perss.get<PersonPk>().end());
	if(isnew)
	{
		perss.insert(pers);
		connect(pers, &Person::dataChangedSig, this, &DataContainer::persChangedSl);
		emit personSig(pers->getPk(), PersonAction::CREATED, -1);
	}
	else
	{
		// somehow simpler that template<typename Modifier> bool modify(iterator position,Modifier mod)
		perss.erase(it); // somehow simpler that template<typename Modifier> bool modify(iterator position,Modifier mod)
		perss.insert(pers);
		// change sig was emitted during commit
	}
}

void DataContainer::deletePerson(Person *pers, bool deleteFromDb)
{
	auto it=perss.get<PersonPk>().find(pers->getPk());
	if(it!=perss.get<PersonPk>().end())
		perss.erase(it);
	set<int> fx=getAttendFragPks(pers->getPk());
	for(auto fit=fx.begin(); fit!=fx.end(); ++fit)
	{
		deleteAttend(*fit, pers->getPk(), false); // db cascades anyway
	}
	emit personSig(pers->getPk(), PersonAction::DELETED, -1);
	if(deleteFromDb)
		pers->deleteInDb();
	delete pers;
}

void DataContainer::deletePerson(int persPk, bool deleteFromDb)
{
	auto it=perss.get<PersonPk>().find(persPk);
	if(it!=perss.get<PersonPk>().end())
	{
		Person *pers=(*it);
		perss.erase(it);
		set<int> fx=getAttendFragPks(persPk);
		for(auto fit=fx.begin(); fit!=fx.end(); ++fit)
		{
			deleteAttend(*fit, persPk, false); // db cascades anyway
		}
		emit personSig(persPk, PersonAction::DELETED, -1);
		if(deleteFromDb)
			pers->deleteInDb();
		delete pers;
	}
}

Attend *DataContainer::getAttend(int fragPk, int persPk)
{
	auto &idx=atts.get<FragmentPk>();
	auto range=idx.equal_range(fragPk);
	for(auto it=range.first; it!=range.second; ++it)
	{
		if((*it)->getPersPk()==persPk)
			return (*it);
	}
	//qCritical()<<"getAttend invalid pk"<<fragPk;
	return nullptr;
}

std::set<int> DataContainer::getAttendPersPks(int fragPk) const
{
	std::set<int> res;
	auto &idx=atts.get<FragmentPk>();
	auto range=idx.equal_range(fragPk);
	for(auto it=range.first; it!=range.second; ++it)
	{
		res.insert((*it)->getPersPk());
	}
	return res;
}

std::set<int> DataContainer::getAttendFragPks(int persPk) const
{
	std::set<int> res;
	auto &idx=atts.get<PersonPk>();
	auto range=idx.equal_range(persPk);
	for(auto it=range.first; it!=range.second; ++it)
	{
		res.insert((*it)->getFragPk());
	}
	return res;
}

void DataContainer::insertAttend(Attend *att)
{
	atts.insert(att);
	emit attendSig(att->getPk(), AttendAction::CREATED, att->getFragPk(), att->getPersPk());
}

void DataContainer::deleteAttend(Attend *att, bool deleteFromDb)
{
	if(!att)
		return;
	auto it=atts.get<AttendPk>().find(att->getPk());
	if(it!=atts.get<AttendPk>().end())
		atts.erase(it);
	emit attendSig(att->getPk(), AttendAction::DELETED, att->getFragPk(), att->getPersPk());
	if(deleteFromDb)
		att->deleteInDb();
	delete att;
}

bool DataContainer::deleteAttend(int fragPk, int persPk, bool deleteFromDb)
{
	Attend *att=getAttend(fragPk, persPk);
	if(att)
	{
		deleteAttend(att, deleteFromDb);
		return true;
	}
	return false;
}

void DataContainer::setBookAuthor(const QString &author)
{
	if(bookAuthor==author)
		return;
	bookAuthor=author;
	if(!dbw)
		return;
	QSqlQuery q(dbw->db());
	q.exec(QString("update settings set value=%1 where key='author'").arg(DataContainer::toSqlString(bookAuthor)));
	q.finish();
}

void DataContainer::setBookTitle(const QString &title)
{
	if(bookTitle==title)
		return;
	bookTitle=title;
	if(!dbw)
		return;
	QSqlQuery q(dbw->db());
	q.exec(QString("update settings set value=%1 where key='title'").arg(DataContainer::toSqlString(bookTitle)));
	q.finish();
}

void DataContainer::setPaths(const QString &fragP, const QString &bioP, const QString &genP, const QString &outP)
{
	if(!dbw)
		return;
	QSqlQuery q(dbw->db());
	q.exec(QString("update settings set value=%1 where key='fragPath'").arg(DataContainer::toSqlString(fragP)));
	q.finish();
	fragPath=toAbsPathDir(fragP);
	if(q.lastError().isValid())
		qCritical()<<"FAILED settings update"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
	q.exec(QString("update settings set value=%1 where key='bioPath'").arg(DataContainer::toSqlString(bioP)));
	q.finish();
	bioPath=toAbsPathDir(bioP);
	if(q.lastError().isValid())
		qCritical()<<"FAILED settings update"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
	q.exec(QString("update settings set value=%1 where key='generatorPath'").arg(DataContainer::toSqlString(genP)));
	q.finish();
	generatorPath=toAbsPathDir(genP);
	if(q.lastError().isValid())
		qCritical()<<"FAILED settings update"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
	q.exec(QString("update settings set value=%1 where key='outPath'").arg(DataContainer::toSqlString(outP)));
	q.finish();
	outPath=toAbsPathDir(outP);
	if(q.lastError().isValid())
		qCritical()<<"FAILED settings update"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
}

void DataContainer::readPrefsSl()
{
	QSettings settings;
	cmdFullEdit=settings.value(QStringLiteral("pref/cmdFullEdit"), QStringLiteral("gedit")).toString();
	cmdLatex=settings.value(QStringLiteral("pref/cmdLatex"), QStringLiteral("pdflatex")).toString();
	cmdPdfViewer=settings.value(QStringLiteral("pref/cmdPdfViewer"), QStringLiteral("xdg-open")).toString();
}

void DataContainer::openDbSl()
{
	delete dbw;
	dbw=nullptr;
	auto &aidx=atts.get<AttendPk>();
	for(auto ia=aidx.begin(); ia!=aidx.end(); ++ia)
		delete *ia;
	atts.clear();
	emit attendSig(-1, AttendAction::ALLGONE, -1, -1);
	auto &fidx=frags.get<FragmentPk>();
	for(auto iF=fidx.begin(); iF!=fidx.end(); ++iF)
		delete *iF;
	frags.clear();
	emit fragmentSig(-1, FragmentAction::ALLGONE, QVector<int>());
	auto &pidx=perss.get<PersonPk>();
	for(auto ip=pidx.begin(); ip!=pidx.end(); ++ip)
		delete *ip;
	perss.clear();
	emit personSig(-1, PersonAction::ALLGONE, -1);
	QSettings settings;
	do
	{
		databaseName=settings.value(QStringLiteral("pref/databaseName"), QStringLiteral("~/noveliqs")).toString();
		QFileInfo basePath(databaseName);
		if(basePath.exists())
		{
			dbw=new DbWrapper(databaseName, this);
			if(!dbw->isOpen())
			{
				if(QMessageBox::critical(mw, tr("Cannot Open Book"), tr("Cannot open book because of %1. Select different file or quit?").arg(dbw->lastError()),
										 QMessageBox::Retry | QMessageBox::Abort)==QMessageBox::Abort)
				{
					QCoreApplication::exit();
					return;
				}
				delete dbw;
				dbw=nullptr;
				QString fn=QFileDialog::getSaveFileName(mw, QStringLiteral("Create New or Open Existing Book"), databaseName,
														QStringLiteral("SQLIte Database (*.db)"), nullptr, QFileDialog::DontConfirmOverwrite);
				if(!fn.isEmpty())
					settings.setValue(QStringLiteral("pref/databaseName"), fn);
			}
		}
		else
		{
			auto res=QMessageBox::question(mw, tr("Creating new Book"), tr("Create a new book database %1?").arg(databaseName),
									 QMessageBox::Yes | QMessageBox::Retry | QMessageBox::Abort);
			if(res==QMessageBox::Retry)
			{
				QString fn=QFileDialog::getSaveFileName(mw, QStringLiteral("Create New or Open Existing Book"), databaseName,
														QStringLiteral("SQLIte Database (*.db)"), nullptr, QFileDialog::DontConfirmOverwrite);
				if(!fn.isEmpty())
					settings.setValue(QStringLiteral("pref/databaseName"), fn);
			}
			else if(res==QMessageBox::Abort)
			{
				QCoreApplication::exit();
				return;
			}
			else
			{ // create new
				QString sqlfn=QDir::tempPath()+QStringLiteral("/NoveliqsSQLCreate%1.sql").arg(QDateTime::currentDateTime().toString(QStringLiteral("yyyyMMddhhmmss")));
				QFile::copy(QStringLiteral(":/newdb.sql"), sqlfn);
				QProcess p(this); // something like sqlite3 testDB.db < newdb.sql
				p.setStandardInputFile(sqlfn);
				p.start(QStringLiteral("sqlite3"), QStringList()<<databaseName);
				if(!p.waitForFinished(5000))
				{
					qCritical()<<"DCON::on_pushNewDb_clicked creation failed"<<p.error();
					QMessageBox::critical(mw, tr("Creation Failed"), tr("Cannot create database because %1.").arg(p.error()),
											 QMessageBox::Abort);
					QCoreApplication::exit();
					return;
				}
				qInfo()<<"Database created"<<databaseName;
				dbw=new DbWrapper(databaseName, this);
				if(!dbw->isOpen())
				{
					QMessageBox::critical(mw, tr("Creation Failed"), tr("Cannot create book because of %1.").arg(dbw->lastError()),
											 QMessageBox::Abort);
					dbw=nullptr;
					QCoreApplication::exit();
					return;
				}
			}
		}
	}
	while(!dbw);
	QSqlQuery q(QStringLiteral("select key, value from settings"), DCON.db()->db());
	while(q.next())
	{
		QString key=q.value(0).toString();
		QString val=q.value(1).toString();
		if(key==QStringLiteral("fragPath"))
		{
			fragPath=toAbsPathDir(val);
		}
		else if(key==QStringLiteral("bioPath"))
		{
			bioPath=toAbsPathDir(val);
		}
		else if(key==QStringLiteral("generatorPath"))
		{
			generatorPath=toAbsPathDir(val);
		}
		else if(key==QStringLiteral("outPath"))
		{
			outPath=toAbsPathDir(val);
		}
		else if(key==QStringLiteral("title"))
		{
			bookTitle=val;
		}
		else if(key==QStringLiteral("author"))
		{
			bookAuthor=val;
		}
		if(fragPath.isEmpty() || bioPath.isEmpty() || generatorPath.isEmpty() || outPath.isEmpty())
			qCritical()<<"corrupted paths";
	}
	qCDebug(datacontainer)<<"paths"<<fragPath<<bioPath<<generatorPath<<outPath;
}

void DataContainer::calcMinMaxDate()
{
	auto it=frags.get<FragmentSeq>().begin();
	auto ex=frags.get<FragmentSeq>().end();
	Fragment *chapter=nullptr;
	while(it!=ex)
	{
		if((*it)->getFunction()==Function::CHAPTER)
			chapter=(*it);
		if((*it)->getFunction()==Function::FRAGMENT)
			break;
		++it;
	}
	QDateTime newStart, newEnd, newChapStart, newChapEnd;
	if(it==ex || !(*it))
	{
		newStart=QDateTime::currentDateTime();
		newEnd=storyStart.addDays(1);
	}
	else
	{
		newStart=(*it)->getStart();
		newChapStart=newStart;
		newEnd=(*it)->getEnd();
		newChapEnd=newEnd;
		++it;
		for(; it!=ex; ++it)
		{
			Fragment * f=(*it);
			if(!f)
				continue;
			if(f->getFunction()==Function::CHAPTER)
			{
				if(chapter)
				{
					chapter->setStart(newChapStart);
					chapter->setEnd(newChapEnd);
					chapter->commit();
				}
				chapter=f;
				newChapStart=QDateTime();
				continue;
			}
			if(f->getFunction()!=Function::FRAGMENT)
				continue;
			if(f->getStart()<newStart)
				newStart=f->getStart();
			if(f->getEnd()>newEnd)
				newEnd=f->getEnd();
			if(chapter)
			{
				if(newChapStart.isValid())
				{
					if(f->getStart()<newChapStart)
						newChapStart=f->getStart();
					if(f->getEnd()>newChapEnd)
						newChapEnd=f->getEnd();
				}
				else
				{
					newChapStart=f->getStart();
					newChapEnd=f->getEnd();
				}
			}
		}
	}
	if(chapter && newChapStart.isValid())
	{
		chapter->setStart(newChapStart);
		chapter->setEnd(newChapEnd);
		chapter->commit();
	}
	if(newStart!=storyStart || newEnd!=storyEnd)
	{
		storyStart=newStart;
		storyEnd=newEnd;
		emit minMaxDateSig(storyStart, storyEnd);
	}
}

QDir DataContainer::toAbsPathDir(const QString &val)
{
	QDir res(val);
	if(res.isRelative())
	{
		QFileInfo basePath(databaseName);
		return QDir(QDir::cleanPath(basePath.dir().absolutePath()+"/"+val));
	}
	return res;
}

void DataContainer::fragChangedSl(int pk, FragmentAction action)
{
	if(action==FragmentAction::CHANGED || action==FragmentAction::SEQCHANGED)
		emit fragmentSig(pk, action, QVector<int>()); // delete handeled directly, other for SEQCHANGED unknown
}

void DataContainer::persChangedSl(int pk, PersonAction action)
{
	if(action==PersonAction::CHANGED)
		emit personSig(pk, action, -1);
}
