#ifndef IFACTOR_H
#define IFACTOR_H

#include "nvlh.h"
#include <QObject>
#include <QMetaObject>

class IfActor
{
public:
	static QByteArray fragmentSigNorm;
	static QByteArray fragmentSlNorm;
	static QByteArray personSigNorm;
	static QByteArray personSlNorm;
	static QByteArray attendSigNorm; // attPk, action, fragPk, persPk
	static QByteArray attendSlNorm;
	static QByteArray minMaxDateSigNorm;
	static QByteArray minMaxDateSlNorm;
	static QByteArray refDateSigNorm;
	static QByteArray refDateSlNorm;
	static bool connectX(QObject *sender, const QByteArray &signal, QObject *actor, const QByteArray &slot);
	virtual void connectAll(ObjectMSP & otherActors)=0;
	virtual void setup()=0;
};
typedef std::map< QString, IfActor * > IfActorMSP;

#endif // IFACTOR_H
