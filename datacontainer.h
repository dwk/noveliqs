#ifndef DATACONTAINER_H
#define DATACONTAINER_H

#include "nvlh.h"
#include <QObject>
#include "ifactor.h"
#include <QMutex>
#include <QDir>
#include <boost/multi_index_container.hpp>
#include <boost/multi_index/ordered_index.hpp>
#include <boost/multi_index/mem_fun.hpp>
#include <boost/multi_index/member.hpp>
#include "fragment.h"
#include "person.h"
#include "attend.h"
#include "mainwindow.h"
#include "dbwrapper.h"

Q_DECLARE_LOGGING_CATEGORY(datacontainer)

class QListWidget;

class DataContainer : public QObject, public IfActor
{
	Q_OBJECT
public:
	enum class PersonIndex{PK, NAME};
	enum class FragmentIndex{PK, SEQ};
	static QString toSqlString(const QString &text, bool empty2Null = false);
	static QString toSqlString(const QDateTime &dt, bool invalid2Null = false);
	static QString toSqlString(const QDate &dt, bool invalid2Null = false);
	explicit DataContainer(QObject *parent = nullptr);
	~DataContainer();
	DataContainer *self() {return this;}
	// IfActor
	virtual void connectAll(ObjectMSP &otherActors) override;
	virtual void setup() override;
	//
	MainWindow *getMainDlg() const {return mw;}
	void setup(MainWindow *mw);
	void shutdown();
	DbWrapper * db() {return dbw;}
	const QDateTime & getStoryStart() const {return storyStart;}
	const QDateTime & getStoryEnd() const {return storyEnd;}
	//bool hasProlog() const {return hasProlog_;}
	//void  setProlog(bool hasProlog);
	//bool hasEpilog() const {return hasEpilog_;}
	//void  setEpilog(bool hasEpilog);
	// fragments -----------------
	int getFragCnt() const {return frags.size();}
	Fragment * getFragment(int fragPk, bool warnIfNotFound = true);
	Fragment * getFragment(int index, FragmentIndex pidx);
	std::set<int> getFragmentPks() const;
	Fragment * getProlog() const;
	Fragment * getEpilog() const;
	bool canBeProlog(int sequ);
	bool canBeEpilog(int sequ);
	void iterateFragments(QObject *target, const char *slot, QVariant hint, FragmentIndex pidx);
	void insertFragment(Fragment *frag, bool minMaxUpdate = true);
	void deleteFragment(Fragment *frag, bool deleteFromDb = false);
	void deleteFragment(int fragPk, bool deleteFromDb = false);
	void fragSwapSequ(int frag1Pk, int frag2Pk);
	// persons -------------------
	int getPersonCnt() const {return perss.size();}
	Person * getPerson(int persPk, bool warnIfNotFound = true);
	std::set<int> getPersonPks() const;
	void iteratePersons(QObject *target, const char *slot, QVariant hint, PersonIndex pidx);
	void insertPerson(Person *pers);
	void deletePerson(Person *pers, bool deleteFromDb = false);
	void deletePerson(int persPk, bool deleteFromDb = false);
	// attends -------------------
	Attend * getAttend(int fragPk, int persPk);
	std::set<int> getAttendPersPks(int fragPk) const;
	std::set<int> getAttendFragPks(int persPk) const;
	void insertAttend(Attend *att);
	void deleteAttend(Attend *att, bool deleteFromDb = false);
	bool deleteAttend(int fragPk, int persPk, bool deleteFromDb = false); // false if no such attend found
	// settings ------------
	const QString & getBookAuthor() const {return bookAuthor;}
	void setBookAuthor(const QString &author);
	const QString & getBookTitle() const {return bookTitle;}
	void setBookTitle(const QString &title);
	QDir getFragmentDir() const {return fragPath;}
	QDir getBiographyDir() const {return bioPath;}
	QDir getGeneratorDir() const {return generatorPath;}
	QDir getOutputDir() const {return outPath;}
	void setPaths(const QString &fragP, const QString &bioP, const QString &genP, const QString &outP);
	// preferences ------------
	QString getCmdFullEdit() const {return cmdFullEdit;}
	QString getCmdLatex() const {return cmdLatex;}
	QString getCmdPdfViewer() const {return cmdPdfViewer;}
public slots:
	void readPrefsSl();
	void openDbSl();
signals:
	void iterateSig(QObject *obj, int index, IterateObjectHint objectHint, QVariant hint);
	void minMaxDateSig(const QDateTime &minDate, const QDateTime &maxDate); // IfActor
	void fragmentSig(int fragPk, FragmentAction action, QVector<int> pkOther); // IfActor
	void personSig(int persPk, PersonAction action, int pkOther); // IfActor
	void attendSig(int attPk, AttendAction action, int fragPk, int persPk); // IfActor
private:
	struct PersonPk{};
	struct PersonName{};
	typedef boost::multi_index::multi_index_container< QPointer<Person>, boost::multi_index::indexed_by<
		boost::multi_index::ordered_unique< boost::multi_index::tag<PersonPk>, boost::multi_index::member< Person, int, &Person::pk> >,
		boost::multi_index::ordered_non_unique< boost::multi_index::tag<PersonName>, boost::multi_index::member< Person, QString, &Person::last> >
		>
	> Persons;
	struct FragmentPk{};
	struct FragmentSeq{}; // should be strict now! allow (temporarily) non-unique sequence for easy re-ordering and insert
	typedef boost::multi_index::multi_index_container< QPointer<Fragment>, boost::multi_index::indexed_by<
		boost::multi_index::ordered_unique< boost::multi_index::tag<FragmentPk>, boost::multi_index::member< Fragment, int, &Fragment::pk> >,
		boost::multi_index::ordered_unique< boost::multi_index::tag<FragmentSeq>, boost::multi_index::member< Fragment, int, &Fragment::seq> >
		>
	> Fragments;
	struct AttendPk{};
	typedef boost::multi_index::multi_index_container< QPointer<Attend>, boost::multi_index::indexed_by<
		boost::multi_index::ordered_unique< boost::multi_index::tag<AttendPk>, boost::multi_index::member< Attend, int, &Attend::pk> >,
		boost::multi_index::ordered_non_unique< boost::multi_index::tag<FragmentPk>, boost::multi_index::member< Attend, int, &Attend::fragPk> >,
		boost::multi_index::ordered_non_unique< boost::multi_index::tag<PersonPk>, boost::multi_index::member< Attend, int, &Attend::persPk> >
		>
	> Attends;
	void calcMinMaxDate();
	QDir toAbsPathDir(const QString &val);
private slots:
	void fragChangedSl(int pk, FragmentAction action);
	void persChangedSl(int pk, PersonAction action);
private:
	DbWrapper *dbw=nullptr;
	MainWindow *mw=nullptr;
	//bool hasProlog_=false, hasEpilog_=false;
	Fragments frags;
	QDateTime storyStart, storyEnd;
	Persons perss;
	Attends atts;
	QString databaseName, bookAuthor, bookTitle, cmdFullEdit, cmdLatex, cmdPdfViewer;
	QDir fragPath, bioPath, generatorPath, outPath;
};

#endif // DATACONTAINER_H
