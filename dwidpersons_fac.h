#ifndef DWIDPERSONS_FAC_H
#define DWIDPERSONS_FAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidPersons(parent);
}
const char *techname="DWidPersons";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Persons", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDPERSONS_FAC_H
