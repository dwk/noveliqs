#include "editorfragment.h"
#include <QPainter>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include "qmath.h"
#include "fragment.h"
#include "dlgfragment.h"
#include "dwidtimeline.h"
#include "nvli.h"

Q_LOGGING_CATEGORY(editorfragment, "editorfragment")

EditorFragment::EditorFragment(int fragPk, QObject * parent) :
	QObject(parent), baseColor(100,200,80), normalBrush(baseColor.lighter(100)), overBrush(baseColor.lighter(150)),
	selPen(QColor(255,80,40)), normalPen(baseColor.darker()), overselPen(QColor(255,230,80)), overPen(baseColor)
{
	selPen.setWidth(5);
	normalPen.setWidth(3);
	overselPen.setWidth(5);
	overPen.setWidth(3);
	f=DCON.getFragment(fragPk);
	peopleCnt=DCON.getAttendPersPks(fragPk).size();
	if(!f)
	{
		qCritical()<<"EditorObject no fragment"<<fragPk;
		return;
	}
	funct=f->getFunction();
	if(funct==Function::CHAPTER)
	{
		setFlags(ItemIsSelectable | ItemSendsGeometryChanges);
		setAcceptHoverEvents(false);
		normalPen=QPen(Qt::black);
		normalPen.setWidth(9);
	}
	else
	{
		setFlags(ItemIsSelectable | ItemIsMovable | ItemSendsGeometryChanges);
		setAcceptHoverEvents(true);
	}
	setZValue(0.);
	setupGraphic();
	el=new EditorLabel(f->getTitle(), DCON.getMainDlg()->font(), funct==Function::CHAPTER?EditorLabel::Alignment::TOP:EditorLabel::Alignment::VCENT, this);
}

EditorFragment::~EditorFragment()
{
}

QRectF EditorFragment::boundingRect() const
{
	return boundingBox;
}

QPainterPath EditorFragment::shape() const
{
	QPainterPath path;
	path.addPath(body);
	path.addRect(peopleBox);
	return path;
}

void EditorFragment::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget);
	if(!f)
		return;
	painter->save();
	painter->setPen((option->state & QStyle::State_Selected)?\
						((option->state & QStyle::State_MouseOver)?overselPen:selPen):\
						((option->state & QStyle::State_MouseOver)?overPen:normalPen));
	if(funct==Function::FRAGMENT)
	{
		painter->setBrush(QBrush());
		painter->drawRoundedRect(peopleBox, objectRenderUnit/2, objectRenderUnit/2);
		for(int i=0; i<peopleCnt; ++i)
			painter->drawEllipse(QPointF(0., objectRenderUnit*i), objectRenderUnit/4, objectRenderUnit/4);
	}
	painter->setBrush((option->state & QStyle::State_MouseOver)?overBrush:normalBrush);
	painter->drawPath(body);
	painter->restore();
}

int EditorFragment::getPk() const
{
	if(f)
		return f->getPk();
	return -1;
}

void EditorFragment::fastRepos()
{
	setupGraphic(); // no shortcut right now, need new pos and size
	//qDebug()<<"EditorFragment::fastRepos"<<pos()<<boundingRect()<<f->getTitle();
}

void EditorFragment::updatData()
{
	if(!f)
		return;
	//qDebug()<<(f?f->getPk():0)<<pk;
	peopleCnt=DCON.getAttendPersPks(f->getPk()).size();
	setupGraphic();
	el->setText(f->getTitle());
	update();
}

void EditorFragment::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	if(!f)
		return;
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QAction * act=menu->addAction(QIcon(":/res/star.png"), QStringLiteral("Edit"));
	connect(act, &QAction::triggered, this, &EditorFragment::editObjSl);
	act=menu->addAction(QIcon(":/res/cross_red.png"), tr("Delete"));
	act->setEnabled(false);
	// todo delete?
	//connect(act, &QAction::triggered, ce, [ce, this](){ce->deleteObjSl(id);});
	menu->popup(event->screenPos());
}

QVariant EditorFragment::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change==ItemPositionHasChanged)
	{
		wasMoved=true;
		//qDebug()<<"EditorObject::itemChange"<<value.toPointF();
	}
	else if(change==ItemSelectedChange)
	{
		//qDebug()<<"EditorFragment::itemChange"<<change<<value.toBool()<<static_cast<QGraphicsItem*>(this);
		emit selectedSig(f?f->getPk():-1, value.toBool()); // deselected ignored in DWidTimeline
	}
	return QGraphicsItem::itemChange(change, value);
}

void EditorFragment::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj dblclk at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseDoubleClickEvent(event);
	if(event->button()==Qt::LeftButton)
		editObjSl();
}

void EditorFragment::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj press at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mousePressEvent(event);
	wasMoved=false;
}

void EditorFragment::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj release at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseReleaseEvent(event);
	if(wasMoved)
	{
		wasMoved=false;
		if(!f)
			return;
		double ncy=scenePos().y();
		setPos(getDWid()->datetime2Coord(f->getStart()), ncy);
		f->setEditorhint("cy", QString::number(ncy));
		f->commit();
	}
}

void EditorFragment::setupGraphic()
{
	if(!f)
		return;
	double stc=getDWid()->datetime2Coord(f->getStart());
	prepareGeometryChange();
	setPos(stc, funct==Function::CHAPTER?10.:f->getEditorhint("cy").toDouble());
	body.clear();
	if(funct==Function::CHAPTER)
	{
		const int hei=3.;
		double xe=getDWid()->datetime2Coord(f->getEnd())-stc;
		body.moveTo(0., -hei);
		body.lineTo(0., hei);
		body.moveTo(0., 0.);
		body.lineTo(xe, 0.);
		body.moveTo(xe, -hei);
		body.lineTo(xe, hei);
		boundingBox=body.boundingRect();
	}
	else
	{
		peopleBox=peopleCnt?QRectF(-objectRenderUnit/2, -objectRenderUnit/2, objectRenderUnit, objectRenderUnit*peopleCnt):QRectF();
		body.addRect(QRectF(0., -objectRenderUnit, getDWid()->datetime2Coord(f->getEnd())-stc, objectRenderUnit*2));
		boundingBox=body.boundingRect().united(peopleBox);
	}
	boundingBox.adjust(-objectRenderUnit/4.,-objectRenderUnit/4.,objectRenderUnit/4.,objectRenderUnit/4.);
	setToolTip(f->getTitle());
}

void EditorFragment::editObjSl()
{
	if(!f)
		return;
	DlgFragment dlg(*f, true, DCON.getMainDlg());
	dlg.exec();
}

DWidTimeline *EditorFragment::getDWid() const
{
	return qobject_cast<DWidTimeline *>(parent()->parent()); // EditorFragment -> EditorScene -> DWidTimeline
}
