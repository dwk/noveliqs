#ifndef DWIDTIMELINE_H
#define DWIDTIMELINE_H

#include "nvlh.h"
#include <QWidget>
#include "ifactor.h"
#include "editorscene.h"

Q_DECLARE_LOGGING_CATEGORY(dwidtimeline)

class QTreeWidgetItem;
namespace Ui {
class DWidTimeline;
}

class DWidTimeline : public QWidget, public IfActor
{
	Q_OBJECT
public:
	explicit DWidTimeline(QWidget *parent = 0);
	~DWidTimeline();
	double datetime2Coord(const QDateTime & dt) const;
	double datetime2Coord(const QDate & dt) const;
	QDate coord2Date(double t); // returns the day which contains t
	QDateTime coord2DateTime(double t);
	//ZoomLevel getZoomLevel() const {return zoom;}
	// IfActor
	virtual void connectAll(ObjectMSP &otherActors) override;
	virtual void setup() override;
	//
private slots:
	// IfActor
	void fragmentSl(int pk, FragmentAction action, QVector<int> pkOther); // IfActor
	void personSl(int pk, PersonAction action, int pkOther); // IfActor
	void attendSl(int attPk, AttendAction action, int fragPk, int persPk); // IfActor
	void minMaxDateSl(const QDateTime & start, const QDateTime &end); // IfActor
	void refDateSl(const QDateTime &refDate);
	//
	void timelineZoomChangedSl(int zoomDelta, int baseWidth);
	void fragSelectedSl(int fragPk, bool selected);
	void persSelectedSl(int persPk, bool selected);
	void on_toolZoomIn_clicked();
	void on_toolZoomRes_clicked();
	void on_toolZoomOut_clicked();
private:
	Ui::DWidTimeline *ui;
	EditorScene *scene;
	bool freezeSigs=false;
	double timelineScale=.1; // days_after_start * timelineScale = pixels
	//ZoomLevel zoom=ZoomLevel::DAYS;
signals:
	void fragmentSig(int pk, FragmentAction action, QVector<int> pkOther); // IfActor
	void personSig(int pk, PersonAction action, int pkOther); // IfActor
};

#endif // DWIDTIMELINE_H
