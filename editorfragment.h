#ifndef EDITORFRAGMENT_H
#define EDITORFRAGMENT_H
#include "nvlh.h"
#include <QPointer>
#include <QGraphicsItem>
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QFont>
#include <QDateTime>
#include "editorobject.h"

Q_DECLARE_LOGGING_CATEGORY(editorfragment)

class DWidTimeline;

class EditorFragment : public QObject, public QGraphicsItem
{
	Q_OBJECT
	Q_INTERFACES(QGraphicsItem)
public:
	static const int objectRenderUnit=20;
	EditorFragment(int fragPk, QObject *parent);
	virtual ~EditorFragment();
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE;
	virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	//
	int getPk() const;
	Fragment * getFrag() const {return f;}
	int getRenderUnit() const {return objectRenderUnit;}
	void fastRepos();
	void setDecoVisbility(bool visible) {if(el) el->setVisible(visible);}
	void updatData();
protected:
	// QGraphicsItem
	virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) Q_DECL_OVERRIDE;
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;
	virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	//
	virtual void setupGraphic();
private slots:
	void editObjSl();
private:
	DWidTimeline * getDWid() const;
	// graphic representation
	QColor baseColor;
	QBrush normalBrush, overBrush;
	QPen selPen, normalPen, overselPen, overPen;
	int peopleCnt=3;
	QRectF boundingBox, peopleBox;
	QPainterPath body;
	bool wasMoved=false;
	//
	EditorLabel *el;
	Fragment *f=nullptr;
	Function funct=Function::COMMENT;
signals:
	void selectedSig(int fragPk, bool selected);
	//void connectionReposSig(void * node);
};
typedef std::map<int, QPointer<EditorFragment> > EditorFragmentMISP; // key is pk

#endif // EDITOROBJECT_H
