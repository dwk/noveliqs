#include "dlgprefabout.h"
#include "ui_dlgprefabout.h"
#include <QFileDialog>
#include <QProcess>
#include "nvli.h"

Q_LOGGING_CATEGORY(dlgprefabout, "dlgprefabout")

DlgPrefAbout::DlgPrefAbout(QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgPrefAbout)
{
	ui->setupUi(this);
	QSettings settings;
	ui->labelVersion->setText(VERSION_STR);
	ui->linedb->setText(DCON.db()->getFilename());
	ui->lineTitle->setText(DCON.getBookTitle());
	ui->lineAuthor->setText(DCON.getBookAuthor());
	ui->lineFragPath->setText(toRelPath(DCON.getFragmentDir().absolutePath()));
	ui->lineBioPath->setText(toRelPath(DCON.getBiographyDir().absolutePath()));
	ui->lineGeneratorPath->setText(toRelPath(DCON.getGeneratorDir().absolutePath()));
	ui->lineOutputPath->setText(toRelPath(DCON.getOutputDir().absolutePath()));
	//ui->checkProlog->setChecked(DCON.hasProlog());
	//ui->checkEpilog->setChecked(DCON.hasEpilog());
	ui->lineCmdFullEdit->setText(settings.value(QStringLiteral("pref/cmdFullEdit"), QStringLiteral("gedit")).toString());
	ui->lineCmdLatex->setText(settings.value(QStringLiteral("pref/cmdLatex"), QStringLiteral("pdflatex")).toString());
	ui->lineCmdPdfViewer->setText(settings.value(QStringLiteral("pref/cmdPdfViewer"), QStringLiteral("xdg-open")).toString());
}

DlgPrefAbout::~DlgPrefAbout()
{
	delete ui;
}

void DlgPrefAbout::accept()
{
	QSettings settings;
	settings.setValue(QStringLiteral("pref/cmdFullEdit"), ui->lineCmdFullEdit->text());
	settings.setValue(QStringLiteral("pref/cmdLatex"), ui->lineCmdLatex->text());
	settings.setValue(QStringLiteral("pref/cmdPdfViewer"), ui->lineCmdPdfViewer->text());
	DCON.readPrefsSl(); // no matter if anything changed or not - it's cheap
	DCON.setBookTitle(ui->lineTitle->text());
	DCON.setBookAuthor(ui->lineAuthor->text());
	if(pathsChanged)
		DCON.setPaths(ui->lineFragPath->text(), ui->lineBioPath->text(), ui->lineGeneratorPath->text(), ui->lineOutputPath->text());
	QDialog::accept();
}

void DlgPrefAbout::on_toolDb_clicked()
{
	QString fn=QFileDialog::getSaveFileName(this, QStringLiteral("Create New or Open Existing Book"), ui->linedb->text(),
											QStringLiteral("SQLIte Database (*.db)"), nullptr, QFileDialog::DontConfirmOverwrite);
	if(fn.isEmpty())
		return;
	qCDebug(dlgprefabout)<<"PrefAbout::on_toolDb_clicked"<<fn;
	ui->linedb->setText(fn);
	QSettings settings;
	settings.setValue(QStringLiteral("pref/databaseName"), ui->linedb->text());
	DCON.openDbSl();
	DCON.setup();
	ui->linedb->setText(DCON.db()->getFilename());
	ui->lineFragPath->setText(toRelPath(DCON.getFragmentDir().absolutePath()));
	ui->lineBioPath->setText(toRelPath(DCON.getBiographyDir().absolutePath()));
	ui->lineGeneratorPath->setText(toRelPath(DCON.getGeneratorDir().absolutePath()));
	ui->lineOutputPath->setText(toRelPath(DCON.getOutputDir().absolutePath()));
	//ui->checkProlog->setChecked(DCON.hasProlog());
	//ui->checkEpilog->setChecked(DCON.hasEpilog());
	pathsChanged=false;
}

void DlgPrefAbout::on_toolFragPath_clicked()
{
	QString fn=QFileDialog::getExistingDirectory(this, QStringLiteral("Select Fragment Folder"), DCON.getFragmentDir().path());
	qCDebug(dlgprefabout)<<"PrefAbout::on_toolFragPath_clicked"<<fn;
	if(fn.isEmpty())
		return;
	ui->lineFragPath->setText(toRelPath(fn));
	pathsChanged=true;
}

void DlgPrefAbout::on_toolBioPath_clicked()
{
	QString fn=QFileDialog::getExistingDirectory(this, QStringLiteral("Select Biography Folder"), DCON.getBiographyDir().path());
	qCDebug(dlgprefabout)<<"PrefAbout::on_toolBioPath_clicked"<<fn;
	if(fn.isEmpty())
		return;
	ui->lineBioPath->setText(toRelPath(fn));
	pathsChanged=true;
}

void DlgPrefAbout::on_toolGeneratorPath_clicked()
{
	QString fn=QFileDialog::getExistingDirectory(this, QStringLiteral("Select Generator Tool Folder"), DCON.getGeneratorDir().path());
	qCDebug(dlgprefabout)<<"PrefAbout::on_toolGeneratorPath_clicked"<<fn;
	if(fn.isEmpty())
		return;
	ui->lineGeneratorPath->setText(toRelPath(fn));
	pathsChanged=true;
}

void DlgPrefAbout::on_toolOutputPath_clicked()
{
	QString fn=QFileDialog::getExistingDirectory(this, QStringLiteral("Select Output Folder"), DCON.getOutputDir().path());
	qCDebug(dlgprefabout)<<"PrefAbout::on_toolOutputPath_clicked"<<fn;
	if(fn.isEmpty())
		return;
	ui->lineOutputPath->setText(toRelPath(fn));
	pathsChanged=true;
}

QString DlgPrefAbout::toRelPath(const QString &p)
{
	QString dbp=QFileInfo(ui->linedb->text()).absolutePath();
	int i=0;
	for(; i<dbp.size() && i<p.size() && dbp[i]==p[i]; ++i)
		;
	if(i==dbp.size()) // selected path is sub of db
	{
		QString res=QStringLiteral(".%1").arg(p.mid(i));
		qCDebug(dlgprefabout)<<"toRelPath"<<dbp<<p<<res;
		return res;
	}
	else // every other situation as absolute path
	{
		qCDebug(dlgprefabout)<<"toRelPath"<<dbp<<p<<"is abs";
		return p;
	}
}
