#ifndef DICTCSS_H
#define DICTCSS_H

#include "nvlh.h"

class DictCss
{
	typedef std::map< QString, QString > KvM;
public:
	DictCss() {}
	explicit DictCss(QString fromString);
	QString toString() const;
	QString getValue(const QString &property) const;
	void setValue(const QString &property, const QString &value); // setting an emtpy string removes
	bool operator==(const DictCss &o) const;
	bool operator!=(const DictCss &o) const {return !(*this==o);}
private:
	KvM kvs;
};

#endif // DICTCSS_H
