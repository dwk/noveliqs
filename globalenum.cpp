#include "globalenum.h"

QString enumFragmentActionStr(FragmentAction ia)
{
	switch(ia)
	{
	case FragmentAction::CREATED:
		return QString("CREATED");
	case FragmentAction::DELETED:
		return QString("DELETED");
	case FragmentAction::CHANGED:
		return QString("CHANGED");
	case FragmentAction::SEQCHANGED:
		return QString("SEQCHANGED");
	case FragmentAction::ATTENTION:
		return QString("ATTENTION");
	case FragmentAction::ALLGONE:
		return QString("ALLGONE");
	}
	return QStringLiteral("unknown FragmentAction: %1").arg((int)ia);
}

QString enumPersonActionStr(PersonAction ia)
{
	switch(ia)
	{
	case PersonAction::CREATED:
		return QString("CREATED");
	case PersonAction::DELETED:
		return QString("DELETED");
	case PersonAction::CHANGED:
		return QString("CHANGED");
	case PersonAction::ATTENTION:
		return QString("ATTENTION");
	case PersonAction::ALLGONE:
		return QString("ALLGONE");
	}
	return QStringLiteral("unknown PersonAction: %1").arg((int)ia);
}

QString enumAttendActionStr(AttendAction ia)
{
	switch(ia)
	{
	case AttendAction::CREATED:
		return QString("CREATED");
	case AttendAction::DELETED:
		return QString("DELETED");
	case AttendAction::ALLGONE:
		return QString("ALLGONE");
	}
	return QStringLiteral("unknown AttendAction: %1").arg((int)ia);
}

QString enumAttendRelStr(AttendRel rel)
{
	switch(rel)
	{
	case AttendRel::VISIBLE:
		return QString("VISIBLE");
	case AttendRel::HIDDEN:
		return QString("HIDDEN");
	}
	return QStringLiteral("unknown AttendRel: %1").arg((int)rel);
}

// Maturity --------------

QString enumMaturityStr(Maturity ia)
{
	switch(ia)
	{
	case Maturity::DUMMY:
		return QString("DUMMY");
	case Maturity::KEYWORDS:
		return QString("KEYWORDS");
	case Maturity::CONTENT:
		return QString("CONTENT");
	case Maturity::PARTTEXT:
		return QString("PARTTEXT");
	case Maturity::RAWTEXT:
		return QString("RAWTEXT");
	case Maturity::FULLTEXT:
		return QString("FULLTEXT");
	}
	return QStringLiteral("unknown Maturity: %1").arg((int)ia);
}

void enumMaturityComboSetup(QComboBox *cb)
{
	if(!cb)
		return;
	cb->addItem(QStringLiteral("DUMMY"), (int)Maturity::DUMMY);
	cb->addItem(QStringLiteral("KEYWORDS"), (int)Maturity::KEYWORDS);
	cb->addItem(QStringLiteral("CONTENT"), (int)Maturity::CONTENT);
	cb->addItem(QStringLiteral("PARTTEXT"), (int)Maturity::PARTTEXT);
	cb->addItem(QStringLiteral("RAWTEXT"), (int)Maturity::RAWTEXT);
	cb->addItem(QStringLiteral("FULLTEXT"), (int)Maturity::FULLTEXT);
}

void enumMaturityComboSet(QComboBox *cb, Maturity m)
{
	if(!cb)
		return;
	int idx=cb->findData((int)m);
	cb->setCurrentIndex(idx);
}

Maturity enumMaturityComboRead(QComboBox *cb)
{
	if(!cb)
		return Maturity::DUMMY;
	QVariant v=cb->currentData();
	if(v.isValid())
		return (Maturity)(v.toInt());
	return Maturity::DUMMY;
}

// Function --------------

QString enumFunctionStr(Function ia)
{
	switch(ia)
	{
	case Function::CHAPTER:
		return QString("CHAPTER");
	case Function::FRAGMENT:
		return QString("FRAGMENT");
	case Function::PROLOG:
		return QString("PROLOG");
	case Function::EPILOG:
		return QString("EPILOG");
	case Function::EVENT:
		return QString("EVENT");
	case Function::COMMENT:
		return QString("COMMENT");
	}
	return QStringLiteral("unknown Function: %1").arg((int)ia);
}

void enumFunctionComboSetup(QComboBox *cb)
{
	if(!cb)
		return;
	cb->addItem(QStringLiteral("CHAPTER"), (int)Function::CHAPTER);
	cb->addItem(QStringLiteral("FRAGMENT"), (int)Function::FRAGMENT);
	cb->addItem(QStringLiteral("PROLOG"), (int)Function::PROLOG);
	cb->addItem(QStringLiteral("EPILOG"), (int)Function::EPILOG);
	cb->addItem(QStringLiteral("EVENT"), (int)Function::EVENT);
	cb->addItem(QStringLiteral("COMMENT"), (int)Function::COMMENT);
}

void enumFunctionComboSet(QComboBox *cb, Function m)
{
	if(!cb)
		return;
	int idx=cb->findData((int)m);
	cb->setCurrentIndex(idx);
}

Function enumFunctionComboRead(QComboBox *cb)
{
	if(!cb)
		return Function::COMMENT;
	QVariant v=cb->currentData();
	if(v.isValid())
		return (Function)(v.toInt());
	return Function::COMMENT;
}

// PersDetail --------------

QString enumPersRelevanceStr(PersRelevance ia)
{
	switch(ia)
	{
	case PersRelevance::BACKGROUND:
		return QString("BACKGROUND");
	case PersRelevance::MINOR:
		return QString("MINOR");
	case PersRelevance::SUPPORT:
		return QString("SUPPORT");
	case PersRelevance::STAR:
		return QString("STAR");
	}
	return QStringLiteral("unknown PersRole: %1").arg((int)ia);
}

void enumPersRelevanceComboSetup(QComboBox *cb)
{
	if(!cb)
		return;
	cb->addItem(QStringLiteral("BACKGROUND"), (int)PersRelevance::BACKGROUND);
	cb->addItem(QStringLiteral("MINOR"), (int)PersRelevance::MINOR);
	cb->addItem(QStringLiteral("SUPPORT"), (int)PersRelevance::SUPPORT);
	cb->addItem(QStringLiteral("STAR"), (int)PersRelevance::STAR);
}

void enumPersRelevanceComboSet(QComboBox *cb, PersRelevance m)
{
	if(!cb)
		return;
	int idx=cb->findData((int)m);
	cb->setCurrentIndex(idx);
}

PersRelevance enumPersRelevanceComboRead(QComboBox *cb)
{
	if(!cb)
		return PersRelevance::BACKGROUND;
	QVariant v=cb->currentData();
	if(v.isValid())
		return (PersRelevance)(v.toInt());
	return PersRelevance::BACKGROUND;
}

// IterateObjectHint --------------

QString enumIterateObjectHintStr(IterateObjectHint io)
{
	switch(io)
	{
	case IterateObjectHint::PROLOG:
		return QString("PROLOG");
	case IterateObjectHint::FRAGMENT:
		return QString("FRAGMENT");
	case IterateObjectHint::EPILOG:
		return QString("EPILOG");
	case IterateObjectHint::PERSON:
		return QString("PERSON");
	}
	return QStringLiteral("unknown IterateObjectHint: %1").arg((int)io);
}

/* ZoomLevel --------------

QString enumZoomLevelStr(ZoomLevel zl)
{
	switch(zl)
	{
	case ZoomLevel::DAY:
		return QString("DAY");
	case ZoomLevel::DAYS:
		return QString("DAYS");
	case ZoomLevel::MONTH:
		return QString("MONTH");
	case ZoomLevel::YEAR:
		return QString("YEAR");
	case ZoomLevel::DECADE:
		return QString("DECADE");
	case ZoomLevel::CENTURY:
		return QString("CENTURY");
	}
	return QStringLiteral("unknown ZoomLevel: %1").arg((int)zl);
}


void enumZoomLevelComboSetup(QComboBox *cb)
{
	if(!cb)
		return;
	cb->addItem(QStringLiteral("Day"), (int)ZoomLevel::DAY);
	cb->addItem(QStringLiteral("Days"), (int)ZoomLevel::DAYS);
	cb->addItem(QStringLiteral("Month"), (int)ZoomLevel::MONTH);
	cb->addItem(QStringLiteral("Year"), (int)ZoomLevel::YEAR);
	cb->addItem(QStringLiteral("Decade"), (int)ZoomLevel::DECADE);
	cb->addItem(QStringLiteral("Century"), (int)ZoomLevel::CENTURY);
}

void enumZoomLevelComboSet(QComboBox *cb, ZoomLevel zl)
{
	if(!cb)
		return;
	int idx=cb->findData((int)zl);
	cb->setCurrentIndex(idx);
}

ZoomLevel enumZoomLevelComboRead(QComboBox *cb)
{
	if(!cb)
		return ZoomLevel::DAY;
	QVariant v=cb->currentData();
	if(v.isValid())
		return (ZoomLevel)(v.toInt());
	return ZoomLevel::DAY;
} */
