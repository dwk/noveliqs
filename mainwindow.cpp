#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QPointer>
#include <QDockWidget>
#include <QShortcut>
#include <QKeySequence>
#include "dlgprefabout.h"
#include "dlggenerate.h"
#include "dockwidfactory.h"
#include "ifactor.h"
#include "nvli.h"

Q_LOGGING_CATEGORY(mainwindow, "mainwindow")

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow), tagMapper(new QSignalMapper(this))
{
	ui->setupUi(this);
	DCON.setup(this);
	if(!DCON.db())
	{
		abort=true;
		return; // user aborted startup
	}

	//QString centrName=settings.value("layout/central", "dwid_image").toString();
	//QWidget *central=nullptr;
	QStringList dws=DockWidFactory::get().allDockWids();
	foreach(QString dwname, dws)
	{
		DockWidFactory::DWInfo *dwi=DockWidFactory::get().produce(dwname);
		if(!dwi)
			continue;
		/*if(dwname==centrName)
		{
			qCDebug(mainwindow)<<"central widget"<<dwname;
			central=dwi->factory(nullptr);
			actors.insert(ObjectMSP::value_type(dwname, central));
			setCentralWidget(central);
			continue;
		}*/
		qCDebug(mainwindow)<<"dock widget"<<dwname;
		QDockWidget *dock = new QDockWidget(dwi->guiName, this);
		dock->setObjectName(dwi->techName);
		dock->setStyleSheet("QDockWidget{border: 4px solid #804000} QDockWidget::title{background: #ffd8c0}");
		dock->setAllowedAreas(dwi->allowedAreas);
		QWidget *dw = dwi->factory(dock);
		dock->setWidget(dw);
		actors.insert(ObjectMSP::value_type(dwname, dw));
		ui->menuWindows->addAction(dock->toggleViewAction());
		addDockWidget(dwi->defaultArea, dock);
		/* bool restore=restoreDockWidget(dock);
		if(!restore)
		{
			qWarning()<<"unable to restore layout of dock widget"<<dwname;
			addDockWidget(dwi->defaultArea, dock);
		} */
	}
	//if(!central)
	//	qWarning()<<"no central widget"<<centrName;

	bool restOkRead;
	QSettings settings;
	restOkRead=restoreGeometry(settings.value("layout/geometry").toByteArray());
	restOkRead=restoreState(settings.value("layout/windowState").toByteArray()) && restOkRead;
	if(!restOkRead)
	{
		qWarning()<<"unable to restore layout or status";
	}
	ui->textScribble->setHtml(settings.value("data/scribble").toString());

	connect(ui->actionAbout, SIGNAL(triggered(bool)), this, SLOT(about()));
	connect(ui->actionGenerate, SIGNAL(triggered(bool)), this, SLOT(generateSl()));
	/*auto itImages=actors.find(QString("dwid_images"));
	if(itImages!=actors.end())
	{
		connect(ui->actionNextImage, SIGNAL(triggered(bool)), itImages->second, SLOT(nextImageSl()));
		connect(ui->actionPrevImage, SIGNAL(triggered(bool)), itImages->second, SLOT(prevImageSl()));
		connect(ui->actionEditDescription, SIGNAL(triggered(bool)), itImages->second, SLOT(editDescriptionSl()));
		connect(ui->actionEditImage, SIGNAL(triggered(bool)), itImages->second, SLOT(editImageSl()));
		connect(ui->actionOpenGimp, SIGNAL(triggered(bool)), itImages->second, SLOT(openGimpSl()));
		connect(ui->actionDeleteImage, SIGNAL(triggered(bool)), itImages->second, SLOT(deleteImageSl()));
	}
	else
		qWarning()<<"unable to locate dwid_images - some signals may remain unconnected";*/
	// make sure DCON setups first so that data is available - undocumented Qt behavior?
	actors.insert(ObjectMSP::value_type(QStringLiteral("000_DataContainer"), &DCON));
	for(auto it=actors.begin(); it!=actors.end(); ++it)
	{
		IfActor * act=dynamic_cast<IfActor *>(it->second);
		qCDebug(mainwindow)<<"connectAll"<<it->second->objectName();
		if(act)
			act->connectAll(actors);
	}
	for(auto it=actors.begin(); it!=actors.end(); ++it)
	{
		IfActor * act=dynamic_cast<IfActor *>(it->second);
		qCDebug(mainwindow)<<"setup"<<it->second->objectName();
		if(act)
			act->setup();
	}
}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	QSettings settings;
	settings.setValue("layout/geometry", saveGeometry());
	settings.setValue("layout/windowState", saveState());
	settings.setValue("data/scribble", ui->textScribble->toHtml());
	DCON.shutdown();
	qInfo()<<"closing MainWindow";
	QMainWindow::closeEvent(event);
}

/*void MainWindow::imageActionGSl(int imagePk, ImageAction action)
{
	switch(action)
	{
	case ImageAction::SELECT:
	{
		bool ena=(imagePk>0);
		singleImageSelected=ena;
		ui->actionTagAgain->setEnabled(singleImageSelected);
		ui->actionEditDescription->setEnabled(ena);
		ui->actionEditImage->setEnabled(ena);
		ui->actionDeleteImage->setEnabled(ena);
		ui->actionOpenGimp->setEnabled(ena);
		break;
	}
	case ImageAction::SELECT_MULTI:
		singleImageSelected=false;
		ui->actionTagAgain->setEnabled(false);
		ui->actionEditDescription->setEnabled(true);
		ui->actionEditImage->setEnabled(false);
		ui->actionDeleteImage->setEnabled(false);
		ui->actionOpenGimp->setEnabled(false);
		break;
	case ImageAction::MARK:
	case ImageAction::UNMARK:
	case ImageAction::MARK_MULTI:
	case ImageAction::DELETE:
	case ImageAction::SAVE_AS:
	case ImageAction::LABELS_CHANGED:
	case ImageAction::ATTRIB_CHANGED:
	case ImageAction::PIXEL_CHANGED:
		break;
	}
}*/

void MainWindow::about()
{
	DlgPrefAbout dlg(this);
	dlg.exec();
}

void MainWindow::generateSl()
{
	DlgGenerate dlg(this);
	dlg.exec();
}

