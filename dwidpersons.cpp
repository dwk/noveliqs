#include "dwidpersons.h"
#include "dwidpersons_fac.h"
#include "ui_dwidpersons.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QFontDatabase>
#include <QMenu>
#include "dlgperson.h"
#include "nvli.h"

Q_LOGGING_CATEGORY(dwidpersons, "dwidpersons")

DWidPersons::DWidPersons(QWidget *parent) :
	QWidget(parent), ui(new ::Ui::DWidPersons)
{
	setObjectName(techname);
	ui->setupUi(this);
	ui->tabPerson->setContextMenuPolicy(Qt::CustomContextMenu);
	QFontDatabase fdb;
	QFont sysfont=QFontDatabase::systemFont(QFontDatabase::GeneralFont);
	itFont=fdb.font(sysfont.family(), QStringLiteral("Italic"), sysfont.pointSize());
}

DWidPersons::~DWidPersons()
{
	delete ui;
}

void DWidPersons::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		if(it->second->objectName()==objectName())
			continue; // don't connect to objects of your own type
		/*if(obj->objectName()=="000_DataContainer")
		{
			for(int i=0; i<mo->methodCount(); ++i)
			{
				QMetaMethod mm=mo->method(i);
				qDebug()<<mm.typeName()<<mm.name()<<mm.methodSignature();
			}
		}*/
		IfActor::connectX(it->second, IfActor::personSigNorm, this, IfActor::personSlNorm);
	}
}

void DWidPersons::setup()
{
	ui->tabPerson->resizeColumnsToContents();
	ui->tabPerson->resizeRowsToContents();
	if(ui->tabPerson->rowCount()>0)
	{
		ui->tabPerson->selectionModel()->select(ui->tabPerson->model()->index(0,0), QItemSelectionModel::Select);
		ui->tabPerson->setCurrentCell(0,0);
	}
}

Person *DWidPersons::tabRow2Pers(int row)
{
	int r=row;
	if(r<0)
		r=ui->tabPerson->currentRow();
	if(r<0)
	{
		qCritical()<<"tabRow2Frag no row";
		return nullptr;
	}
	int pk=ui->tabPerson->item(r, 0)->data(Qt::UserRole).toInt();
	if(pk<=0)
	{
		qCritical()<<"tabRow2Frag no pk"<<r;
		return nullptr;
	}
	return DCON.getPerson(pk);
}

int DWidPersons::tabPers2Row(int persPk)
{
	for(int r=0, rm=ui->tabPerson->rowCount(); r<rm; ++r)
	{
		if(ui->tabPerson->item(r, 0)->data(Qt::UserRole).toInt()==persPk)
			return r;
	}
	return -1;
}

QColor DWidPersons::role2Color(Person &pers)
{
	switch(pers.getRelevance())
	{
	case PersRelevance::BACKGROUND:
		return QColor(200, 200, 200);
	case PersRelevance::MINOR:
		return QColor(230, 230, 255);
	case PersRelevance::SUPPORT:
		return QColor(230, 230, 200);
	case PersRelevance::STAR:
		return QColor(255, 240, 160);
	}
	return QColor(50, 50, 50);
}

void DWidPersons::buttonState(int row)
{
	ui->toolDelete->setEnabled(row>=0 && ui->tabPerson->rowCount()>1);
	ui->toolEdit->setEnabled(row>=0);
	ui->toolFullEdit->setEnabled(row>=0);
}

void DWidPersons::personSl(int pk, PersonAction action, int pkOther)
{
	Q_UNUSED(pkOther)
	Person * pers=nullptr;
	if(action!=PersonAction::DELETED && action!=PersonAction::ALLGONE)
	{
		pers=DCON.getPerson(pk);
		if(!pers)
		{
			qCritical()<<"DWidPersons::personSl not a Person"<<pk;
			return;
		}
	}
	switch(action)
	{
	case PersonAction::CREATED:
	{
		freezePersSigs=true;
		QColor bc=role2Color(*pers);
		int row=ui->tabPerson->rowCount(); // here we want to insert
		ui->tabPerson->setRowCount(row+1);
		QTableWidgetItem *twi;
		twi=new QTableWidgetItem(pers->getFirst());
		twi->setData(Qt::UserRole, pers->getPk());
		twi->setBackground(bc);
		ui->tabPerson->setItem(row, 0, twi); // first
		twi=new QTableWidgetItem(pers->getLast());
		twi->setBackground(bc);
		ui->tabPerson->setItem(row, 1, twi); // last
		twi=new QTableWidgetItem(pers->getRole());
		twi->setBackground(bc);
		ui->tabPerson->setItem(row, 2, twi); // role
		twi=new QTableWidgetItem(pers->getBirth().toString(UI_DATE_FORMAT));
		twi->setBackground(bc);
		ui->tabPerson->setItem(row, 3, twi); //birth
		twi=new QTableWidgetItem(pers->getDeath().toString(UI_DATE_FORMAT));
		twi->setBackground(bc);
		ui->tabPerson->setItem(row, 4, twi); // death
		twi=new QTableWidgetItem(pers->getFilename());
		twi->setBackground(bc);
		ui->tabPerson->setItem(row, 5, twi); // filename
		break;
	}
	case PersonAction::DELETED:
	{
		freezePersSigs=true;
		int row=tabPers2Row(pk);
		if(row>=0)
			ui->tabPerson->removeRow(row);
		break;
	}
	case PersonAction::CHANGED:
	{
		freezePersSigs=true;
		QColor bc=role2Color(*pers);
		int row=tabPers2Row(pk);
		QTableWidgetItem *twi=ui->tabPerson->item(row, 0); // first
		twi->setText(pers->getFirst());
		twi->setBackground(bc);
		twi=ui->tabPerson->item(row, 1); // last
		twi->setText(pers->getLast());
		twi->setBackground(bc);
		twi=ui->tabPerson->item(row, 2); // role
		twi->setText(pers->getRole());
		twi->setBackground(bc);
		twi=ui->tabPerson->item(row, 3); // brith
		twi->setText(pers->getBirth().toString(UI_DATE_FORMAT));
		twi->setBackground(bc);
		twi=ui->tabPerson->item(row, 4); // death
		twi->setText(pers->getDeath().toString(UI_DATE_FORMAT));
		twi->setBackground(bc);
		twi=ui->tabPerson->item(row, 5); // file
		twi->setText(pers->getFilename());
		twi->setBackground(bc);
		break;
	}
	case PersonAction::ATTENTION:
	{
		freezePersSigs=true;
		int r=tabPers2Row(pk);
		if(r>=0)
		{
			ui->tabPerson->selectionModel()->select(ui->tabPerson->model()->index(r,0), QItemSelectionModel::Select);
			ui->tabPerson->setCurrentCell(r,0);
		}
		break;
	}
	case PersonAction::ALLGONE:
		freezePersSigs=true;
		ui->tabPerson->clearContents();
		ui->tabPerson->setRowCount(0);
		break;
	}
	freezePersSigs=false;
}

void DWidPersons::on_tabPerson_itemDoubleClicked(QTableWidgetItem *item)
{
	Q_UNUSED(item)
	on_toolEdit_clicked();
}

void DWidPersons::on_tabPerson_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous)
{
	int r=(current?current->row():-1);
	buttonState(r);
	if(freezePersSigs)
		return;
	int pk=-1, pkOld=-1;
	bool ok=false;
	if(r>=0)
		pk=ui->tabPerson->item(r, 0)->data(Qt::UserRole).toInt(&ok);
	if(!ok)
		pk=-1;
	if(previous)
	{
		int ro=previous->row();
		ok=false;
		if(ro>=0)
			pkOld=ui->tabPerson->item(ro, 0)->data(Qt::UserRole).toInt(&ok);
		if(!ok)
			pkOld=-1;
	}
	emit personSig(pk, PersonAction::ATTENTION, pkOld);
}

void DWidPersons::on_tabPerson_customContextMenuRequested(const QPoint &pos)
{
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QAction * act=menu->addAction(tr("use birth as ref.date"));
	connect(act, &QAction::triggered, this, &DWidPersons::refDateSl);
	act=menu->addAction(tr("select scenes where active"));
	connect(act, &QAction::triggered, this, &DWidPersons::selFragsSl);
	menu->popup(ui->tabPerson->viewport()->mapToGlobal(pos));
}

void DWidPersons::on_toolNew_clicked()
{
	DlgPerson dlg(this);
	dlg.exec(); // table update through IfActor signal, sequence shifting in data container
}

void DWidPersons::on_toolEdit_clicked()
{
	Person *p=tabRow2Pers();
	if(!p)
		return;
	DlgPerson dlg(*p, this);
	dlg.exec();
}

void DWidPersons::on_toolFullEdit_clicked()
{
	Person *pers=tabRow2Pers();
	if(!pers)
		return;
	pers->editBioFile();
}

void DWidPersons::on_toolDelete_clicked()
{
	Person *p=tabRow2Pers();
	if(!p)
		return;
	if(QMessageBox::question(this, tr("Delete Person"), tr("You are about to delete person %1 '%2'. Really?").arg(p->getName(Person::NameStyle::FULL_HUMAN), p->getFilename()), QMessageBox::Yes | QMessageBox::No, QMessageBox::No)!=QMessageBox::Yes)
		return;
	DCON.deletePerson(p, true);
}

void DWidPersons::refDateSl()
{
	Person *p=tabRow2Pers();
	if(!p)
		return;
	emit refDateSig(p->getBirth().startOfDay());
}

void DWidPersons::selFragsSl()
{
	Person *p=tabRow2Pers();
	if(!p)
		return;
	std::set<int> atts=DCON.getAttendFragPks(p->getPk());
	int fragPk=-1;
	QVector<int> of;
	auto it=atts.begin();
	if(it!=atts.end())
	{
		fragPk=*it;
		++it;
		for(; it!=atts.end(); ++it)
			of<<*it;
	}
	emit fragmentSig(fragPk, FragmentAction::ATTENTION, of);
}
