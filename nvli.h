#ifndef NVLI_H
#define NVLI_H

#include <QDebug>
#include <QMessageBox>
#include <QApplication>
#include <QSettings>
#include <QMetaMethod>
#include <QVector>
#include "datacontainer.h"
#include "singleton.h"
#define DCON Singleton<DataContainer>::instance()

using namespace std;

#endif // NVLI_H
