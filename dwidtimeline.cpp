#include "dwidtimeline.h"
#include "dwidtimeline_fac.h"
#include "ui_dwidtimeline.h"
#include "qmath.h"
#include "dlgfragment.h"
#include "nvli.h"

Q_LOGGING_CATEGORY(dwidtimeline, "dwidtimeline")

DWidTimeline::DWidTimeline(QWidget *parent) :
	QWidget(parent), ui(new ::Ui::DWidTimeline), scene(new EditorScene(this))
{
	setObjectName(techname);
	ui->setupUi(this);
	ui->graphicsView->setScene(scene);
	connect(ui->graphicsView, &EditorView::zoomChangedSig, this, &DWidTimeline::timelineZoomChangedSl);
	connect(ui->checkPers, SIGNAL(toggled(bool)), scene, SLOT(perssVisSl(bool)));
	connect(ui->checkPersLabel, SIGNAL(toggled(bool)), scene, SLOT(perssDecoVisSl(bool)));
	connect(ui->checkFragLabel, SIGNAL(toggled(bool)), scene, SLOT(fragsDecoVisSl(bool)));
	//scene->clearFrags(); empty anyway!
}

DWidTimeline::~DWidTimeline()
{
	delete ui;
}

double DWidTimeline::datetime2Coord(const QDateTime &dt) const
{
	if(dt.isValid())
		return ((double)DCON.getStoryStart().date().daysTo(dt.date())+(double)(dt.time().msecsSinceStartOfDay())/86400000.)*timelineScale;
	return numeric_limits<double>::quiet_NaN();
}

double DWidTimeline::datetime2Coord(const QDate &dt) const
{
	if(dt.isValid())
		return (double)DCON.getStoryStart().date().daysTo(dt)*timelineScale;
	return numeric_limits<double>::quiet_NaN();
}

QDate DWidTimeline::coord2Date(double t)
{
	return DCON.getStoryStart().date().addDays(floor(t/timelineScale));
}

QDateTime DWidTimeline::coord2DateTime(double t)
{
	return DCON.getStoryStart().addSecs(t*86400./timelineScale);
}

void DWidTimeline::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		if(it->second->objectName()==objectName())
			continue; // don't connect to objects of your own type
		/*if(obj->objectName()=="000_DataContainer")
		{
			for(int i=0; i<mo->methodCount(); ++i)
			{
				QMetaMethod mm=mo->method(i);
				qDebug()<<mm.typeName()<<mm.name()<<mm.methodSignature();
			}
		}*/
		IfActor::connectX(it->second, IfActor::fragmentSigNorm, this, IfActor::fragmentSlNorm);
		IfActor::connectX(it->second, IfActor::personSigNorm, this, IfActor::personSlNorm);
		IfActor::connectX(it->second, IfActor::attendSigNorm, this, IfActor::attendSlNorm);
		IfActor::connectX(it->second, IfActor::minMaxDateSigNorm, this, IfActor::minMaxDateSlNorm);
		IfActor::connectX(it->second, IfActor::refDateSigNorm, this, IfActor::refDateSlNorm);
	}
}

void DWidTimeline::setup()
{
	scene->addTimeline(timelineScale);
	ui->label->setText(scene->scaleChanged(timelineScale));
}

void DWidTimeline::fragmentSl(int pk, FragmentAction action, QVector<int> pkOther)
{
	Fragment * frag=nullptr;
	if(action!=FragmentAction::DELETED && action!=FragmentAction::ALLGONE)
	{
		frag=DCON.getFragment(pk);
		if(!frag)
		{
			qCritical()<<"DWidTimeline::fragmentSl not a Fragment"<<pk;
			return;
		}
	}
	switch(action)
	{
	case FragmentAction::CREATED:
	{
		Function f=frag->getFunction();
		if(f!=Function::CHAPTER && f!=Function::FRAGMENT && f!=Function::EVENT)
			return;
		EditorFragment *ef=new EditorFragment(frag->getPk(), scene);
		connect(ef, &EditorFragment::selectedSig, this, &DWidTimeline::fragSelectedSl);
		scene->addFrag(ef);
		ui->graphicsView->zoomTo(frag);
		break;
	}
	case FragmentAction::DELETED:
	{
		scene->removeFrag(pk);
		break;
	}
	case FragmentAction::CHANGED:
	{
		Function f=frag->getFunction();
		if(f!=Function::CHAPTER && f!=Function::FRAGMENT && f!=Function::EVENT)
		{
			scene->removeFrag(pk);
			return;
		}
		scene->updateFrag(pk);
		set<int> persPks=DCON.getAttendPersPks(pk);
		for(auto pit=persPks.begin(); pit!=persPks.end(); ++pit)
			scene->updatePers(*pit);
		break;
	}
	case FragmentAction::SEQCHANGED:
	{
		break; // timeline does not care
	}
	case FragmentAction::ATTENTION:
	{
		ui->graphicsView->zoomTo(frag);
		scene->selectFrag(pk, pkOther);
		break;
	}
	case FragmentAction::ALLGONE:
	{
		scene->clearFrags();
		break;
	}
	}
}

void DWidTimeline::personSl(int pk, PersonAction action, int pkOther)
{
	Q_UNUSED(pkOther)
	Person * pers=nullptr;
	if(action!=PersonAction::DELETED && action!=PersonAction::ALLGONE)
	{
		pers=DCON.getPerson(pk);
		if(!pers)
		{
			qCritical()<<"DWidTimeline::personSl not a Person"<<pk;
			return;
		}
	}
	switch(action)
	{
	case PersonAction::CREATED:
	{
		EditorPerson *ep=new EditorPerson(pers->getPk(), scene);
		connect(ep, &EditorPerson::selectedSig, this, &DWidTimeline::persSelectedSl);
		scene->addPers(ep);
		break;
	}
	case PersonAction::DELETED:
	{
		scene->removePers(pk);
		break;
	}
	case PersonAction::CHANGED:
	{
		scene->updatePers(pk);
		// fragments never update on person change
		break;
	}
	case PersonAction::ATTENTION:
	{
		scene->selectPers(pk);
		break;
	}
	case PersonAction::ALLGONE:
	{
		scene->clearPers();
		break;
	}
	}
}

void DWidTimeline::attendSl(int attPk, AttendAction action, int fragPk, int persPk)
{
	Q_UNUSED(attPk)
	Q_UNUSED(fragPk)
	switch(action)
	{
	case AttendAction::ALLGONE:
		break;
	case AttendAction::CREATED:
	case AttendAction::DELETED:
		scene->updateFrag(fragPk);
		scene->updatePers(persPk);
		break;
	}
}

void DWidTimeline::minMaxDateSl(const QDateTime &start, const QDateTime &end)
{
	if(!scene || !scene->getTimeline())
		return;
	scene->getTimeline()->minMaxDateSl(start, end);
}

void DWidTimeline::refDateSl(const QDateTime &refDate)
{
	if(!scene || !scene->getTimeline())
		return;
	scene->getTimeline()->refDateSl(refDate);
}

void DWidTimeline::timelineZoomChangedSl(int zoomDelta, int baseWidth)
{
	Q_UNUSED(baseWidth)
	//qDebug()<<"timelineZoomChangedSl"<<zoomDelta<<baseWidth;
	if(zoomDelta>0)
		timelineScale*=(1.+(double)zoomDelta/120./5.);
	else if(zoomDelta<0)
		timelineScale/=(1.-(double)zoomDelta/120./5.);
	if(zoomDelta)
	{
		ui->label->setText(scene->scaleChanged(timelineScale));
		ui->graphicsView->recenter();
	}
	//qDebug()<<"DataContainer timelineScale"<<timelineScale<<enumZoomLevelStr(zl)<<baseWidth<<getStoryStartCoord()<<getStoryEndCoord();
}

void DWidTimeline::fragSelectedSl(int fragPk, bool selected)
{
	if(!selected || fragPk<0)
		return;
	emit fragmentSig(fragPk, FragmentAction::ATTENTION, QVector<int>()); // we don't know which one was deselected
}

void DWidTimeline::persSelectedSl(int persPk, bool selected)
{
	if(!selected || persPk<0)
		return;
	emit personSig(persPk, PersonAction::ATTENTION, -1); // we don't know which one was deselected
}

void DWidTimeline::on_toolZoomIn_clicked()
{
	ui->graphicsView->saveRefPnt(QPoint());
	timelineZoomChangedSl(120, 0);
}

void DWidTimeline::on_toolZoomRes_clicked()
{
	ui->graphicsView->saveRefPnt(QPoint());
	timelineScale=.1;
	ui->label->setText(scene->scaleChanged(timelineScale));
	ui->graphicsView->recenter();
	ui->graphicsView->resetIsoZoom();
}

void DWidTimeline::on_toolZoomOut_clicked()
{
	ui->graphicsView->saveRefPnt(QPoint());
	timelineZoomChangedSl(-120, 0);
}
