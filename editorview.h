#ifndef EDITORVIEW_H
#define EDITORVIEW_H

#include "nvlh.h"
#include <QGraphicsView>
#include <QDateTime>

class DWidTimeline;
class Fragment;

class EditorView : public QGraphicsView
{
    Q_OBJECT
public:
	// Zoom is only relevant in time.
	// Since pen width is defined and text is drawn in world coordinates
	// I decided to use a fixed, isotrope scaling. Switching between
	// "zoom" levels then has to re-position editor objects. Zoom becomes
	// an object property, it is not view specific. Still not that bad
	// since the persistent object uses real world DateTime, which does
	// not change with zoom. Transformation between DateTime and coord
	// does change with zoom.
	EditorView(QWidget *parent = Q_NULLPTR);
	void resetIsoZoom();
	void zoomTo(int fragPk);
	void zoomTo(Fragment *frag);
	void saveRefPnt(const QPoint &point);
	void recenter();
protected:
	virtual void resizeEvent(QResizeEvent *event) override;
#ifndef QT_NO_WHEELEVENT
    void wheelEvent(QWheelEvent *) Q_DECL_OVERRIDE;
#endif
private:
	DWidTimeline * getDWid();
	QPoint lastCenterPos;
	QDateTime lastCenterDate;
	double lastCenterY=0., isoZoom=1.;
signals:
	void zoomChangedSig(int zoomDelta, int baseWidth); // zoomDelta>0 = zoom in; old ZoomLevel newLevel
};

#endif // EDITORVIEW_H
