#ifndef PERSON_H
#define PERSON_H

#include "nvlh.h"
#include <QObject>
#include <QPointer>
#include <QDate>
#include "dictcss.h"

class Person : public QObject
{
	friend class DataContainer;
	Q_OBJECT
public:
	enum class NameStyle {FULL_MASCHINE, FULL_HUMAN, SEMI_HUMAN};
	static void fromDb();
	explicit Person(QObject *parent);
	Person(int pk, QString first, QString middle, QString last, QDate birth, QDate death, QString role, QString bio,
		PersRelevance detail, QString editorhint, QObject *parent);
	bool fromDb(QString first, QString middle, QString last, QDate birth, QDate death, QString role, QString bio,
		PersRelevance detail, QString editorhint); // true if changed
	bool commit(); // true if db transaction
	void deleteInDb();
	QString getName(Person::NameStyle st) const;
	int getPk() const {return pk;}
	QString getFirst() const {return first;}
	void setFirst(const QString &first) {if(this->first!=first) {this->first=first; touch();}}
	QString getMiddle() const {return middle;}
	void setMiddle(const QString &middle) {if(this->middle!=middle) {this->middle=middle; touch();}}
	QString getLast() const {return last;}
	void setLast(const QString &last) {if(this->last!=last) {this->last=last; touch();}}
	QString getRole() const {return role;}
	void setRole(const QString &role) {if(this->role!=role) {this->role=role; touch();}}
	QString getFilename() const {return bio;}
	void setFilename(const QString &bio) {if(this->bio!=bio) {this->bio=bio; touch();}}
	QDate getBirth() const {return birth;}
	void setBirth(const QDate &birth) {if(this->birth!=birth) {this->birth=birth; touch();}}
	QDate getDeath() const {return death;}
	void setDeath(const QDate &death) {if(this->death!=death) {this->death=death; touch();}}
	PersRelevance getRelevance() const {return detail;}
	void setRelevance(PersRelevance detail) {if(this->detail!=detail) {this->detail=detail; touch();}}
	void editBioFile();
	static bool editBioFile(const QString & bioFile);
	QString getEditorhint(const QString &property) const {return editorhints.getValue(property);}
	void setEditorhint(const QString &property, const QString &value) {editorhints.setValue(property, value); touch();}
signals:
	void dataChangedSig(int pk, PersonAction action);
private:
	void touch();
	int pk=-1; // -2 not initialized, -1 new (not in DB), 0 invalid / error, >0 in DB
	bool mod=false; // modified compared to DB
	QString first, middle, last, role, bio;
	QDate birth, death;
	PersRelevance detail=PersRelevance::BACKGROUND;
	DictCss editorhints;
};
#endif // PERSON_H
