#ifndef DLGPERSON_H
#define DLGPERSON_H

#include "nvlh.h"
#include <QDialog>
#include "person.h"

Q_DECLARE_LOGGING_CATEGORY(dlgperson)

namespace Ui {
class DlgPerson;
}

class DlgPerson : public QDialog
{
	Q_OBJECT

public:
	explicit DlgPerson(QWidget *parent = nullptr);
	DlgPerson(Person &pers, QWidget *parent = nullptr);
	~DlgPerson();
	Person *getPerson() {return pers;}
public slots:
	virtual void accept();
private slots:
	void on_checkNoBirth_stateChanged(int);
	void on_checkNoDeath_stateChanged(int);
	void on_toolBio_clicked();
	void on_toolEditBio_clicked();
private:
	Ui::DlgPerson *ui;
	Person *pers=nullptr;
};

#endif // DLGPERSON_H
