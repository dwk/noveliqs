#include "person.h"
#include <QProcess>
#include <QSqlQuery>
#include <QSqlError>
#include <QTimer>
#include "nvli.h"

#define PERSON_DCSIG_DELAY 333

void Person::fromDb()
{
	set<int> pks=DCON.getPersonPks();
	MainWindow * nv=DCON.getMainDlg();
	//                                 0   1      2       3     4      5      6     7    8       9
	QSqlQuery q(QStringLiteral("select pk, first, middle, last, birth, death, role, bio, detail, editorhint from persons"), DCON.db()->db());
	while(q.next())
	{
		int pk=q.value(0).toInt();
		Person *op=DCON.getPerson(pk, false);
		if(!op)
		{
			Person *p=new Person(pk, q.value(1).toString(), q.value(2).toString(), q.value(3).toString(), \
					q.value(4).toDateTime().date(), q.value(5).toDateTime().date(), q.value(6).toString(), q.value(7).toString(), \
					(PersRelevance)(q.value(8).toInt()), q.value(9).toString(), nv);
			DCON.insertPerson(p);
		}
		else
		{
			op->fromDb(q.value(1).toString(), q.value(2).toString(), q.value(3).toString(), \
								q.value(4).toDateTime().date(), q.value(5).toDateTime().date(), q.value(6).toString(), \
								q.value(7).toString(), (PersRelevance)(q.value(8).toInt()), q.value(9).toString());
			pks.erase(pk);
			DCON.insertPerson(op); // update
		}
	}
	for(auto pit=pks.begin(); pit!=pks.end(); ++pit)
		DCON.deletePerson(*pit); // don't touch DB
}

Person::Person(QObject *parent) : QObject(parent)
{
}

Person::Person(int pk, QString first, QString middle, QString last, QDate birth, QDate death, QString role, QString bio, PersRelevance detail, QString editorhint, QObject *parent) :
	QObject(parent), pk(pk), first(first), middle(middle), last(last), role(role), bio(bio), birth(birth), death(death), detail(detail), editorhints(editorhint)
{

}

bool Person::fromDb(QString first, QString middle, QString last, QDate birth, QDate death, QString role, QString bio, PersRelevance detail, QString editorhint)
{
	bool changed=false;
	if(this->first!=first)
	{
		this->first=first;
		changed=true;
	}
	if(this->middle!=middle)
	{
		this->middle=middle;
		changed=true;
	}
	if(this->last!=last)
	{
		this->last=last;
		changed=true;
	}
	if(this->role!=role)
	{
		this->role=role;
		changed=true;
	}
	if(this->bio!=bio)
	{
		this->bio=bio;
		changed=true;
	}
	if(this->birth!=birth)
	{
		this->birth=birth;
		changed=true;
	}
	if(this->death!=death)
	{
		this->death=death;
		changed=true;
	}
	if(this->detail!=detail)
	{
		this->detail=detail;
		changed=true;
	}
	DictCss dc(editorhint);
	if(this->editorhints!=dc)
	{
		this->editorhints=dc;
		changed=true;
	}
	mod=false;
	if(changed)
		emit dataChangedSig(pk, PersonAction::CHANGED);
	return changed;
}

bool Person::commit()
{
	if(pk>0 && !mod)
		return false;
	if(pk>0)
	{
		QSqlQuery q(QStringLiteral("update persons set first=%2, middle=%3, last=%4, role=%5, bio=%6, birth=%7, death=%8, detail=%9, editorhint=%10 where pk=%1")\
					.arg(pk).arg(DataContainer::toSqlString(first), DataContainer::toSqlString(middle), DataContainer::toSqlString(last),\
					DataContainer::toSqlString(role), DataContainer::toSqlString(bio),\
					DataContainer::toSqlString(birth, true), DataContainer::toSqlString(death, true)).arg((int)detail).\
					arg(DataContainer::toSqlString(editorhints.toString(),true)));
		if(q.lastError().isValid())
		{
			qCritical()<<"FAILED Person update"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
			return false;
		}
		emit dataChangedSig(pk, PersonAction::CHANGED);
	} // DataContainer::toSqlString(editorhints.toString())
	else
	{
		QSqlQuery q(QStringLiteral("insert into persons (first, middle, last, role, bio, birth, death, detail, editorhint) values (%1, %2, %3, %4, %5, %6, %7, %8, %9)")\
					.arg(DataContainer::toSqlString(first), DataContainer::toSqlString(middle), DataContainer::toSqlString(last),\
						 DataContainer::toSqlString(role), DataContainer::toSqlString(bio),\
						 DataContainer::toSqlString(birth, true), DataContainer::toSqlString(death, true)).arg((int)detail).\
						 arg(DataContainer::toSqlString(editorhints.toString(),true)));
		if(q.lastError().isValid())
		{
			qCritical()<<"FAILED Person insert"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
			return false;
		}
		else
		{
			pk=q.lastInsertId().toInt();
		}
		emit dataChangedSig(pk, PersonAction::CREATED);
	}
	mod=false;
	return true;
}

void Person::deleteInDb()
{
	if(pk<=0)
		return; // not in DB anyway
	QSqlQuery q(QStringLiteral("delete from persons where pk=%1").arg(pk));
	if(q.lastError().isValid())
	{
		qCritical()<<"FAILED Person delete"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
	}
	mod=false;
	pk=-2;
}

QString Person::getName(Person::NameStyle st) const
{
	switch(st)
	{
		case NameStyle::FULL_MASCHINE:
			return (first.size()?(first+"_"):QString(""))+(middle.size()?(middle+"_"):"")+last;
		case NameStyle::FULL_HUMAN:
			return (first.size()?(first+" "):QString(""))+(middle.size()?(middle+" "):"")+last;
		case NameStyle::SEMI_HUMAN:
			return (first.size()?(first+" "):QString(""))+last;
	}
	return (first.size()?(first+"_"):QString(""))+(middle.size()?(middle+"_"):"")+last;
}

void Person::editBioFile()
{
	QString fn;
	if(getFilename().isEmpty())
	{
		QString ns=getName(NameStyle::FULL_MASCHINE);
		//if(ns.isEmpty())
		//{
		//	QMessageBox::warning(DCON.getMainDlg(), tr("Create Biography"), tr("Neither biography filename nor name - can't create file."));
		//	return;
		//}
		ns+=QStringLiteral("_");
		ns+=QString::number(pk);
		ns+=QStringLiteral(".txt");
		if(QMessageBox::question(DCON.getMainDlg(), tr("Create Biography"), tr("Create with default name %1?").arg(ns))==QMessageBox::No)
			return;
		setFilename(ns);
		commit();
		fn=DCON.getBiographyDir().absoluteFilePath(getFilename());
		QFile nf(fn);
		if(nf.open(QIODevice::WriteOnly | QIODevice::NewOnly))
		{
			nf.write(getRole().toUtf8());
			nf.close();
		}
	}
	else
	{
		fn=DCON.getBiographyDir().absoluteFilePath(getFilename());
		if(!QFileInfo::exists(fn) &&
			QMessageBox::question(DCON.getMainDlg(), tr("Create Biography"), tr("The biography file (%1) can't be found - create it?").arg(fn))==QMessageBox::No)
			return;
	}
	editBioFile(fn);
}

bool Person::editBioFile(const QString &bioFile)
{
	QFileInfo fi(bioFile);
	if(fi.exists() && !fi.isFile())
	{
		qCritical()<<"Person::editBioFile it's not a file, maybe dir"<<bioFile;
		return false;
	}
	QProcess p;
	p.setProgram(DCON.getCmdFullEdit());
	p.setArguments(QStringList()<<bioFile);
	if(!p.startDetached())
	{
		qCritical()<<"Person::editBioFile failed"<<bioFile;
		return false;
	}
	return true;
}

void Person::touch()
{
	mod=true;
	/*
	if(!chsigPending)
	{
		chsigPending=true;
		QTimer::singleShot(PERSON_DCSIG_DELAY, this, &Person::dataChangeTrigSl);
	}*/
}

