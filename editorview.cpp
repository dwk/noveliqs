#include "editorview.h"
#include <QtWidgets>
#include <qmath.h>
#include "dwidtimeline.h"
#include "nvli.h"

EditorView::EditorView(QWidget *parent) : QGraphicsView(parent)
{
	setViewport(new QWidget);
	setRenderHint(QPainter::Antialiasing, false);
	setDragMode(QGraphicsView::ScrollHandDrag);
	setOptimizationFlags(QGraphicsView::DontSavePainterState);
	setViewportUpdateMode(QGraphicsView::SmartViewportUpdate);
	setTransformationAnchor(QGraphicsView::NoAnchor);
}

void EditorView::resetIsoZoom()
{
	isoZoom=1.;
	setTransform(QTransform());
}

void EditorView::zoomTo(int fragPk)
{
	zoomTo(DCON.getFragment(fragPk));
}

void EditorView::zoomTo(Fragment *frag)
{
	if(frag)
	{
		centerOn(QPointF(getDWid()->datetime2Coord(frag->getStart()), lastCenterY));
	}
}

void EditorView::saveRefPnt(const QPoint &point)
{
	lastCenterPos=point.isNull()?QPoint(viewport()->width()/2, viewport()->height()/2):point;
	QPointF rp=mapToScene(lastCenterPos);
	lastCenterY=rp.y();
	lastCenterDate=getDWid()->coord2DateTime(rp.x());
	//qDebug()<<"saveRefPnt"<<lastCenterPos<<lastCenterY<<rp.x()<<"->"<<lastCenterDate;
}

void EditorView::recenter()
{
	if(lastCenterDate.isValid())
	{
		QPointF nrp=mapToScene(lastCenterPos);
		double arx=getDWid()->datetime2Coord(lastCenterDate);
		double dx(nrp.x()-arx), dy(nrp.y()-lastCenterY);
		translate(dx, dy);
		//qDebug()<<"recenter newrefPos"<<nrp<<"@"<<lastCenterPos<<"should be"<<getDWid()->datetime2Coord(lastCenterDate)<<"delta"<<dx<<dy;
	}
}

void EditorView::resizeEvent(QResizeEvent *event)
{
	QGraphicsView::resizeEvent(event);
	emit zoomChangedSig(0, event->size().width());
}

#ifndef QT_NO_WHEELEVENT
void EditorView::wheelEvent(QWheelEvent *e)
{
	if (e->modifiers() & Qt::ControlModifier)
	{
		/* int fullWidth=viewport()->width();
		double mousefac=.5;
		if(e->position().x()<fullWidth/3)
			mousefac=0.45;
		else if(e->position().x()>fullWidth*2/3)
			mousefac=0.55;
		saveRefPnt(QPoint(mousefac*fullWidth, viewport()->height()/2)); */
		saveRefPnt(QPoint(std::round(e->position().x()), std::round(e->position().y())));
		//qDebug()<<"lastCenter"<<lastCenterY<<lastCenterDate;
		emit zoomChangedSig(e->angleDelta().y(), size().width());
		e->accept();
		return;
	}
	if (e->modifiers() & Qt::ShiftModifier)
	{
		if(e->angleDelta().y()>0)
			isoZoom*=1.05;
		else
			isoZoom/=1.05;
		QTransform matrix;
		matrix.scale(isoZoom, isoZoom);
		setTransform(matrix);
		e->accept();
		return;
	}
	//translate(100, 10);
	QGraphicsView::wheelEvent(e);
}
#endif

DWidTimeline *EditorView::getDWid()
{
	return qobject_cast<DWidTimeline *>(parent());
}
