#include "fragment.h"
#include <QProcess>
#include <QSqlQuery>
#include <QSqlError>
#include <QTimer>
#include "nvli.h"

#define FRAGMENT_DCSIG_DELAY 333

void Fragment::fromDb()
{
	set<int> pks=DCON.getFragmentPks();
	MainWindow * nv=DCON.getMainDlg();
	//                                 0   1     2      3     4         5         6        7         8         9           10
	QSqlQuery q(QStringLiteral("select pk, file, title, time, location, maturity, comment, sequence, duration, editorhint, function from fragments order by sequence"), DCON.db()->db());
	while(q.next())
	{
		int pk=q.value(0).toInt();
		Fragment *frag=DCON.getFragment(pk, false);
		if(!frag)
		{
			frag=new Fragment(pk, q.value(1).toString(), q.value(2).toString(), q.value(3).toDateTime(), q.value(4).toString(),
								(Maturity)(q.value(5).toInt()), q.value(6).toString(), q.value(7).toInt(), q.value(8).toInt(),
								q.value(9).toString(), (Function)(q.value(10).toInt()), nv);
			DCON.insertFragment(frag, false);
		}
		else
		{
			frag->fromDb(q.value(1).toString(), q.value(2).toString(), q.value(3).toDateTime(), q.value(4).toString(),
						(Maturity)(q.value(5).toInt()), q.value(6).toString(), q.value(7).toInt(), q.value(8).toInt(),
						q.value(9).toString(), (Function)(q.value(10).toInt()));
			DCON.insertFragment(frag, false); // update index
			pks.erase(pk);
		}
	}
	for(auto pit=pks.begin(); pit!=pks.end(); ++pit)
		DCON.deleteFragment(*pit);
	DCON.insertFragment(nullptr); // update min and max date
}

Fragment::Fragment(QObject *parent) : QObject(parent)
{
}

Fragment::Fragment(int pk, QString filename, QString title, QDateTime dt, QString location, Maturity mat, QString comment,
				   int seq, int duration, QString editorhint, Function func, QObject *parent) :
	QObject(parent), pk(pk), filename(filename), title(title), location(location), comment(comment), editorhints(editorhint),
	startDt(dt), mat(mat), func(func), seq(seq), duration(duration)
{
}

bool Fragment::fromDb(QString filename, QString title, QDateTime dt, QString location, Maturity mat, QString comment, int seq,
					  int duration, QString editorhint, Function func)
{
	mod=false;
	modSequ=false;
	if(this->filename!=filename)
	{
		this->filename=filename;
		mod=true;
	}
	if(this->title!=title)
	{
		this->title=title;
		mod=true;
	}
	if(this->startDt!=dt)
	{
		this->startDt=dt;
		mod=true;
	}
	if(this->location!=location)
	{
		this->location=location;
		mod=true;
	}
	if(this->mat!=mat)
	{
		this->mat=mat;
		mod=true;
	}
	if(this->func!=func)
	{
		this->func=func;
		mod=true;
	}
	if(this->comment!=comment)
	{
		this->comment=comment;
		mod=true;
	}
	if(this->seq!=seq)
	{
		this->seq=seq;
		modSequ=true;
	}
	if(this->duration!=duration)
	{
		this->duration=duration;
		mod=true;
	}
	DictCss dc(editorhint);
	if(this->editorhints!=dc)
	{
		this->editorhints=dc;
		mod=true;
	}
	bool ret=mod||modSequ;
	if(mod)
	{
		mod=false;
		emit dataChangedSig(pk, FragmentAction::CHANGED);
	}
	if(modSequ)
	{
		modSequ=false;
		emit dataChangedSig(pk, FragmentAction::SEQCHANGED);
	}
	return ret;
}

bool Fragment::commit(bool signalChange, bool signalSeqChange)
{
	if(pk>0 && !mod && !modSequ)
		return false;
	if(pk>0)
	{
		QSqlQuery q(QStringLiteral("update fragments set file=%2, title=%3, time=%4, location=%5, maturity=%6, comment=%7, sequence=%8, duration=%9, editorhint=%10, function=%11 where pk=%1")\
					.arg(pk).arg(DataContainer::toSqlString(filename), DataContainer::toSqlString(title), DataContainer::toSqlString(startDt.toString(Qt::ISODate)), DataContainer::toSqlString(location)).\
					arg((int)mat).arg(DataContainer::toSqlString(comment)).arg(seq).arg(duration).arg(DataContainer::toSqlString(editorhints.toString())).arg((int)func));
		if(q.lastError().isValid())
		{
			qCritical()<<"FAILED Fragment update"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
			return false;
		}
	}
	else
	{
		QSqlQuery q(QStringLiteral("insert into fragments (file, title, time, location, maturity, function, comment, sequence, duration, editorhint) values (%1, %2, %3, %4, %5, %6, %7, %8, %9, %10)")\
					.arg(DataContainer::toSqlString(filename), DataContainer::toSqlString(title), DataContainer::toSqlString(startDt.toString(Qt::ISODate)), DataContainer::toSqlString(location)).\
					arg((int)mat).arg((int)func).arg(DataContainer::toSqlString(comment)).arg(seq).arg(duration).arg(DataContainer::toSqlString(editorhints.toString())));
		if(q.lastError().isValid())
		{
			qCritical()<<"FAILED Fragment insert"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
			return false;
		}
		else
		{
			pk=q.lastInsertId().toInt();
		}
	}
	if(mod)
	{
		mod=false;
		if(signalChange)
			emit dataChangedSig(pk, FragmentAction::CHANGED);
	}
	if(modSequ)
	{
		modSequ=false;
		if(signalSeqChange)
			emit dataChangedSig(pk, FragmentAction::SEQCHANGED);
	}
	return true;
}

void Fragment::deleteInDb()
{
	if(pk<=0)
		return; // not in DB anyway
	QSqlQuery q(QStringLiteral("delete from fragments where pk=%1").arg(pk));
	if(q.lastError().isValid())
	{
		qCritical()<<"FAILED Fragment delete"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
	}
	mod=false;
	pk=-2;
}

void Fragment::editFile()
{
	QString fn;
	if(getFilename().isEmpty())
	{
		QString ns, nv=getTitle();
		for(int i=0, im=nv.size(); i<im; ++i)
		{
			QChar c=nv.at(i);
			if(c.category()==QChar::Letter_Lowercase ||
				c.category()==QChar::Letter_Uppercase ||
				c.category()==QChar::Number_DecimalDigit ||
				c=='-' || c=='_')
			{
				if(c==u'ö' || c==u'Ö' || c==u'ä' || c==u'Ä' || c==u'ü' || c==u'Ü' || c==u'ß')
					continue;
				ns.append(c);
			}
		}
		ns+=QStringLiteral("_");
		ns+=QString::number(pk);
		ns+=QStringLiteral(".txt");
		if(QMessageBox::question(DCON.getMainDlg(), tr("Create Fragment"), tr("Create with default name %1?").arg(ns))==QMessageBox::No)
			return;
		setFilename(ns);
		commit();
		fn=DCON.getFragmentDir().absoluteFilePath(getFilename());
		QFile nf(fn);
		if(nf.open(QIODevice::WriteOnly | QIODevice::NewOnly))
		{
			nf.write(getComment().toUtf8());
			nf.close();
		}
	}
	else
	{
		fn=DCON.getFragmentDir().absoluteFilePath(getFilename());
		if(!QFileInfo::exists(fn) &&
			QMessageBox::question(DCON.getMainDlg(), tr("Create Fragment"), tr("The fragment file (%1) can't be found - create it?").arg(fn))==QMessageBox::No)
			return;
	}
	editFile(fn);
}

bool Fragment::editFile(const QString &filename)
{
	QFileInfo fi(filename);
	if(fi.exists() && !fi.isFile())
	{
		qCritical()<<"Fragment::editFile it's not a file, maybe dir"<<filename;
		return false;
	}
	QProcess p;
	p.setProgram(DCON.getCmdFullEdit());
	p.setArguments(QStringList()<<filename);
	if(!p.startDetached())
	{
		qCritical()<<"Fragment::editFile failed"<<filename;
		return false;
	}
	return true;
}

/*void Fragment::dataChangeTrigSl()
{
	chsigPending=false;
	emit dataChangedSig(pk);
}*/

void Fragment::touch()
{
	mod=true;
	/*if(!chsigPending)
	{
		chsigPending=true;
		QTimer::singleShot(FRAGMENT_DCSIG_DELAY, this, &Fragment::dataChangeTrigSl);
	}*/
}

