#ifndef DBWRAPPER_H
#define DBWRAPPER_H

#include "nvlh.h"
#include <QObject>
#include <QSqlDatabase>

Q_DECLARE_LOGGING_CATEGORY(dbwrapper)

class DbWrapper : public QObject
{
	Q_OBJECT
public:
	explicit DbWrapper(const QString &dbName, QObject *parent = 0);
	~DbWrapper();
	QSqlDatabase &db() {return db_;}
	bool isOpen() const {return running;}
	QString lastError();
	QString getFilename() const {return dbName;}
	QString getVersion();
	QStringList getAllTableNames() const;
signals:

public slots:
private:
	bool running=false;
	QString dbName;
	QSqlDatabase db_;
};

#endif // DBWRAPPER_H
