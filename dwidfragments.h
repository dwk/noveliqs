#ifndef DWIDFRAGMENTS_H
#define DWIDFRAGMENTS_H

#include "nvlh.h"
#include <QWidget>
#include <QItemSelection>
#include "ifactor.h"

Q_DECLARE_LOGGING_CATEGORY(dwidfragments)

class QTreeWidgetItem;
namespace Ui {
class DWidFragments;
}
class QTableWidgetItem;

class DWidFragments : public QWidget, public IfActor
{
	Q_OBJECT
public:
	explicit DWidFragments(QWidget *parent = 0);
	~DWidFragments();
	// IfActor
	virtual void connectAll(ObjectMSP &otherActors) override;
	virtual void setup() override;
	//
private:
	Fragment *tabRow2Frag(int row=-1);
	int tabRow2pk(int row=-1);
	int tabFrag2Row(int fragPk);
	QColor cellBgColor(Fragment &frag);
	void buttonState(int row, bool multiselect);
	QString tDiffFormat(qint64 diffSec);
private slots:
	// IfActor
	void fragmentSl(int pk, FragmentAction action, QVector<int> pkOther);
	void minMaxDateSl(const QDateTime & start, const QDateTime &end);
	void refDateSl(const QDateTime &refDate);
	//
	void on_treeFrags_itemDoubleClicked(QTableWidgetItem *item);
	void on_treeFrags_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous);
	void treeFrags_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected);
	void on_treeFrags_customContextMenuRequested(const QPoint &pos);
	void on_toolNew_clicked();
	void on_toolUp_clicked();
	void on_toolDown_clicked();
	void on_toolEdit_clicked();
	void on_toolFullEdit_clicked();
	void on_toolDelete_clicked();
	void on_dateRef_dateTimeChanged(const QDateTime &datetime);
	void on_checkEvents_stateChanged(int state);
	void on_checkComments_stateChanged(int state);
	void check_EventsComments(bool allShown = false);
	void timeRefSl();
private:
	int upperVisibleLine(int currentLine);
	int lowerVisibleLine(int currentLine);
	Ui::DWidFragments *ui;
	QFont normFont, itFont, boldFont;
	bool freezeFragSigs=false;
signals:
	void fragmentSig(int pk, FragmentAction action, QVector<int> pkOther); // IfActor
	void refDateSig(const QDateTime & refDate); // IfActor

	/*private slots:
		void on_pushAddSib_clicked();
		void on_pushAddSub_clicked();
		void on_pushAdd(QTreeWidgetItem *parent);
		void on_pushEdit_clicked();
		void on_pushDelete_clicked();
		void on_pushCommit_clicked();
		void tagtreeChangedSl();
		void setDirtySl();
		void updateButtonsSl();
	private:
		typedef std::multimap<int, QTreeWidgetItem *> DeleteItemMMP;
		typedef std::set<QTreeWidgetItem *> SequSP;
		void addSubItems(QTreeWidgetItem *twp, int tagPk, int level);
		void iterateItem(QTreeWidgetItem *item, int sequence, int level, SequSP &sequUpdates, DeleteItemMMP & deleteItems); */

};

#endif // DWIDFRAGMENTS_H
