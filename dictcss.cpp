#include "dictcss.h"
#include "nvli.h"

DictCss::DictCss(QString fromString)
{
	QStringList sl=fromString.split(";", Qt::SkipEmptyParts);
	foreach(QString s, sl)
	{
		int idx=s.indexOf(':');
		if(idx>=0)
		{
			kvs.insert(KvM::value_type(s.left(idx), s.mid(idx+1)));
		}
	}
}

QString DictCss::toString() const
{
	QStringList res;
	QString templ("%1:%2");
	for(auto it=kvs.begin(); it!=kvs.end(); ++it)
		res<<templ.arg(it->first, it->second);
	return res.join(';');
}

QString DictCss::getValue(const QString &property) const
{
	auto it=kvs.find(property);
	if(it==kvs.end())
		return QString();
	return it->second;
}

void DictCss::setValue(const QString &property, const QString &value)
{
	auto it=kvs.find(property);
	if(it==kvs.end())
	{
		if(value.size())
			kvs.insert(KvM::value_type(property, value));
	}
	else
	{
		if(value.size())
			it->second=value;
		else
			kvs.erase(it);
	}
}

bool DictCss::operator==(const DictCss &o) const
{
	if(kvs.size()!=o.kvs.size())
		return false;
	for(auto it1=kvs.begin(), it2=o.kvs.begin(); it1!=kvs.end();)
	{
		if(it1->first!=it2->first || it1->second!=it2->second)
			return false;
		++it1;
		++it2;
	}
	return true;
}
