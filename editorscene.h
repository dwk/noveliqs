#ifndef EDITORSCENE_H
#define EDITORSCENE_H

#include "nvlh.h"
#include <QGraphicsScene>
#include <QGraphicsLineItem>
#include "editorobject.h"
#include "editorfragment.h"
#include "editorperson.h"

class EditorScene : public QGraphicsScene
{
    Q_OBJECT
public:
	EditorScene(QWidget *parent);
	//void setup();
	// timeline
	void addTimeline(double timelineScale);
	EditorTimeline *getTimeline() const {return etl;}
	// frags
	void clearFrags();
	void addFrag(EditorFragment *ef);
	QPointF getFragPos(int fragPk) const;
	EditorFragment *getFrag(int fragPk) const;
	void removeFrag(int fragPk);
	void selectFrag(int fragPk, QVector<int> more);
	void updateFrag(int fragPk);
	// pers
	void clearPers();
	void addPers(EditorPerson *ep);
	void removePers(int persPk);
	void selectPers(int persPk);
	void updatePers(int persPk);
	//
	void rubberBand(const QPointF &start);
	QString scaleChanged(double timelineScale);
public slots:
	void rubberBandCancelSl();
	void perssVisSl(bool checked);
	void perssDecoVisSl(bool checked);
	void fragsVisSl(bool checked);
	void fragsDecoVisSl(bool checked);
protected:
	virtual void keyPressEvent(QKeyEvent *keyEvent);
	virtual void mouseMoveEvent(QGraphicsSceneMouseEvent *mouseEvent);
private:
	QPointF rubberStart;
	QGraphicsLineItem *rubBand=nullptr;
	EditorTimeline *etl=nullptr;
	EditorFragmentMISP editorFrags;
	EditorPersonMISP editorPerss;
signals:
	void rubberBandCancelSig();
};

#endif // EDITORSCENE_H
