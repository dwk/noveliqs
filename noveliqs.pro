QT       += core gui sql

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

INCLUDEPATH += /usr/local/share/boost_1_72_0

CONFIG(release, debug|release):QMAKE_POST_LINK = cp $$OUT_PWD/$$TARGET ~/bin

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

SOURCES += \
    attend.cpp \
    datacontainer.cpp \
    dbwrapper.cpp \
    dictcss.cpp \
    dlgfragment.cpp \
    dlggenerate.cpp \
    dlgperson.cpp \
    dlgprefabout.cpp \
    dockwidfactory.cpp \
    dwidfragments.cpp \
	dwidpersons.cpp \
    dwidtimeline.cpp \
    editorfragment.cpp \
    editorobject.cpp \
    editorperson.cpp \
    editorscene.cpp \
    editorview.cpp \
    fragment.cpp \
    globalenum.cpp \
	ifactor.cpp \
    main.cpp \
    mainwindow.cpp \
#    noveliqs.cpp \
    person.cpp

HEADERS += \
    attend.h \
    datacontainer.h \
    dbwrapper.h \
    dictcss.h \
    dlgfragment.h \
    dlggenerate.h \
    dlgperson.h \
    dlgprefabout.h \
    dockwidfactory.h \
    dwidfragments.h \
    dwidfragments_fac.h \
	dwidpersons.h \
	dwidpersons_fac.h \
    dwidtimeline.h \
    dwidtimeline_fac.h \
    editorfragment.h \
    editorobject.h \
    editorperson.h \
    editorscene.h \
    editorview.h \
    fragment.h \
    globalenum.h \
    ifactor.h \
    mainwindow.h \
#    noveliqs.h \
    nvlh.h \
    nvli.h \
    person.h \
    singleton.h

FORMS += \
    dlgfragment.ui \
    dlggenerate.ui \
    dlgperson.ui \
    dlgprefabout.ui \
    dwidfragments.ui \
	dwidpersons.ui \
    dwidtimeline.ui \
	mainwindow.ui
#    noveliqs.ui

TRANSLATIONS += \
	noveliqs_en_US.ts

# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target

RESOURCES += \
	noveliqs.qrc

DISTFILES += \
	newdb.sql \
	res/icons8-zoom-in-50.png \
	res/icons8-zoom-out-48.png \
	res/icons8-zoom-reset-50.png
