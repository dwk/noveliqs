#include "dlgperson.h"
#include "ui_dlgperson.h"
#include <QFileDialog>
#include <QProcess>
#include "nvli.h"

Q_LOGGING_CATEGORY(dlgperson, "dlgperson")

DlgPerson::DlgPerson(QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgPerson)
{
	ui->setupUi(this);
	ui->labelPk->setText(QStringLiteral("new"));
	enumPersRelevanceComboSetup(ui->comboDetail);
	ui->dateBirth->setDateTime(DCON.getStoryStart());
	ui->dateDeath->setDateTime(DCON.getStoryEnd());
	on_checkNoBirth_stateChanged(0);
	on_checkNoDeath_stateChanged(0);
}

DlgPerson::DlgPerson(Person &pers, QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgPerson), pers(&pers)
{
	ui->setupUi(this);
	ui->labelPk->setText(QString::number(pers.getPk()));
	ui->lineFirst->setText(pers.getFirst());
	ui->lineMiddle->setText(pers.getMiddle());
	ui->lineLast->setText(pers.getLast());
	enumPersRelevanceComboSetup(ui->comboDetail);
	enumPersRelevanceComboSet(ui->comboDetail, pers.getRelevance());
	ui->lineRole->setText(pers.getRole());
	ui->lineBio->setText(pers.getFilename());
	if(pers.getBirth().isValid())
	{
		ui->dateBirth->setDate(pers.getBirth());
		ui->checkNoBirth->setChecked(false);
	}
	else
		ui->checkNoBirth->setChecked(true);
	on_checkNoBirth_stateChanged(0);
	if(pers.getDeath().isValid())
	{
		ui->dateDeath->setDate(pers.getDeath());
		ui->checkNoDeath->setChecked(false);
	}
	else
		ui->checkNoDeath->setChecked(true);
	on_checkNoDeath_stateChanged(0);
}

DlgPerson::~DlgPerson()
{
	delete ui;
}

void DlgPerson::accept()
{
	if(!pers)
	{
		pers=new Person(DCON.getMainDlg());
	}
	pers->setFirst(ui->lineFirst->text());
	pers->setMiddle(ui->lineMiddle->text());
	pers->setLast(ui->lineLast->text());
	pers->setRelevance(enumPersRelevanceComboRead(ui->comboDetail));
	pers->setRole(ui->lineRole->text());
	pers->setFilename(ui->lineBio->text());
	pers->setBirth(ui->checkNoBirth->isChecked()?QDate():ui->dateBirth->date());
	pers->setDeath(ui->checkNoDeath->isChecked()?QDate():ui->dateDeath->date());
	pers->commit();
	DCON.insertPerson(pers); // insert, or update index
	QDialog::accept();
}

void DlgPerson::on_checkNoBirth_stateChanged(int)
{
	//qCDebug(dlgperson)<<ui->checkNoBirth->isChecked();
	ui->dateBirth->setDisabled(ui->checkNoBirth->isChecked());
}

void DlgPerson::on_checkNoDeath_stateChanged(int)
{
	//qCDebug(dlgperson)<<ui->checkNotDeath->isChecked();
	ui->dateDeath->setDisabled(ui->checkNoDeath->isChecked());
}

void DlgPerson::on_toolBio_clicked()
{
	QString fd=DCON.getBiographyDir().absoluteFilePath(ui->lineBio->text());
	QString fn=QFileDialog::getOpenFileName(this, QStringLiteral("Select Biography File"), fd, QStringLiteral("Text file (*.*)"));
	qCDebug(dlgperson)<<"DlgPerson::on_toolBio_clicked"<<fn;
	if(fn.isEmpty())
		return;
	QString base=DCON.getBiographyDir().path();
	if(!fn.startsWith(base))
	{
		QMessageBox::warning(this, tr("Invalid Location"), tr("Biography files must be in or below %1").arg(base));
		return;
	}
	ui->lineBio->setText(fn.mid(base.size()+1));
}

void DlgPerson::on_toolEditBio_clicked()
{
	QString fn=DCON.getBiographyDir().absoluteFilePath(ui->lineBio->text());
	if(!Person::editBioFile(fn))
		QMessageBox::warning(this, tr("Edit Failed"), tr("Cannot edit %1 - invalid file name?").arg(fn));
}


