#ifndef FRAGMENT_H
#define FRAGMENT_H

#include "nvlh.h"
#include <QObject>
#include <QPointer>
#include <QDateTime>
#include "dictcss.h"

class Fragment : public QObject
{
	friend class DataContainer;
	Q_OBJECT
public:
	static void fromDb();
	explicit Fragment(QObject *parent);
	Fragment(int pk, QString filename, QString title, QDateTime dt, QString location, Maturity mat,
			 QString comment, int seq, int duration, QString editorhint, Function func, QObject *parent);
	bool fromDb(QString filename, QString title, QDateTime dt, QString location, Maturity mat, QString comment,
				int seq, int duration, QString editorhint, Function func); // true if changed
	bool commit(bool signalChange=true, bool signalSeqChange=true); // true if db transaction
	void deleteInDb();
	int getPk() const {return pk;}
	QString getFilename() const {return filename;}
	void setFilename(const QString &filename) {if(this->filename!=filename) {this->filename=filename; touch();}}
	QString getTitle() const {return title;}
	void setTitle(const QString &title) {if(this->title!=title) {this->title=title; touch();}}
	QString getLocation() const {return location;}
	void setLocation(const QString &location) {if(this->location!=location) {this->location=location; touch();}}
	QString getComment() const {return comment;}
	void setComment(const QString &comment) {if(this->comment!=comment) {this->comment=comment; touch();}}
	QDateTime getStart() const {return startDt;}
	void setStart(const QDateTime &dt) {if(this->startDt!=dt) {this->startDt=dt; touch();}}
	QDateTime getEnd() const {return startDt.addSecs(duration);}
	void setEnd(const QDateTime &dt) {int newDur=startDt.secsTo(dt); if(this->duration!=newDur) {this->duration=newDur; touch();}}
	int getDurationSec() const {return duration;}
	void setDuration(int duration) {if(this->duration!=duration) {this->duration=duration; touch();}}
	Maturity getMaturity() const {return mat;}
	void setMaturity(Maturity mat) {if(this->mat!=mat) {this->mat=mat; touch();}}
	Function getFunction() const {return func;}
	void setFunction(Function func) {if(this->func!=func) {this->func=func; touch();}}
	int getSequence() const {return seq;}
	void setSequence(int seq) {if(this->seq!=seq) {this->seq=seq; modSequ=true;}}
	void editFile();
	static bool editFile(const QString & filename);
	const DictCss &getEditorhints() const {return editorhints;}
	void setEditorhints(const DictCss &editorhints) {if(this->editorhints!=editorhints) {this->editorhints=editorhints; touch();}}
	QString getEditorhint(const QString &property) const {return editorhints.getValue(property);}
	void setEditorhint(const QString &property, const QString &value) {editorhints.setValue(property, value); touch();}
private slots:
	//void dataChangeTrigSl();
signals:
	void dataChangedSig(int pk, FragmentAction action);
private:
	void touch();
	int pk=-1; // -2 not initialized, -1 new (not in DB), 0 invalid / error, >0 in DB
	bool mod=false, modSequ=false; // modified compared to DB
	bool chsigPending=false;
	QString filename, title, location, comment;
	DictCss editorhints;
	QDateTime startDt;
	Maturity mat=Maturity::DUMMY;
	Function func=Function::FRAGMENT;
	int seq=0, duration=3600; // 1 hour default duration
};
#endif // FRAGMENT_H
