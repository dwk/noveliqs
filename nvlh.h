#ifndef NVLH_H
#define NVLH_H

#include <vector>
#include <list>
#include <map>
#include <set>
#include <QString>
#include <QtGlobal>
#include <QLoggingCategory>

#include "globalenum.h"

#define VERSION_STR "nvl-1.2"
#define UI_DATETIME_FORMAT "yyyy-MM-dd HH:mm:ss ddd"
#define UI_DATE_FORMAT "yyyy-MM-dd"

class QWidget;
class QObject;
typedef std::map< QString, QObject * > ObjectMSP;
class Fragment;
class Person;

#endif // NVLH_H
