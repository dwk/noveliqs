#include "dlgfragment.h"
#include "ui_dlgfragment.h"
#include <QFileDialog>
#include <QTableWidgetItem>
#include <QListWidgetItem>
#include "attend.h"
#include "nvli.h"

Q_LOGGING_CATEGORY(dlgfragment, "dlgfragment")

DlgFragment::DlgFragment(Fragment &frag, bool doEdit, QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgFragment), frag(&frag), editSession(doEdit)
{
	ui->setupUi(this);
	ui->labelPk->setText(editSession?QString::number(frag.getPk()):QStringLiteral("new"));
	enumMaturityComboSetup(ui->comboMaturity);
	enumFunctionComboSetup(ui->comboFunction);
	if(editSession)
	{
		ui->lineFilename->setText(frag.getFilename());
		ui->lineTitle->setText(frag.getTitle());
		enumFunctionComboSet(ui->comboFunction, frag.getFunction());
		on_comboFunction_currentIndexChanged(ui->comboFunction->currentIndex());
		ui->lineLocation->setText(frag.getLocation());
		ui->lineComment->setPlainText(frag.getComment());
		enumMaturityComboSet(ui->comboMaturity, frag.getMaturity());
		on_comboMaturity_currentIndexChanged(ui->comboMaturity->currentIndex());
		ui->dateTimeStart->setDateTime(frag.getStart());
		ui->dateTimeEnd->setDateTime(frag.getEnd());
		currentDur=frag.getDurationSec();
		origPersPks=DCON.getAttendPersPks(frag.getPk());
	}
	else
	{
		Fragment * prevFrag=DCON.getFragment(frag.getSequence()-1, DataContainer::FragmentIndex::SEQ);
		if(prevFrag)
		{
			qint64 dist=prevFrag->getEnd().secsTo(frag.getStart());
			QDateTime bas=prevFrag->getEnd().addSecs((dist-currentDur)/2);
			qDebug()<<objectName()<<"refs"<<prevFrag->getTitle()<<prevFrag->getEnd()<<frag.getTitle()<<frag.getStart()<<dist;
			ui->dateTimeStart->setDateTime(bas);
			ui->dateTimeEnd->setDateTime(bas.addSecs(currentDur));
		}
		else
		{
			QDateTime bas=frag.getStart().addSecs(-2*currentDur);
			ui->dateTimeStart->setDateTime(bas);
			ui->dateTimeEnd->setDateTime(bas.addSecs(currentDur));
		}
	}
	fillDuration();
	if(frag.getFunction()==Function::CHAPTER || frag.getFunction()==Function::EVENT)
	{
		ui->lineFilename->setEnabled(false);
	}
	if(frag.getFunction()!=Function::FRAGMENT && frag.getFunction()!=Function::EVENT)
	{
		ui->dateTimeStart->setEnabled(false);
		ui->dateTimeEnd->setEnabled(false);
	}
	if(frag.getFunction()==Function::CHAPTER || frag.getFunction()==Function::COMMENT)
	{
		ui->tabPers->setVisible(false);
		ui->toolAdd->setVisible(false);
		ui->toolRemove->setVisible(false);
		ui->listPersAct->setVisible(false);
	}
	else
	{
		DCON.iteratePersons(this, SLOT(fillNamesSl(QObject*,int)), QVariant(), DataContainer::PersonIndex::NAME);
		ui->tabPers->resizeColumnsToContents();
		ui->tabPers->resizeRowsToContents();
		ui->toolAdd->setEnabled(false);
		ui->toolRemove->setEnabled(false);
	}
}

DlgFragment::~DlgFragment()
{
	delete ui;
}

void DlgFragment::accept()
{
	Function f=enumFunctionComboRead(ui->comboFunction);
	if(f==Function::CHAPTER || f==Function::EVENT)
	{
		if(!ui->lineFilename->text().isEmpty() && QMessageBox::warning(this, tr("Pending Update"), tr("Filename %1 will be cleared. Proceed?").arg(frag->getFilename()),
							 QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
		{
			return;
		}
		ui->lineFilename->setText("");
	}
	Fragment * saveFrag=frag;
	if(!editSession)
	{
		int newSequ=frag->getSequence();
		frag=new Fragment(DCON.getMainDlg());
		frag->setSequence(newSequ);
	}
	frag->setFilename(ui->lineFilename->text());
	if(f==Function::PROLOG && !DCON.canBeProlog(frag->getSequence()))
	{
		if(QMessageBox::warning(this, tr("Pending Update"), tr("This is not the first printable fragment and cannot be a prolog. Make it a FRAGMENT?"),
							 QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
		{
			if(!editSession)
			{
				delete frag;
				frag=saveFrag;
			}
			return;
		}
		frag->setFunction(Function::FRAGMENT);
	}
	else if(f==Function::EPILOG && !DCON.canBeEpilog(frag->getSequence()))
	{
		if(QMessageBox::warning(this, tr("Pending Update"), tr("This is not the last printable fragment and cannot be an epilog. Make it a FRAGMENT?"),
							 QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::No)
		{
			if(!editSession)
			{
				delete frag;
				frag=saveFrag;
			}
			return;
		}
		frag->setFunction(Function::FRAGMENT);
	}
	else
		frag->setFunction(f);
	frag->setTitle(ui->lineTitle->text());
	frag->setLocation(ui->lineLocation->text());
	frag->setComment(ui->lineComment->toPlainText());
	frag->setStart(ui->dateTimeStart->dateTime());
	frag->setDuration(currentDur);
	frag->setMaturity(enumMaturityComboRead(ui->comboMaturity));
	frag->commit();
	DCON.insertFragment(frag); // insert, or update index
	qCDebug(dlgfragment)<<"attend updates"<<toggleAtts.size();
	for(auto it=toggleAtts.begin(); it!=toggleAtts.end(); ++it)
	{
		qCDebug(dlgfragment)<<it->first<<it->second.add<<it->second.remove<<it->second.visible;
		if(it->second.add)
		{ // insert Attend
			Attend *att=new Attend(frag->getPk(), it->first, it->second.visible?AttendRel::VISIBLE:AttendRel::HIDDEN, DCON.getMainDlg());
			att->commit();
			DCON.insertAttend(att);
		}
		else if(it->second.remove)
		{ // delete Attend
			if(!DCON.deleteAttend(frag->getPk(), it->first, true))
				qCritical()<<"DlgFragment::accept can't find / delete Attend"<<frag->getPk()<<it->first;
		}
		else
		{
			Attend * att=DCON.getAttend(frag->getPk(), it->first);
			if(att)
			{
				att->setRelation(it->second.visible?AttendRel::VISIBLE:AttendRel::HIDDEN);
				att->commit();
			}
		}
	}
	QDialog::accept();
}

void DlgFragment::fillNamesSl(QObject *pers, int row)
{
	Person *p=qobject_cast<Person*>(pers);
	if(!p)
	{
		qCritical()<<"DlgFragment::fillNamesSl not a Person"<<row;
		return;
	}
	auto pit=origPersPks.find(p->getPk());
	int cc=ui->tabPers->rowCount();
	ui->tabPers->setRowCount(cc+1);
	QTableWidgetItem *twi=new QTableWidgetItem(p->getFirst());
	twi->setData(Qt::UserRole, p->getPk());
	ui->tabPers->setItem(cc, 0, twi);
	twi=new QTableWidgetItem(p->getLast());
	ui->tabPers->setItem(cc, 1, twi);
	twi=new QTableWidgetItem(p->getRole());
	ui->tabPers->setItem(cc, 2, twi);
	if(pit!=origPersPks.end() && frag)
	{
		ui->tabPers->hideRow(cc);
		QListWidgetItem *lwi=new QListWidgetItem(p->getName(Person::NameStyle::FULL_HUMAN));
		lwi->setData(Qt::UserRole, p->getPk());
		Attend * att=DCON.getAttend(frag->getPk(), p->getPk());
		if(att)
		{
			lwi->setCheckState( (att->getRealtion()==AttendRel::VISIBLE)?Qt::Checked:Qt::Unchecked );
		}
		ui->listPersAct->addItem(lwi);
	}
}

void DlgFragment::on_toolFileSelect_clicked()
{
	QString fd=DCON.getFragmentDir().absoluteFilePath(ui->lineFilename->text());
	QString fn=QFileDialog::getOpenFileName(this, QStringLiteral("Select Fragment File"), fd, QStringLiteral("Text file (*.*)"));
	qCDebug(dlgfragment)<<"DlgFragment::on_toolFileSelect_clicked"<<fn;
	if(fn.isEmpty())
		return;
	QString base=DCON.getFragmentDir().path();
	if(!fn.startsWith(base))
	{
		QMessageBox::warning(this, tr("Invalid Location"), tr("Fragment files must be in or below %1").arg(base));
		return;
	}
	ui->lineFilename->setText(fn.mid(base.size()+1));
}

void DlgFragment::on_toolFileEdit_clicked()
{
	QString fn=DCON.getFragmentDir().absoluteFilePath(ui->lineFilename->text());
	if(!Fragment::editFile(fn))
		QMessageBox::warning(this, tr("Edit Failed"), tr("Cannot edit %1 - invalid fine name?").arg(fn));
}

void DlgFragment::on_dateTimeStart_dateTimeChanged(const QDateTime &datetime)
{
	ui->dateTimeEnd->setDateTime(datetime.addSecs(currentDur));
}

void DlgFragment::on_dateTimeEnd_editingFinished()
{
	QDateTime datetime=ui->dateTimeEnd->dateTime();
	if(datetime<=ui->dateTimeStart->dateTime())
	{
		QApplication::beep();
		ui->dateTimeEnd->setDateTime(ui->dateTimeStart->dateTime().addSecs(1));
		currentDur=1;
		return;
	}
	currentDur=ui->dateTimeStart->dateTime().secsTo(datetime);
	fillDuration();
}

void DlgFragment::on_timeDuration_editingFinished()
{
	currentDur=ui->timeDuration->time().msecsSinceStartOfDay()/1000;
	ui->dateTimeEnd->setDateTime(ui->dateTimeStart->dateTime().addSecs(currentDur));
}

void DlgFragment::on_radioEnd_clicked()
{
	ui->dateTimeEnd->setEnabled(true);
	ui->timeDuration->setEnabled(false);
}

void DlgFragment::on_radioDuration_clicked()
{
	ui->dateTimeEnd->setEnabled(false);
	ui->timeDuration->setEnabled(true);
}

void DlgFragment::on_comboMaturity_currentIndexChanged(int index)
{
	Q_UNUSED(index)
}

void DlgFragment::on_comboFunction_currentIndexChanged(int index)
{
	Q_UNUSED(index)
	Function f=enumFunctionComboRead(ui->comboFunction);
	switch(f)
	{
	case Function::CHAPTER:
		ui->lineFilename->setText("");
		currentDur=0;
		ui->dateTimeEnd->setDateTime(ui->dateTimeStart->dateTime());
		fillDuration();
		ui->lineFilename->setEnabled(false);
		ui->toolFileSelect->setEnabled(false);
		ui->toolFileEdit->setEnabled(false);
		ui->dateTimeStart->setEnabled(true);
		ui->dateTimeEnd->setEnabled(true);
		ui->tabPers->setVisible(false);
		break;
	case Function::FRAGMENT:
		ui->lineFilename->setEnabled(true);
		ui->toolFileSelect->setEnabled(true);
		ui->toolFileEdit->setEnabled(true);
		ui->dateTimeStart->setEnabled(true);
		ui->dateTimeEnd->setEnabled(true);
		ui->tabPers->setVisible(true);
		break;
	case Function::PROLOG:
		ui->lineFilename->setEnabled(true);
		ui->toolFileSelect->setEnabled(true);
		ui->toolFileEdit->setEnabled(true);
		ui->dateTimeStart->setEnabled(true);
		ui->dateTimeEnd->setEnabled(true);
		ui->tabPers->setVisible(true);
		break;
	case Function::EPILOG:
		ui->lineFilename->setEnabled(true);
		ui->toolFileSelect->setEnabled(true);
		ui->toolFileEdit->setEnabled(true);
		ui->dateTimeStart->setEnabled(true);
		ui->dateTimeEnd->setEnabled(true);
		ui->tabPers->setVisible(true);
		break;
	case Function::EVENT:
		ui->lineFilename->setText("");
		ui->lineFilename->setEnabled(false);
		ui->toolFileSelect->setEnabled(false);
		ui->toolFileEdit->setEnabled(false);
		ui->dateTimeStart->setEnabled(true);
		ui->dateTimeEnd->setEnabled(true);
		ui->tabPers->setVisible(true);
		break;
	case Function::COMMENT:
		ui->lineFilename->setEnabled(true);
		ui->toolFileSelect->setEnabled(true);
		ui->toolFileEdit->setEnabled(true);
		ui->dateTimeStart->setEnabled(false);
		ui->dateTimeEnd->setEnabled(false);
		ui->tabPers->setVisible(false);
		break;
	}
}

void DlgFragment::on_tabPers_itemSelectionChanged()
{
	QList<QTableWidgetItem *> sels=ui->tabPers->selectedItems();
	ui->toolAdd->setEnabled(sels.size());
}

void DlgFragment::on_listPersAct_itemSelectionChanged()
{
	QList<QListWidgetItem *> sels=ui->listPersAct->selectedItems();
	ui->toolRemove->setEnabled(sels.size());
}

void DlgFragment::on_listPersAct_itemChanged(QListWidgetItem *item)
{
	Person *p=DCON.getPerson(item->data(Qt::UserRole).toInt());
	if(!p)
	{
		qCritical()<<"change: no person for pk"<<item->data(Qt::UserRole).toInt()<<item->text();
		return;
	}
	auto it=toggleAtts.find(p->getPk());
	if(it!=toggleAtts.end())
	{
		it->second.visible=(item->checkState()==Qt::Checked);
	}
	else
	{
		AttData ad={.add=false, .remove=false, .visible=(item->checkState()==Qt::Checked)};
		toggleAtts.insert(pair<int, AttData>(p->getPk(), ad));
	}
}

void DlgFragment::on_toolAdd_clicked()
{
	ui->toolAdd->setEnabled(false);
	QList<QTableWidgetItem *> sels=ui->tabPers->selectedItems();
	foreach(QTableWidgetItem * twi, sels)
	{
		if(twi->column()!=0 || ui->tabPers->isRowHidden(twi->row()))
			continue;
		ui->tabPers->hideRow(twi->row());
		Person *p=DCON.getPerson(twi->data(Qt::UserRole).toInt());
		if(!p)
		{
			qCritical()<<"add: no person for pk"<<twi->data(Qt::UserRole).toInt()<<twi->row();
			continue;
		}
		auto it=toggleAtts.find(p->getPk());
		if(it!=toggleAtts.end())
		{
			if(it->second.add)
				qCritical()<<"toggleAtts: added while add==true"<<frag->getPk()<<editSession<<p->getPk();
			else
				it->second.remove=false; // maybe we have to update the visibility
		}
		else
		{
			AttData ad={.add=true, .remove=false, .visible=true};
			toggleAtts.insert(pair<int, AttData>(p->getPk(), ad));
		}
		QListWidgetItem *lwi=new QListWidgetItem(p->getName(Person::NameStyle::FULL_HUMAN));
		lwi->setData(Qt::UserRole, p->getPk());
		lwi->setCheckState(Qt::Checked);
		ui->listPersAct->addItem(lwi);
	}
}

void DlgFragment::on_toolRemove_clicked()
{
	ui->toolRemove->setEnabled(false);
	QList<QListWidgetItem *> sels=ui->listPersAct->selectedItems();
	std::set<int> pkstoshow;
	foreach(QListWidgetItem * lwi, sels)
	{
		int ppk=lwi->data(Qt::UserRole).toInt();
		if(!ppk)
		{
			qCritical()<<"remove: no person pk for"<<lwi->text();
			continue;
		}
		delete lwi;
		auto it=toggleAtts.find(ppk);
		if(it!=toggleAtts.end())
		{
			if(it->second.add)
				toggleAtts.erase(it); // just undo the add
			else if(it->second.remove)
				qCritical()<<"toggleAtts: removed while remove==true"<<frag->getPk()<<editSession<<ppk;
			else
				it->second.remove=true;
		}
		else
		{
			AttData ad={.add=false, .remove=true, .visible=false};
			toggleAtts.insert(pair<int, AttData>(ppk, ad));
		}
		pkstoshow.insert(ppk);
	}
	for(int i=0, im=ui->tabPers->rowCount(); i<im; ++i)
	{
		if(pkstoshow.find(ui->tabPers->item(i, 0)->data(Qt::UserRole).toInt())!=pkstoshow.end())
		{
			ui->tabPers->showRow(i);
		}
	}
}

void DlgFragment::fillDuration()
{
	if(currentDur>86399 || enumFunctionComboRead(ui->comboFunction)==Function::CHAPTER)
	{
		ui->radioEnd->setChecked(true);
		ui->radioDuration->setEnabled(false);
		ui->timeDuration->setTime(QTime(0,0));
		ui->timeDuration->setEnabled(false);
	}
	else
	{
		ui->radioDuration->setEnabled(true);
		int h=currentDur/3600;
		int s=(currentDur-h*3600);
		ui->timeDuration->setTime(QTime(h, s/60, s%60));
	}
}
