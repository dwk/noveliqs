#ifndef DWIDFRAGMENTS_FAC_H
#define DWIDFRAGMENTS_FAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidFragments(parent);
}
const char *techname="DWidFragments";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Fragments", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDFRAGMENTS_FAC_H
