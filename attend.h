#ifndef ATTEND_H
#define ATTEND_H

#include "nvlh.h"
#include <QObject>
#include <QPointer>

class Attend : public QObject
{
	friend class DataContainer;
	Q_OBJECT
public:
	static void fromDb();
	explicit Attend(int fragPk, int persPk, AttendRel rel, QObject *parent);
	Attend(int pk, int fragPk, int persPk, AttendRel rel, QObject *parent);
	bool commit(); // true if db transaction
	void deleteInDb();
	int getPk() const {return pk;}
	int getFragPk() const {return fragPk;}
	int getPersPk() const {return persPk;}
	AttendRel getRealtion() const {return rel;}
	void setRelation(AttendRel rel) {if(this->rel!=rel) {this->rel=rel; mod=true;}}
signals:
private:
	int pk=-1; // -2 not initialized, -1 new (not in DB), 0 invalid / error, >0 in DB
	int fragPk=-1, persPk=-1;
	AttendRel rel;
	bool mod=false; // modified compared to DB
};
#endif // ATTEND_H
