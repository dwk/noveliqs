#ifndef DWIDPERSONS_H
#define DWIDPERSONS_H

#include "nvlh.h"
#include <QWidget>
#include "ifactor.h"

Q_DECLARE_LOGGING_CATEGORY(dwidpersons)

namespace Ui {
class DWidPersons;
}
class QTableWidgetItem;

class DWidPersons : public QWidget, public IfActor
{
	Q_OBJECT
public:
	explicit DWidPersons(QWidget *parent = 0);
	~DWidPersons();
	// IfActor
	virtual void connectAll(ObjectMSP &otherActors) override;
	virtual void setup() override;
	//
private:
	Person *tabRow2Pers(int row=-1);
	int tabPers2Row(int persPk);
	QColor role2Color(Person &pers);
	void buttonState(int row);
private slots:
	// IfActor
	void personSl(int pk, PersonAction action, int pkOther);
	//
	void on_tabPerson_itemDoubleClicked(QTableWidgetItem *item);
	void on_tabPerson_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous);
	void on_tabPerson_customContextMenuRequested(const QPoint &pos);
	void on_toolNew_clicked();
	void on_toolEdit_clicked();
	void on_toolFullEdit_clicked();
	void on_toolDelete_clicked();
	void refDateSl();
	void selFragsSl();
private:
	Ui::DWidPersons *ui;
	QFont itFont;
	bool freezePersSigs=false;
signals:
	void personSig(int pk, PersonAction action, int pkOther); // IfActor
	void refDateSig(const QDateTime & refDate); // IfActor
	void fragmentSig(int pk, FragmentAction action, QVector<int> pkOther); // IfActor
};

#endif // DWIDPERSONS_H
