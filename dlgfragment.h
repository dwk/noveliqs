#ifndef DLGFRAGMENT_H
#define DLGFRAGMENT_H

#include "nvlh.h"
#include <QDialog>
#include "fragment.h"

Q_DECLARE_LOGGING_CATEGORY(dlgfragment)

class QListWidgetItem;

namespace Ui {
class DlgFragment;
}

class DlgFragment : public QDialog
{
	Q_OBJECT

public:
	DlgFragment(Fragment &frag, bool doEdit, QWidget *parent);
	~DlgFragment();
	Fragment *getFragment() {return frag;}
public slots:
	virtual void accept();
	void fillNamesSl(QObject *pers, int row);
private slots:
	void on_toolFileSelect_clicked();
	void on_toolFileEdit_clicked();
	void on_dateTimeStart_dateTimeChanged(const QDateTime &datetime);
	void on_dateTimeEnd_editingFinished();
	void on_timeDuration_editingFinished();
	void on_radioEnd_clicked();
	void on_radioDuration_clicked();
	void on_comboMaturity_currentIndexChanged(int index);
	void on_comboFunction_currentIndexChanged(int index);
	void on_tabPers_itemSelectionChanged();
	void on_listPersAct_itemSelectionChanged();
	void on_listPersAct_itemChanged(QListWidgetItem *item);
	void on_toolAdd_clicked();
	void on_toolRemove_clicked();
private:
	struct AttData{bool add, remove, visible;};
	void fillDuration();
	Ui::DlgFragment *ui;
	Fragment *frag=nullptr;
	bool editSession=true;
	int currentDur=3600;
	std::set<int> origPersPks;
	std::map<int, AttData> toggleAtts;
};

#endif // DLGFRAGMENT_H
