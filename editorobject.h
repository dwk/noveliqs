#ifndef EDITOROBJECT_H
#define EDITOROBJECT_H
#include "nvlh.h"
#include <QPointer>
#include <QGraphicsItem>
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QFont>
#include <QDateTime>

Q_DECLARE_LOGGING_CATEGORY(editorobject)

class DWidTimeline;

class EditorLabel : public QGraphicsItem
{
	Q_INTERFACES(QGraphicsItem)
public:
	enum class Alignment {VCENT, TOP, BOT};
	EditorLabel(const QString &text, const QFont & font, Alignment align, QGraphicsItem *parent);
	virtual ~EditorLabel() {}
	void setText(const QString & newText);
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE {return boundingBox;}
	virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	//
protected:
	// QGraphicsItem
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;
	double border=2.;
	QColor baseColor;
	QPen outlinePen;
	QFont font;
	QRectF boundingBox, body;
	Alignment align;
	QString text;
};

class EditorSymbol : public QGraphicsItem
{
	Q_INTERFACES(QGraphicsItem)
public:
	enum class Symbol {STAR, CROSS, RING};
	EditorSymbol(EditorSymbol::Symbol sym, double size, QGraphicsItem *parent);
	virtual ~EditorSymbol() {}
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE {return boundingBox;}
	virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	//
	EditorSymbol::Symbol getSymbolType() const {return symbol;}
protected:
	// QGraphicsItem
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;
	Symbol symbol;
	QColor baseColor;
	QPen outlinePen;
	QRectF boundingBox;
	QPainterPath body;
};


class EditorTimeline : public QObject, public QGraphicsItem
{
	Q_OBJECT
	Q_INTERFACES(QGraphicsItem)
	struct ZoomLevel
	{
		enum class AdjustType {NONE=0, DAY, WEEK, MONTH, YEAR};
		ZoomLevel(QString desc, double maxPixPerDay, qint64 addSecs, qint64 addDays, qint64 addMonths, qint64 addYears) :
			desc(desc), maxPixPerDay(maxPixPerDay), addSecs(addSecs), addDays(addDays), addMonths(addMonths), addYears(addYears) {}
		void adjustDown(QDateTime &val);
		void adjustUp(QDateTime &val);
		void incr(QDateTime &val);
		QString desc; // display name
		double maxPixPerDay=1.; // up to which pixel per day zoom level to be used
		qint64 addSecs=0, addDays=0, addMonths=0, addYears=0; // increment per step
		int fillMlt=0; // steps per fill, 0 is no fill
		QBrush *br=nullptr;
		AdjustType adjustType=AdjustType::DAY;
		bool showTime=false;
	};
	typedef std::vector<ZoomLevel> ZoomLevelVV;
public:
	EditorTimeline(double timelineScale, QObject *parent);
	virtual ~EditorTimeline() {}
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE {return body;}
	virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	//
	QString scaleChanged(double timelineScale);
public slots:
	void minMaxDateSl(const QDateTime &minDate_, const QDateTime &maxDate_);
	void refDateSl(const QDateTime &refDate);
protected:
	DWidTimeline * getDWid() const;
	void calcBody();
	double gridHeight=400., gridBorder=5.;
	QPen outlinePen;
	QBrush gradDay, gradYear;
	QRectF body;
	QLineF refLine;
	QDateTime minDate, maxDate, refDate;
	double txoff=-1.;
	double timelineScale;
	ZoomLevelVV zoomLevels;
	int currentLevel=0;
};

#endif // EDITOROBJECT_H
