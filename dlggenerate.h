#ifndef DLGGENERATE_H
#define DLGGENERATE_H

#include "nvlh.h"
#include <QDialog>
#include <QFile>
#include <QTextCharFormat>
#include <QDateTime>

Q_DECLARE_LOGGING_CATEGORY(dlggenerate)

namespace Ui {
class DlgGenerate;
}

class DlgGenerate : public QDialog
{
	Q_OBJECT
public:
	explicit DlgGenerate(QWidget *parent = nullptr);
	~DlgGenerate();
public slots:
	void fragIterSl(QObject *obj, int index, IterateObjectHint objectHint, QVariant hint);
private slots:
	void on_pushStart_clicked();
private:
	enum class LineAtt {LOW, NORMAL, HIGHBAD, HIGHGOOD};
	enum class LineSrc {HEADER1, HEADER2, INFO, OUTPUT};
	typedef std::map<int, QString> IndexMap;
	bool readTemplate(const QString &filename, const QStringList &tags, IndexMap &map, QString &templ);
	void addLine(QString line, LineSrc src=LineSrc::INFO, LineAtt att=LineAtt::NORMAL);
	void add2Book(Fragment *f);
	Ui::DlgGenerate *ui;
	QTextCharFormat dtf;
	QString templBook, templFrag, templPro, templEpi;
	IndexMap mapBook, mapFrag, mapPro, mapEpi;
	QFile *fbook=nullptr;
	IndexMap::iterator bookIter;
	int bookBase=0;
	QDateTime lastFragEnd;
};

#endif // DLGGENERATE_H
