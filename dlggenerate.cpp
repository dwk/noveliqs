#include "dlggenerate.h"
#include "ui_dlggenerate.h"
#include <QProcess>
#include <QFile>
#include <QFontDatabase>
#include <QLocale>
#include "nvli.h"

Q_LOGGING_CATEGORY(dlggenerate, "dlggenerate")

DlgGenerate::DlgGenerate(QWidget *parent) :
	QDialog(parent), ui(new Ui::DlgGenerate)
{
	ui->setupUi(this);
	dtf=ui->textProgress->currentCharFormat();
	addLine(QStringLiteral("LaTeX Generator"), LineSrc::HEADER1);
	bool ready=true;
	QStringList tags;
	tags<<"$AUTHOR$"<<"$BOOKTITLE$"<<"$PROLOG$"<<"$FRAGMENTS$"<<"$EPILOG$";
	if(!readTemplate(QStringLiteral("book_template.tex"), tags, mapBook, templBook))
		ready=false;
	tags.clear();
	tags<<"$TITLE$"<<"$LOCATION$"<<"$TIMESTART$"<<"$ACTORLIST$"<<"$TEXT$";
	if(!readTemplate(QStringLiteral("frag_template.tex"), tags, mapFrag, templFrag))
		ready=false;
	if(!readTemplate(QStringLiteral("prolog_template.tex"), tags, mapPro, templPro))
		ready=false;
	if(!readTemplate(QStringLiteral("epilog_template.tex"), tags, mapEpi, templEpi))
		ready=false;
	ui->pushStart->setEnabled(ready);
}

DlgGenerate::~DlgGenerate()
{
	delete ui;
}

void DlgGenerate::fragIterSl(QObject *obj, int index, IterateObjectHint objectHint, QVariant hint)
{
	Q_UNUSED(objectHint)
	Q_UNUSED(hint)
	Fragment * f=qobject_cast<Fragment*>(obj);
	if(!f || !fbook)
	{
		qCritical()<<"fragIterSl not a Fragment or no fbook"<<index;
		return;
	}
	Function func=f->getFunction();
	switch(func)
	{
	case Function::CHAPTER:
		fbook->write(QString("\\chapter{%1}\n").arg(f->getTitle()).toUtf8());
		addLine(tr("chapter '%1'...").arg(f->getTitle()), LineSrc::INFO, LineAtt::NORMAL);
		return;
	case Function::FRAGMENT:
		if(f->getMaturity()==Maturity::FULLTEXT || f->getMaturity()==Maturity::RAWTEXT || f->getMaturity()==Maturity::PARTTEXT)
			break; // add the complete text to book -> more work
		addLine(tr("fragment '%1' skipped, matruity %2 too low").arg(f->getTitle(), enumMaturityStr(f->getMaturity())), LineSrc::INFO, LineAtt::NORMAL);
		return;
	case Function::PROLOG:
	case Function::EPILOG:
		return; // these are handled directly from the book template
	case Function::EVENT:
	case Function::COMMENT:
		addLine(tr("conceptual %2 '%1' skipped, no source").arg(f->getTitle()).arg(enumFunctionStr(func)), LineSrc::INFO, LineAtt::NORMAL);
		return;
	}
	add2Book(f);
}

void DlgGenerate::on_pushStart_clicked()
{
	addLine(QStringLiteral("Preprocessor"), LineSrc::HEADER2);
	lastFragEnd = QDateTime();
	QString fb=DCON.getOutputDir().absoluteFilePath(QStringLiteral("book.tex"));
	fbook=new QFile(fb);
	if(!fbook->open(QIODevice::WriteOnly | QIODevice::Truncate))
	{
		addLine(tr("ERROR Cannot open output %1").arg(fb), LineSrc::INFO, LineAtt::HIGHBAD);
		return;
	}
	for(bookBase=0, bookIter=mapBook.begin(); bookIter!=mapBook.end(); ++bookIter)
	{
		fbook->write(templBook.mid(bookBase, bookIter->first-bookBase).toUtf8());
		bookBase=bookIter->first;
		if(bookIter->second==QStringLiteral("$AUTHOR$"))
		{
			addLine(tr("adding author %1").arg(DCON.getBookAuthor()), LineSrc::INFO, LineAtt::NORMAL);
			fbook->write(DCON.getBookAuthor().toUtf8());
		}
		else if(bookIter->second==QStringLiteral("$BOOKTITLE$"))
		{
			addLine(tr("adding title %1").arg(DCON.getBookTitle()), LineSrc::INFO, LineAtt::NORMAL);
			fbook->write(DCON.getBookTitle().toUtf8());
		}
		else if(bookIter->second==QStringLiteral("$PROLOG$"))
		{
			Fragment *pro=DCON.getProlog();
			if(!pro)
				continue;
			add2Book(pro);
		}
		else if(bookIter->second==QStringLiteral("$EPILOG$"))
		{
			Fragment *epi=DCON.getEpilog();
			if(!epi)
				continue;
			add2Book(epi);
		}
		else if(bookIter->second==QStringLiteral("$FRAGMENTS$"))
		{
			DCON.iterateFragments(this, SLOT(fragIterSl(QObject*,int,IterateObjectHint,QVariant)), QVariant(), DataContainer::FragmentIndex::SEQ);
		}
	}
	fbook->write(templBook.mid(bookBase).toUtf8());
	fbook->close();
	delete fbook;
	fbook=nullptr;
	addLine(QStringLiteral("LaTeX"), LineSrc::HEADER2);
	// run something like ~/Dropbox/Resonanz/build$ pdflatex -output-directory=./ -interaction=batchmode ./book
	QProcess proc;
	proc.setProgram(DCON.getCmdLatex());
	proc.setArguments(QStringList()<<QStringLiteral("-output-directory=%1").arg(DCON.getOutputDir().absolutePath())\
					  <<QStringLiteral("-interaction=nonstopmode")\
					  <<fb);
	QProcessEnvironment env = QProcessEnvironment::systemEnvironment();
	env.insert("max_print_line", "1000");
	proc.setProcessEnvironment(env);
	addLine(DCON.getCmdLatex()+proc.arguments().join(" "), LineSrc::INFO, LineAtt::LOW);
	bool doloop=false;
	proc.start(QIODevice::ReadWrite);
	do
	{
		if(proc.waitForFinished(5000))
		{
			bool anyError=false, skipLine=false, hideUnderfull=ui->checkHideUnderfull->isChecked();
			QRegularExpression re("\\d+\\.\\d*");
			QString lineTempl("%1> %2");
			int lineNumber=0;
			double overfullLimit=ui->spinOverfiul->value();
			while(proc.canReadLine())
			{
				++lineNumber;
				QString ot=QString::fromUtf8(proc.readLine(10000)).trimmed();
				if(ot.isEmpty())
					continue;
				if(skipLine)
				{
					skipLine=false;
					continue;
				}
				if(ot.indexOf(QStringLiteral("! LaTeX Error:"))>=0)
				{
					addLine(lineTempl.arg(lineNumber).arg(ot), LineSrc::OUTPUT, LineAtt::HIGHBAD);
					anyError=true;
				}
				else if(hideUnderfull && ot.indexOf(QStringLiteral("Underfull"))>=0)
					continue;
				else if(overfullLimit>0. && ot.startsWith(QStringLiteral("Overfull")))
				{
					QRegularExpressionMatch match = re.match(ot);
					if(match.hasMatch())
					{
						bool ok=false;
						double ov=ot.midRef(match.capturedStart(0), match.capturedEnd(0)-match.capturedStart(0)).toDouble(&ok);
						if(ok && ov<=overfullLimit)
						{
							skipLine=true;
							continue;
						}
					}
					addLine(lineTempl.arg(lineNumber).arg(ot), LineSrc::OUTPUT, LineAtt::NORMAL);
				}
				else
					addLine(lineTempl.arg(lineNumber).arg(ot), LineSrc::OUTPUT, LineAtt::NORMAL);
			}
			if(anyError)
				addLine(tr("LaTeX seems to complain. See output above for details. (TOC not updated!)"), LineSrc::INFO, LineAtt::HIGHBAD);
			else
			{ // seems to be OK, so start it a second time for TOC
				proc.start(QIODevice::ReadWrite);
				proc.waitForFinished();
				addLine(tr("generation completed"), LineSrc::INFO, LineAtt::HIGHGOOD);
			}
			if(ui->checkOpenPdf->isChecked())
			{
				QProcess procPdf;
				procPdf.setProgram(DCON.getCmdPdfViewer());
				procPdf.setArguments(QStringList()<<DCON.getOutputDir().absoluteFilePath(QStringLiteral("book.pdf")));
				procPdf.startDetached();
			}
		}
		else
		{
			if(proc.error()!=QProcess::Timedout)
			{
				addLine(QString::fromUtf8(proc.readAll()), LineSrc::OUTPUT);
				addLine(tr("generation failed"), LineSrc::INFO, LineAtt::HIGHBAD);
				qCritical()<<"QProcess error"<<proc.error();
			}
			else
			{
				addLine(QString::fromUtf8(proc.readAll()), LineSrc::OUTPUT);
				if(QMessageBox::question(this, tr("Wait?"), tr("It seems to take a while. Shall I kill the process?"), QMessageBox::Yes | QMessageBox::No, QMessageBox::No)==QMessageBox::Yes)
					addLine(tr("generation terminated on user request"), LineSrc::INFO, LineAtt::HIGHBAD);
				else
					doloop=true;
			}
		}
	}
	while(doloop);
}

bool DlgGenerate::readTemplate(const QString &filename, const QStringList &tags, DlgGenerate::IndexMap &map, QString &templ)
{
	QString ff=DCON.getGeneratorDir().absoluteFilePath(filename);
	QFile file(ff);
	if(file.open(QIODevice::ReadOnly))
	{
		QString templFragRaw=file.readAll();
		addLine(tr("%1: %2 chars").arg(filename).arg(templFragRaw.size()), LineSrc::INFO, LineAtt::LOW);
		IndexMap fragmapRaw;
		foreach(QString tag, tags)
		{
			int idx=templFragRaw.indexOf(tag);
			//qDebug()<<tag<<idx;
			if(idx<0)
				qWarning()<<filename<<"template does not use"<<tag;
			else
				fragmapRaw.insert(IndexMap::value_type(idx, tag));
		}
		int base=0;
		for(auto fit=fragmapRaw.begin(); fit!=fragmapRaw.end(); ++fit)
		{
			//qDebug()<<templFrag<<templFragRaw.mid(base, fit->first-base);
			templ+=templFragRaw.mid(base, fit->first-base);
			//qDebug()<<templFrag.size()<<fit->second;
			map.insert(IndexMap::value_type(templ.size(), fit->second));
			base=fit->first+fit->second.size();
		}
		templ+=templFragRaw.mid(base);
		return true;
	}
	addLine(tr("ERROR: no template %1").arg(ff), LineSrc::INFO, LineAtt::HIGHBAD);
	return false;
}

void DlgGenerate::addLine(QString line, DlgGenerate::LineSrc src, DlgGenerate::LineAtt att)
{
	if(src==LineSrc::INFO && att==LineAtt::NORMAL)
	{
		ui->textProgress->append(line);
		return;
	}
	QTextCharFormat tf=dtf;
	switch(src)
	{
	case LineSrc::HEADER1:
		//qDebug()<<"addLine header point size"<<dtf.font().pointSizeF()*1.7;
		tf.setFontPointSize(dtf.font().pointSizeF()*1.7);
		tf.setFontWeight(QFont::Bold);
		break;
	case LineSrc::HEADER2:
		tf.setFontPointSize(dtf.font().pointSizeF()*1.4);
		tf.setFontWeight(QFont::Bold);
		break;
	case LineSrc::INFO:
		break;
	case LineSrc::OUTPUT:
		tf.setFont(QFontDatabase::systemFont(QFontDatabase::FixedFont));
		break;
	}
	switch(att)
	{
	case LineAtt::LOW:
		tf.setForeground(QBrush(Qt::gray));
		break;
	case LineAtt::NORMAL:
		break;
	case LineAtt::HIGHBAD:
		tf.setForeground(QBrush(Qt::darkRed));
		break;
	case LineAtt::HIGHGOOD:
		tf.setForeground(QBrush(Qt::darkGreen));
		break;
	}
	ui->textProgress->setCurrentCharFormat(tf);
	ui->textProgress->append(line);
	ui->textProgress->setCurrentCharFormat(dtf);
}

void DlgGenerate::add2Book(Fragment *f)
{
	addLine(tr("adding %1 %2").arg(enumFunctionStr(f->getFunction()), f->getTitle()), LineSrc::INFO, LineAtt::NORMAL);
	if(f->getFilename().isEmpty())
	{
		addLine(tr("%2 '%1' has no source!").arg(f->getTitle()).arg(enumFunctionStr(f->getFunction())), LineSrc::INFO, LineAtt::HIGHBAD);
		return;
	}
	QString ff=DCON.getFragmentDir().absoluteFilePath(f->getFilename());
	QString fo=DCON.getOutputDir().absoluteFilePath(f->getFilename()+QStringLiteral(".tex"));
	QFile ffrag(ff);
	QFile ffrago(fo);
	if(!ffrag.open(QIODevice::ReadOnly) || !ffrago.open(QIODevice::WriteOnly))
	{
		qDebug()<<"DlgGenerate::fragIterSl input file"<<ffrag.errorString();
		qDebug()<<"DlgGenerate::fragIterSl output file"<<ffrago.errorString();
		addLine(tr("ERROR transferring fragment '%1'").arg(f->getTitle()), LineSrc::INFO, LineAtt::HIGHBAD);
		return;
	}
	IndexMap * map=nullptr;
	QString *templ=nullptr;
	switch(f->getFunction())
	{
	case Function::FRAGMENT:
		map=&mapFrag;
		templ=&templFrag;
		break;
	case Function::PROLOG:
		map=&mapPro;
		templ=&templPro;
		break;
	case Function::EPILOG:
		map=&mapEpi;
		templ=&templEpi;
		break;
	default:
		break;
	}
	if(!templ)
	{
		addLine(tr("NO template %1 %2").arg(enumFunctionStr(f->getFunction()), f->getTitle()), LineSrc::INFO, LineAtt::HIGHBAD);
		return;
	}
	fbook->write(QString("\\input{%1}\n").arg(fo).toUtf8());
	int idxBase=0;
	for(auto fit=map->begin(); fit!=map->end(); ++fit)
	{
		ffrago.write(templ->mid(idxBase, fit->first-idxBase).toUtf8());
		idxBase=fit->first;
		QByteArray ba;
		if(fit->second==QStringLiteral("$TITLE$"))
			ba=f->getTitle().toUtf8();
		else if(fit->second==QStringLiteral("$LOCATION$"))
			ba=f->getLocation().toUtf8();
		else if(fit->second==QStringLiteral("$TIMESTART$"))
		{
			ba.clear();
			if(lastFragEnd.isValid())
			{
				qint64 delta=lastFragEnd.secsTo(f->getStart());
				delta/=60; // we talk only about minutes
				QStringList its;
				QString preText, postText;
				if(delta>-5 && delta<5)
				{
					its<<tr("zur gleichen Zeit");
					delta=0;
				}
				else if (delta<=-5)
				{
					preText=tr("vor ");
					delta*=-1;
				}
				else
					postText=tr(" später");
				int years=delta/381600; // one year has 381600 minutes
				delta-=years*381600;
				int days=delta/1440;
				delta-=days*1440;
				int hours=delta/60;
				delta-=hours*60;
				// delta is now true minutes
				if(years)
				{
					hours=0;
					delta=0;
					if(years==1)
						its<<tr("ein Jahr");
					else
						its<<tr("%1 Jahre").arg(years);
				}
				if(days)
				{
					delta=0;
					if(days==1)
						its<<tr("ein Tag");
					else
						its<<tr("%1 Tage").arg(days);
				}
				if(delta)
					its<<tr("%1H%2").arg(hours, 2, 10, QChar('0')).arg(delta, 2, 10, QChar('0'));
				else if(hours)
					its<<tr("%1H").arg(hours, 2, 10, QChar('0'));
				ba+=(preText + its.join(QStringLiteral(", ")) + postText + QStringLiteral("; ")).toUtf8();
			}
			ba+=QLocale(QLocale::German).toString(f->getStart(), UI_DATETIME_FORMAT).toUtf8();
			lastFragEnd=f->getEnd();
		}
		else if(fit->second==QStringLiteral("$ACTORLIST$"))
		{
			std::set<int> persPks=DCON.getAttendPersPks(f->getPk());
			QStringList acts;
			for(auto it=persPks.begin(); it!=persPks.end(); ++it)
			{
				Attend *att=DCON.getAttend(f->getPk(), *it);
				if(!att || att->getRealtion()==AttendRel::HIDDEN)
					continue;
				Person *p=DCON.getPerson(*it);
				if(p)
					acts<<p->getName(Person::NameStyle::SEMI_HUMAN);
			}
			ba=acts.join(", ").toUtf8();
		}
		else if(fit->second==QStringLiteral("$TEXT$"))
		{
			ba=ffrag.readAll();
		}
		if(ba.size())
			ffrago.write(ba);
		else
		{
			if(templ->mid(idxBase, 2)==QStringLiteral("\\\\"))
				idxBase+=2; // omit a forced newline for empty blocks, would provoke a LaTeX error
		}
	}
	//qDebug()<<"trailer"<<idxBase<<templFrag.mid(idxBase);
	ffrago.write(templ->mid(idxBase).toUtf8());
	ffrag.close();
	ffrago.close();
}

