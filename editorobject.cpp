#include "editorobject.h"
#include <QPainter>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include <QLinearGradient>
#include "dwidtimeline.h"
#include "qmath.h"
#include "nvli.h"

Q_LOGGING_CATEGORY(editorobject, "editorobject")

EditorLabel::EditorLabel(const QString &text, const QFont &font, Alignment align, QGraphicsItem *parent) :
	QGraphicsItem(parent), baseColor(200, 200, 200), outlinePen(Qt::black), font(font), align(align), text(text)
{
	setFlags(QGraphicsItem::ItemIsSelectable); //QGraphicsItem::ItemIgnoresTransformations |
	setAcceptHoverEvents(true);
	QFontMetrics fm(font);
	body=fm.boundingRect(text).adjusted(0,0,2.*border,2.*border);
	boundingBox=body.adjusted(-1.,-1.,3.,3.);
	double y=0.;
	qDebug()<<body;
	switch(align)
	{
	case Alignment::VCENT: y=-(body.bottom()+body.top())/2.; break;
	case Alignment::TOP: y=-boundingBox.top(); break;
	case Alignment::BOT: y=-boundingBox.bottom(); break;
	}
	setPos(0., y);
	setZValue(2000);
}

void EditorLabel::setText(const QString &newText)
{
	text=newText;
	prepareGeometryChange();
	QFontMetrics fm(font);
	body=fm.boundingRect(text).adjusted(0,0,2.*border,2.*border);
	boundingBox=body.adjusted(-1.,-1.,3.,3.);
	update();
}

QPainterPath EditorLabel::shape() const
{
	QPainterPath path;
	path.addRect(body);
	return path;
}

void EditorLabel::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget)
	painter->save();
	painter->setPen(outlinePen);
	if (option->state & QStyle::State_MouseOver)
	{
		//painter->setBrush(QBrush(baseColor.lighter(110)));
		painter->setBrush(QBrush(baseColor));
	}
	else
		painter->setBrush(QBrush());
	painter->drawRect(body);
	painter->setFont(font);
	painter->drawText(border, border, text);
	painter->restore();
}

QVariant EditorLabel::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change==ItemSelectedChange)
	{
		QGraphicsItem *p=parentItem();
		qDebug()<<"EditorLabel::itemChange"<<change<<value.toBool()<<p;
		if(p)
		{
			p->setSelected(value.toBool());
			return QVariant(); // no call to base class, label shall not select itself
		}
	}
	return QGraphicsItem::itemChange(change, value);
}

// --------------

EditorSymbol::EditorSymbol(EditorSymbol::Symbol sym, double size, QGraphicsItem *parent) :
	QGraphicsItem(parent), symbol(sym)
{
	setFlags(QGraphicsItem::ItemIsSelectable); // QGraphicsItem::ItemIgnoresTransformations |
	setAcceptHoverEvents(true);
	setZValue(-900);
	switch(symbol)
	{
	case Symbol::STAR:
	{
		body.moveTo(size, 0.);
		for(int i=1; i<5; ++i)
		{
			body.lineTo(size*std::cos(0.8*i*M_PI), size*std::sin(0.8*i*M_PI));
		}
		body.closeSubpath();
		boundingBox=body.boundingRect();
		baseColor.setRgb(255, 220, 0);
		outlinePen=QPen(baseColor);
		break;
	}
	case Symbol::CROSS:
	{
		body.moveTo(size, 0.);
		body.lineTo(-size, 0.);
		body.moveTo(0., size);
		body.lineTo(0., -size);
		boundingBox=QRectF(-1.5*size, -1.5*size, 3.*size, 3.*size);
		baseColor.setRgb(60, 60, 60);
		outlinePen=QPen(baseColor);
		outlinePen.setWidth((int)(size/2.));
		break;
	}
	case Symbol::RING:
	{
		body.addEllipse(-size, -size, 2.*size, 2.*size);
		boundingBox=QRectF(-1.5*size, -1.5*size, 3.*size, 3.*size);
		baseColor.setRgb(60, 60, 120);
		outlinePen=QPen(baseColor);
		outlinePen.setWidth((int)(size/2.));
		break;
	}
	}
	setPos(0., 0.);
}

QPainterPath EditorSymbol::shape() const
{
	QPainterPath path;
	path.addRect(boundingBox);
	return path;
}

void EditorSymbol::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget)
	painter->save();
	painter->setPen(outlinePen);
	if(symbol==Symbol::RING)
		painter->setBrush(QBrush());
	else
	{
		if (option->state & QStyle::State_MouseOver)
		{
			painter->setBrush(QBrush(baseColor.lighter(110)));
		}
		else
		{
			painter->setBrush(QBrush(baseColor));
		}
	}
	painter->drawPath(body);
	painter->restore();
}

QVariant EditorSymbol::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change==ItemSelectedChange)
	{
		QGraphicsItem *p=parentItem();
		qDebug()<<"EditorLabel::itemChange"<<change<<value.toBool()<<p;
		if(p)
		{
			p->setSelected(value.toBool());
			return QVariant(); // no call to base class, label shall not select itself
		}
	}
	return QGraphicsItem::itemChange(change, value);
}

// --------------

EditorTimeline::EditorTimeline(double timelineScale, QObject * parent) :
	QObject(parent), outlinePen(Qt::black), timelineScale(timelineScale)
{
	QLinearGradient lg(QPointF(0., 0.), QPointF(1., 0.));
	lg.setColorAt(0., QColor(160, 200, 235));
	lg.setColorAt(.35, QColor(255, 245, 200));
	lg.setColorAt(.5, QColor(255, 255, 240));
	lg.setColorAt(.65, QColor(255, 245, 200));
	lg.setColorAt(.8, QColor(210, 220, 255));
	lg.setColorAt(1., QColor(160, 200, 235));
	lg.setCoordinateMode(QGradient::ObjectMode);
	gradDay=QBrush(lg);
	QLinearGradient lg2(QPointF(0., 0.), QPointF(1., 0.));
	lg2.setColorAt(0., QColor(180, 220, 255));
	lg2.setColorAt(.25, QColor(210, 255, 160));
	lg2.setColorAt(.5, QColor(255, 255, 200));
	lg2.setColorAt(.75, QColor(250, 190, 130));
	lg2.setColorAt(1., QColor(180, 220, 255));
	lg2.setCoordinateMode(QGradient::ObjectMode);
	gradYear=QBrush(lg2);
	{
		ZoomLevel zl(tr("day"), 200., 21600L, 0L, 0L, 0L);
		zl.fillMlt=4; // day spans 4 ticks
		zl.br=&gradDay;
		zl.showTime=true;
		zoomLevels.push_back(zl);
	}
	{
		ZoomLevel zl(tr("days"), 40., 0L, 1L, 0L, 0L);
		zl.fillMlt=1;
		zl.br=&gradDay;
		zoomLevels.push_back(zl);
	}
	{
		ZoomLevel zl(tr("weeks"), 5., 0L, 7L, 0L, 0L);
		zl.adjustType=ZoomLevel::AdjustType::WEEK;
		zoomLevels.push_back(zl);
	}
	{
		ZoomLevel zl(tr("months"), .8, 0L, 0L, 1L, 0L);
		zl.adjustType=ZoomLevel::AdjustType::YEAR;
		zl.fillMlt=12;
		zl.br=&gradYear;
		zoomLevels.push_back(zl);
	}
	{
		ZoomLevel zl(tr("years"), .08, 0L, 0L, 0L, 1L);
		zl.adjustType=ZoomLevel::AdjustType::YEAR;
		zl.fillMlt=1;
		zl.br=&gradYear;
		zoomLevels.push_back(zl);
	}
	{
		ZoomLevel zl(tr("decades"), .01, 0L, 0L, 0L, 10L);
		zl.adjustType=ZoomLevel::AdjustType::YEAR;
		zoomLevels.push_back(zl);
	}
	{
		ZoomLevel zl(tr("centuries"), .001, 0L, 0L, 0L, 100L);
		zl.adjustType=ZoomLevel::AdjustType::YEAR;
		zoomLevels.push_back(zl);
	}
	setPos(0., 0.);
	setFlags(QGraphicsItem::ItemUsesExtendedStyleOption);
	setZValue(-1000);
	// provided through IfActor / dwidtimeline connect(DCON.self(), &DataContainer::minMaxDateSig, this, &EditorTimeline::minMaxDateSl);
	minMaxDateSl(DCON.getStoryStart(), DCON.getStoryEnd());
}

QPainterPath EditorTimeline::shape() const
{
	QPainterPath path;
	path.addRect(body);
	return path;
}

void EditorTimeline::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget)
	painter->save();
	painter->setPen(outlinePen);
	if(txoff<0.)
		txoff=(double)(painter->fontMetrics().boundingRect('X').height()+2);
	ZoomLevel &zl=zoomLevels.at(currentLevel);
	double minx=mapToScene(option->exposedRect.bottomLeft()).x();
	QDateTime dtx(getDWid()->coord2Date(minx), QTime(0,0));
	if(dtx<minDate)
		dtx=minDate;
	else
		zl.adjustDown(dtx);
	double maxx=mapToScene(option->exposedRect.bottomRight()).x();
	//qDebug()<<"EditorTimeline starting"<<dtx<<enumZoomLevelStr(zl)<<cnt;
	double x=0., xSaved=numeric_limits<double>::quiet_NaN();
	std::map<double, QDateTime> lxs;
	bool lastOne=false, lastOneDone=false;
	for(int i=0; !lastOneDone && i<100; ++i) // 100 is safety margin, should never be reached for reasonably wide viewports
	{
		x=getDWid()->datetime2Coord(dtx);
		if(!lastOne && x>maxx)
			lastOne=true;
		if(zl.br && zl.fillMlt && !(i%zl.fillMlt))
		{
			if(!isnan(xSaved) && x>=minx)
			{
				// QRectF(it->first, option->exposedRect.top(), 20., option->exposedRect.height())
				painter->fillRect(QRectF(xSaved, -gridHeight, x-xSaved, gridHeight), *zl.br);
				lastOneDone=lastOne;
			}
			xSaved=x;
		}
		lxs.insert(std::map<double, QDateTime>::value_type(x, dtx));
		zl.incr(dtx);
	}
	for(auto it=lxs.begin(); it!=lxs.end(); ++it)
	{
		painter->drawLine(QLineF(it->first, -gridHeight, it->first, 0.));
	}
	if(!refLine.isNull())
	{
		painter->setPen(Qt::red);
		painter->drawLine(refLine);
		painter->setPen(Qt::black);
	}
	painter->rotate(-90.);
	for(auto it=lxs.begin(); it!=lxs.end(); ++it)
	{
		painter->drawText(0., it->first+txoff, it->second.toString(zl.showTime?UI_DATETIME_FORMAT:UI_DATE_FORMAT));
	}
	painter->restore();
}

QString EditorTimeline::scaleChanged(double timelineScale)
{
	prepareGeometryChange();
	this->timelineScale=timelineScale;
	currentLevel=0;
	for(; currentLevel<(int)zoomLevels.size()-1 && zoomLevels.at(currentLevel).maxPixPerDay>timelineScale; ++currentLevel)
		;
	ZoomLevel &zl=zoomLevels.at(currentLevel);
	// changed timelineScale may affect "rounding" of dates
	minDate=DCON.getStoryStart();
	maxDate=DCON.getStoryEnd();
	zl.adjustDown(minDate);
	zl.adjustUp(maxDate);
	calcBody();
	if(refDate.isValid())
	{
		double c=getDWid()->datetime2Coord(refDate);
		refLine=QLineF(c, 0., c, -gridHeight);
	}
	else
		refLine=QLineF();
	update();
	return zl.desc;
}

void EditorTimeline::minMaxDateSl(const QDateTime &minDate_, const QDateTime &maxDate_)
{
	prepareGeometryChange();
	minDate=minDate_;
	maxDate=maxDate_;
	ZoomLevel &zl=zoomLevels.at(currentLevel);
	zl.adjustDown(minDate);
	zl.adjustUp(maxDate);
	calcBody();
	update();
}

void EditorTimeline::refDateSl(const QDateTime &refDate)
{
	if(this->refDate<minDate || refDate<minDate)
		prepareGeometryChange();
	this->refDate=refDate;
	if(refDate.isValid())
	{
		double c=getDWid()->datetime2Coord(refDate);
		refLine=QLineF(c, 0., c, -gridHeight);
	}
	else
		refLine=QLineF();
	calcBody();
	update();
}

DWidTimeline *EditorTimeline::getDWid() const
{
	return qobject_cast<DWidTimeline *>(parent()->parent()); // EditorTimeline -> EditorScene -> DWidTimeline
}

void EditorTimeline::calcBody()
{
	double minC=getDWid()->datetime2Coord(minDate);
	if(refDate.isValid())
	{
		minC=min(minC, getDWid()->datetime2Coord(refDate));
	}
	double maxC=getDWid()->datetime2Coord(maxDate);
	body=QRectF(minC-gridBorder,-gridHeight-gridBorder, maxC-minC+2.*gridBorder, gridHeight+2.*gridBorder);
	//qDebug()<<"timeline body"<<body;
}

void EditorTimeline::ZoomLevel::adjustDown(QDateTime &val)
{
	switch(adjustType)
	{
	case ZoomLevel::AdjustType::NONE:
		break;
	case ZoomLevel::AdjustType::DAY:
		val.setTime(QTime(0,0));
		break;
	case ZoomLevel::AdjustType::WEEK:
	{
		val.setTime(QTime(0,0));
		int cd=val.date().dayOfWeek();
		val=val.addDays(1-cd); // move to first of week
		break;
	}
	case ZoomLevel::AdjustType::MONTH:
	{
		val.setTime(QTime(0,0));
		int cd=val.date().day();
		val=val.addDays(1-cd); // move to first of month
		break;
	}
	case ZoomLevel::AdjustType::YEAR:
	{
		val.setTime(QTime(0,0));
		int cd=val.date().dayOfYear();
		val=val.addDays(1-cd); // move to first day of year
		break;
	}
	}
}

void EditorTimeline::ZoomLevel::adjustUp(QDateTime &val)
{
	adjustDown(val);
	switch(adjustType)
	{
	case ZoomLevel::AdjustType::NONE:
		break;
	case ZoomLevel::AdjustType::DAY:
		val=val.addDays(1);
		break;
	case ZoomLevel::AdjustType::WEEK:
	{
		val=val.addDays(7);
		break;
	}
	case ZoomLevel::AdjustType::MONTH:
	{
		val=val.addMonths(1);
		break;
	}
	case ZoomLevel::AdjustType::YEAR:
	{
		val=val.addYears(1);
		break;
	}
	}
}

void EditorTimeline::ZoomLevel::incr(QDateTime &val)
{
	if(addSecs)
		val=val.addSecs(addSecs);
	if(addDays)
		val=val.addDays(addDays);
	if(addMonths)
		val=val.addMonths(addMonths);
	if(addYears)
		val=val.addYears(addYears);
}
