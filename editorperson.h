#ifndef EDITORPERSON_H
#define EDITORPERSON_H
#include "nvlh.h"
#include <QPointer>
#include <QGraphicsItem>
#include <QColor>
#include <QBrush>
#include <QPen>
#include <QFont>
#include <QPainterPath>
#include "editorobject.h"

Q_DECLARE_LOGGING_CATEGORY(editorperson)

class DWidTimeline;
class EditorScene;

class EditorPerson : public QObject, public QGraphicsItem
{
	Q_OBJECT
	Q_INTERFACES(QGraphicsItem)
public:
	static const int objectHeaderHeight=10;
	EditorPerson(int persPk, QObject *parent);
	virtual ~EditorPerson();
	// QGraphicsItem
	virtual QRectF boundingRect() const Q_DECL_OVERRIDE;
	virtual QPainterPath shape() const Q_DECL_OVERRIDE;
	virtual void paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget) Q_DECL_OVERRIDE;
	//
	int getPk() const;
	virtual void setupGraphic();
	void fastRepos();
	void setDecoVisbility(bool visible);
public slots:
	void persChangeSl();
protected:
	// QGraphicsItem
	virtual void contextMenuEvent(QGraphicsSceneContextMenuEvent *event) Q_DECL_OVERRIDE;
	virtual QVariant itemChange(GraphicsItemChange change, const QVariant &value) Q_DECL_OVERRIDE;
	virtual void mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mousePressEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	virtual void mouseReleaseEvent(QGraphicsSceneMouseEvent *event) Q_DECL_OVERRIDE;
	//
private slots:
	void editObjSl();
private:
	typedef std::vector<QPointF> Assigns;
	DWidTimeline * getDWid() const;
	void createLifeline(EditorScene *es);
	// graphic representation
	static double unknownBirthOffset;
	QColor baseColor;
	QBrush backgroundBrush;
	QPen normalPen, overPen, selPen, seloverPen;
	QPainterPath lifeLine;
	Assigns assigns;
	QRectF boundingBox; //, body;
	bool wasMoved=false, doSetup=true;
	EditorLabel *el=nullptr;
	EditorSymbol *birthStar=nullptr, *deathCross=nullptr;
	Person *p=nullptr;
signals:
	void selectedSig(int fragPk, bool selected);
	//void connectionReposSig(void * node);
};
typedef std::map<int, QPointer<EditorPerson> > EditorPersonMISP; // key is pk

#endif // EDITORPERSON_H
