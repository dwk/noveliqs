#ifndef DOCKWIDFACTORY_H
#define DOCKWIDFACTORY_H

#include "nvlh.h"
#include <QMainWindow>

Q_DECLARE_LOGGING_CATEGORY(dockwidfactory)

class DockWidFactory
{
public:
	typedef QWidget * (*DockWidCreator)(QWidget *);
	struct DWInfo
	{
		DWInfo(QString & guiName, QString & techName, Qt::DockWidgetAreas allowedAreas, Qt::DockWidgetArea defaultArea, DockWidCreator factory) :
			guiName(guiName), techName(techName), allowedAreas(allowedAreas), defaultArea(defaultArea), factory(factory) {}
		QString guiName, techName;
		Qt::DockWidgetAreas allowedAreas;
		Qt::DockWidgetArea defaultArea;
		DockWidCreator factory;
	};
	static DockWidFactory & get() {if(inst) return *inst; inst=new DockWidFactory(); return *inst;}
	bool registerDockWid(QString guiName, QString techName, Qt::DockWidgetAreas allowedAreas, Qt::DockWidgetArea defaultArea, DockWidCreator factory);
	DWInfo * produce(const QString & techName);
	QStringList allDockWids();
private:
	typedef std::map< QString, DWInfo> DWInfoMV;
	static DockWidFactory * inst;
	DWInfoMV dwInfoMV;
};
#endif // DOCKWIDFACTORY_H
