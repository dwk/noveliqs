#ifndef DLGPREFABOUT_H
#define DLGPREFABOUT_H

#include "nvlh.h"
#include <QDialog>

Q_DECLARE_LOGGING_CATEGORY(dlgprefabout)

namespace Ui {
class DlgPrefAbout;
}

class DlgPrefAbout : public QDialog
{
	Q_OBJECT

public:
	explicit DlgPrefAbout(QWidget *parent = nullptr);
	~DlgPrefAbout();
public slots:
	virtual void accept();
private slots:
	void on_toolDb_clicked();
	void on_toolFragPath_clicked();
	void on_toolBioPath_clicked();
	void on_toolGeneratorPath_clicked();
	void on_toolOutputPath_clicked();
	//void on_tool_clicked();
	//void on_tool_clicked();
private:
	QString toRelPath(const QString &p);
	Ui::DlgPrefAbout *ui;
	bool pathsChanged=false;
};

#endif // DLGPREFABOUT_H
