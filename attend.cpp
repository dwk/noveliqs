#include "attend.h"
#include <QSqlQuery>
#include <QSqlError>
#include "nvli.h"

void Attend::fromDb()
{
	// here we assume that there are no old entries - always insert
	MainWindow * nv=DCON.getMainDlg();
	//                                 0   1         2       3
	QSqlQuery q(QStringLiteral("select pk, fragment, person, relation from attends"), DCON.db()->db());
	while(q.next())
	{
		int pk=q.value(0).toInt();
		//Attend *at=DCON.getAttend(pk, false);
		Attend *att=new Attend(pk, q.value(1).toInt(), q.value(2).toInt(), (AttendRel)q.value(3).toInt(), nv);
		DCON.insertAttend(att);
	}
}

Attend::Attend(int fragPk, int persPk, AttendRel rel, QObject *parent) :
	QObject(parent), fragPk(fragPk), persPk(persPk), rel(rel)
{
}

Attend::Attend(int pk, int fragPk, int persPk,AttendRel rel,  QObject *parent) :
	QObject(parent), pk(pk), fragPk(fragPk), persPk(persPk), rel(rel)
{
}

bool Attend::commit()
{
	if(pk>0 && !mod)
		return false;
	if(pk>0)
	{
		QSqlQuery q(QStringLiteral("update attends set fragment=%2, person=%3, relation=%4 where pk=%1")\
					.arg(pk).arg(fragPk).arg(persPk).arg((int)rel));
		if(q.lastError().isValid())
		{
			qCritical()<<"FAILED Attned update"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
			return false;
		}
	}
	else
	{
		QSqlQuery q(QStringLiteral("insert into attends (fragment, person, relation) values (%1, %2, %3)")\
					.arg(fragPk).arg(persPk).arg((int)rel));
		if(q.lastError().isValid())
		{
			qCritical()<<"FAILED Attned insert"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
			return false;
		}
		else
		{
			pk=q.lastInsertId().toInt();
		}
	}
	mod=false;
	return true;
}

void Attend::deleteInDb()
{
	if(pk<=0)
		return; // not in DB anyway
	QSqlQuery q(QStringLiteral("delete from attends where pk=%1").arg(pk));
	if(q.lastError().isValid())
	{
		qCritical()<<"FAILED Attend delete"<<q.lastError().databaseText()<<q.lastError().driverText()<<q.lastQuery();
	}
	mod=false;
	pk=-2;
}

