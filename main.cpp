#include "mainwindow.h"

#include <QApplication>

// DCON is the lifecycle- and change-master of all DB Objects (Fragment, Person, Attend): never use e.g. Fragemnt::dataChangedSig directly,
// this will be forwarded by DCON

// TODO bugs
//  * Fragment vor ausgeblendetetm Event selktieren - neues Fragment. ursprünglich selektiertes F verschwindet. In der DB passt es.
//  * cannot reprodice - crash: Add a person to a frag, then move the liveline of the newly added Person in the timeline window - maybe only for persons which were not visible in the timeline before
//  * new fragments added with wrong sequence? (at the end? neighbouring a hidden line? somewhere commit missing?)
// TODO feature
//  * DWidPersons: Action "scenes of this actor" -> distinguish between active part and just mentioned
//  * Fragment-Farbe in DWidTimeline nach Status, Form nach Typ
//  * DWidFragments: replace background color with symbol
//  * DWidFragments: add fragment size as column
//  * DWidTimeline: Vorders und hinteres Ende der Lebenslinie in verschiedener Höhe; verbiegbare Linien = Stuetzstellen
//  * add formatting to the scribble area
int main(int argc, char *argv[])
{
	QCoreApplication::setOrganizationName("dwk");
	QCoreApplication::setOrganizationDomain("effektlict.at");
	QCoreApplication::setApplicationName("noveliqs");
	QApplication a(argc, argv);
	QLoggingCategory::setFilterRules(QStringLiteral(
//			"*=false\n"
//			"datacontainer=false\n"
//			"dlgprefabout=false\n"
//			"dbwrapper=false\n"
//			"dlgfragment=false\n"
//			"dlgperson=false\n"
//			"editorobject=false\n"
			));
	MainWindow w;
	if(w.doAbort())
		return 0;
	w.show();
	return a.exec();
}
