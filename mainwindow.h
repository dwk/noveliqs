#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "nvlh.h"
#include <QMainWindow>
#include <QSignalMapper>
#include "globalenum.h"

Q_DECLARE_LOGGING_CATEGORY(mainwindow)

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();
	bool doAbort() const {return abort;}
public slots:
protected:
	virtual void closeEvent(QCloseEvent *event);
private slots:
	void about();
	void generateSl();
private:
	Ui::MainWindow *ui;
	ObjectMSP actors;
	QSignalMapper *tagMapper;
	bool abort=false;
};

#endif // MAINWINDOW_H
