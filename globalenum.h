#ifndef GLOBALENUM_H
#define GLOBALENUM_H

#include <QObject>
#include <QComboBox>

enum class FragmentAction {
				CREATED=1,	//
				DELETED,	//
				CHANGED,	// the data content has changed (pk will never change and sequence has its own action)
				SEQCHANGED, // the sequnce number changed; if otherPk is valid this indicates a swap of these two; otherPk==-1 indicates "does not chnage effective sequence"
				ATTENTION,	// move attention (selection, emphasis) to this fragment, otherPk points to the fragment losing the attention
				ALLGONE		// full reset, all fragments deleted (both PKs==-1)
				};
Q_DECLARE_METATYPE(FragmentAction)
QString enumFragmentActionStr(FragmentAction ia);
/*
	switch(action)
	{
	case FragmentAction::CREATED:
		break;
	case FragmentAction::DELETED:
		break;
	case FragmentAction::CHANGED:
		break;
	case FragmentAction::SEQCHANGED:
		break;
	case FragmentAction::ATTENTION:
		break;
	case FragmentAction::ALLGONE:
		break;
	}
*/

enum class PersonAction {
				CREATED=1,	//
				DELETED,	//
				CHANGED,	// the data content has changed (pk will never change and sequence has its own action)
				ATTENTION,	// move attention (selection, emphasis) to this fragment, otherPk points to the fragment losing the attention
				ALLGONE		// full reset, all persons deleted
						};
Q_DECLARE_METATYPE(PersonAction)
QString enumPersonActionStr(PersonAction ia);
/*
	switch(action)
	{
	case PersonAction::CREATED:
		break;
	case PersonAction::DELETED:
		break;
	case PersonAction::CHANGED:
		break;
	case PersonAction::ATTENTION:
		break;
	case PersonAction::ALLGONE:
		break;
	}
*/

enum class AttendAction {
				CREATED=1,	//
				DELETED,	//
				ALLGONE		// full reset, all attends deleted
						};
Q_DECLARE_METATYPE(AttendAction)
QString enumAttendActionStr(AttendAction ia);
/*
	switch(action)
	{
	case AttendAction::CREATED:
		break;
	case AttendAction::DELETED:
		break;
	case AttendAction::ALLGONE:
		break;
	}
*/

enum class AttendRel {
				VISIBLE=1,	// used in timeline and printed in final text
				HIDDEN		// not shown in final text but used in timeline
						};
Q_DECLARE_METATYPE(AttendRel)
QString enumAttendRelStr(AttendRel rel);
/*
	switch(rel)
	{
	case AttendRel::VISIBLE:
		break;
	case AttendRel::HIDDEN:
		break;
	}
*/

enum class Maturity {DUMMY=1,	// just a placeholder
					KEYWORDS,	// ideas, rough outline
					CONTENT,	// Sketch of content but not a full text
					PARTTEXT,	// full text in progress
					RAWTEXT,	// full text completed but not fine tuned
					FULLTEXT 	// provread full text
					};
Q_DECLARE_METATYPE(Maturity)
QString enumMaturityStr(Maturity ia);
void enumMaturityComboSetup(QComboBox *cb);
void enumMaturityComboSet(QComboBox *cb, Maturity m);
Maturity enumMaturityComboRead(QComboBox *cb);
/*
	switch(action)
	{
	case Maturity::DUMMY:
		break;
	case Maturity::KEYWORDS:
		break;
	case Maturity::CONTENT:
		break;
	case Maturity::PARTTEXT:
		break;
	case Maturity::RAWTEXT:
		break;
	case Maturity::FULLTEXT:
		break;
	}
*/

//                           story start/end?	book?	text?	timel.?
enum class Function {CHAPTER=1,		// (yes)	yes		no		yes		a chapter heading
					FRAGMENT,		// yes		yes		yes		yes 	a section of text below a chapter
					PROLOG,			// no		yes		yes		no
					EPILOG,			// no		yes		yes		no
					EVENT,			// no		no		no		yes		something visible in the timeline only
					COMMENT			// no		no		yes		no		neither visible in timeline nor book but with text file associated
					};
Q_DECLARE_METATYPE(Function)
QString enumFunctionStr(Function ia);
void enumFunctionComboSetup(QComboBox *cb);
void enumFunctionComboSet(QComboBox *cb, Function m);
Function enumFunctionComboRead(QComboBox *cb);
/*
	switch(function)
	{
	case Function::CHAPTER:
		break;
	case Function::FRAGMENT:
		break;
	case Function::PROLOG:
		break;
	case Function::EPILOG:
		break;
	case Function::EVENT:
		break;
	case Function::COMMENT:
		break;
	}
*/

enum class PersRelevance {BACKGROUND=1,	// just a name
					MINOR,				// person with slightly more than a name
					SUPPORT,			// role supporting the story, established biography
					STAR				// major role, essential for story, full biography
						};
Q_DECLARE_METATYPE(PersRelevance)
QString enumPersRelevanceStr(PersRelevance ia);
void enumPersRelevanceComboSetup(QComboBox *cb);
void enumPersRelevanceComboSet(QComboBox *cb, PersRelevance m);
PersRelevance enumPersRelevanceComboRead(QComboBox *cb);

enum class IterateObjectHint {PROLOG=1,	//
					FRAGMENT,			//
					EPILOG,				//
					PERSON				//
						};
Q_DECLARE_METATYPE(IterateObjectHint)
QString enumIterateObjectHintStr(IterateObjectHint io);

/*
enum class ZoomLevel {
	DAY=1,
	DAYS,
	MONTH,
	YEAR,
	DECADE,
	CENTURY
};
Q_DECLARE_METATYPE(ZoomLevel)
QString enumZoomLevelStr(ZoomLevel zl);
void enumZoomLevelComboSetup(QComboBox *cb);
void enumZoomLevelComboSet(QComboBox *cb, ZoomLevel zl);
ZoomLevel enumZoomLevelComboRead(QComboBox *cb);
	switch()
	{
	case ZoomLevel::DAY:
		break;
	case ZoomLevel::DAYS:
		break;
	case ZoomLevel::MONTH:
		break;
	case ZoomLevel::YEAR:
		break;
	case ZoomLevel::DECADE:
		break;
	case ZoomLevel::CENTURY:
		break;
	}
*/

#endif // GLOBALENUM_H
