#include "dwidfragments.h"
#include "dwidfragments_fac.h"
#include "ui_dwidfragments.h"
#include <QSqlQuery>
#include <QSqlError>
#include <QProcess>
#include <QFontDatabase>
#include <QMenu>
#include "dlgfragment.h"
#include "nvli.h"

Q_LOGGING_CATEGORY(dwidfragments, "dwidfragments")

DWidFragments::DWidFragments(QWidget *parent) :
	QWidget(parent), ui(new ::Ui::DWidFragments)
{
	setObjectName(techname);
	ui->setupUi(this);
	ui->treeFrags->setContextMenuPolicy(Qt::CustomContextMenu);
	QFontDatabase fdb;
	normFont=QFontDatabase::systemFont(QFontDatabase::GeneralFont);
	itFont=fdb.font(normFont.family(), QStringLiteral("Italic"), normFont.pointSize());
	boldFont=fdb.font(normFont.family(), QStringLiteral("Bold"), normFont.pointSize()*1.2);
	connect(ui->treeFrags->selectionModel(), &QItemSelectionModel::selectionChanged, this, &DWidFragments::treeFrags_selectionChanged);
}

DWidFragments::~DWidFragments()
{
	delete ui;
}

void DWidFragments::connectAll(ObjectMSP &otherActors)
{
	qDebug()<<"connectAll"<<objectName();
	for(auto it=otherActors.begin(); it!=otherActors.end(); ++it)
	{
		if(it->second->objectName()==objectName())
			continue; // don't connect to objects of your own type
		/*if(obj->objectName()=="000_DataContainer")
		{
			for(int i=0; i<mo->methodCount(); ++i)
			{
				QMetaMethod mm=mo->method(i);
				qDebug()<<mm.typeName()<<mm.name()<<mm.methodSignature();
			}
		}*/
		IfActor::connectX(it->second, IfActor::fragmentSigNorm, this, IfActor::fragmentSlNorm);
		//IfActor::connectX(it->second, IfActor::attendSigNorm, this, IfActor::attendSlNorm); no need yet
		IfActor::connectX(it->second, IfActor::minMaxDateSigNorm, this, IfActor::minMaxDateSlNorm);
		IfActor::connectX(it->second, IfActor::refDateSigNorm, this, IfActor::refDateSlNorm);
	}
}

void DWidFragments::setup()
{
	ui->treeFrags->resizeColumnsToContents();
	ui->treeFrags->resizeRowsToContents();
	if(ui->treeFrags->rowCount()>0)
	{
		ui->treeFrags->selectionModel()->select(ui->treeFrags->model()->index(0,0), QItemSelectionModel::Select);
		ui->treeFrags->setCurrentCell(0,0);
		ui->dateRef->setDate(DCON.getStoryStart().date());
	}
}

Fragment *DWidFragments::tabRow2Frag(int row)
{
	int pk=tabRow2pk(row);
	if(pk<=0)
	{
		qCritical()<<"tabRow2Frag no pk"<<row;
		return nullptr;
	}
	return DCON.getFragment(pk);
}

int DWidFragments::tabRow2pk(int row)
{
	int r=row;
	if(r<0)
	{
		//r=ui->treeFrags->currentRow();
		QModelIndexList mil=ui->treeFrags->selectionModel()->selectedRows();
		if(mil.size()<1)
		{
			qCritical()<<"tabRow2Frag no sel";
			return -1;
		}
		r=mil.at(0).row();
	}
	if(r<0)
	{
		qCritical()<<"tabRow2Frag no row";
		return -1;
	}
	int pk=ui->treeFrags->item(r, 0)->data(Qt::UserRole).toInt();
	return pk;
}

int DWidFragments::tabFrag2Row(int fragPk)
{
	if(fragPk<0)
		return -1; // simple answer
	for(int r=0, rm=ui->treeFrags->rowCount(); r<rm; ++r)
	{
		if(ui->treeFrags->item(r, 0)->data(Qt::UserRole).toInt()==fragPk)
			return r;
	}
	return -1;
}

QColor DWidFragments::cellBgColor(Fragment &frag)
{
	Function f=frag.getFunction();
	if(f==Function::FRAGMENT || f==Function::PROLOG || f==Function::EPILOG)
	{
		switch(frag.getMaturity())
		{
		case Maturity::DUMMY:
			return QColor(200, 220, 220);
		case Maturity::KEYWORDS:
			return QColor(220, 220, 255);
		case Maturity::CONTENT:
			return QColor(100, 160, 255);
		case Maturity::PARTTEXT:
			return QColor(255, 200, 200);
		case Maturity::RAWTEXT:
			return QColor(255, 255, 190);
		case Maturity::FULLTEXT:
			return QColor(200, 255, 200);
		}
	}
	else if(f==Function::CHAPTER)
		return QColor(Qt::white);
	else if(f==Function::EVENT || f==Function::COMMENT)
		return QColor(180, 180, 180);
	return QColor(255, 60, 60);
}

void DWidFragments::buttonState(int row, bool multiselect)
{
	ui->toolDelete->setEnabled(row>=0 && ui->treeFrags->rowCount()>1);
	ui->toolDown->setEnabled(!multiselect && lowerVisibleLine(row)>=0); // -1 if none
	ui->toolUp->setEnabled(!multiselect && upperVisibleLine(row)>=0); // -1 if none
	ui->toolEdit->setEnabled(!multiselect && row>=0);
	ui->toolFullEdit->setEnabled(!multiselect && row>=0);
}

QString DWidFragments::tDiffFormat(qint64 diffSec)
{
	int subday=diffSec%86400;
	diffSec/=86400;
	int ddiff=diffSec%365;
	int ydiff=diffSec/365;
	if(ydiff)
		return QStringLiteral("%1y%2d").arg(ydiff).arg(ddiff);
	else if(ddiff>3)
		return QStringLiteral("%1d").arg(ddiff);
	else if(ddiff)
		return QStringLiteral("%1d%2h").arg(ddiff).arg((int)(subday/3600));
	else if(subday>=3600)
		return QStringLiteral("%1h%2m").arg((int)(subday/3600)).arg((int)((subday%3600)/60));
	return QStringLiteral("%1m").arg((int)(subday/60));
}

void DWidFragments::fragmentSl(int pk, FragmentAction action, QVector<int> pkOther)
{
	Fragment * frag=nullptr;
	if(action!=FragmentAction::DELETED && action!=FragmentAction::ALLGONE)
	{
		frag=DCON.getFragment(pk);
		if(!frag)
		{
			qCritical()<<"DWidFragments::fragmentSl not a Fragment"<<pk;
			return;
		}
	}
	switch(action)
	{
	case FragmentAction::CREATED:
	{
		freezeFragSigs=true;
		QColor bc=cellBgColor(*frag);
		int row=ui->treeFrags->rowCount()-1;
		for(; row>=0; --row)
		{
			QTableWidgetItem *wdg=ui->treeFrags->item(row, 0);
			if(!wdg)
			{
				qDebug()<<"hae?";
				continue;
			}
			int seq=wdg->data(Qt::UserRole+1).toInt();
			if(seq<frag->getSequence())
				break;
		}
		++row; // here we want to insert
		ui->treeFrags->setRowCount(ui->treeFrags->rowCount()+1);
		QTableWidgetItem *twi;
		for(int ir=ui->treeFrags->rowCount()-2; ir>=row; --ir)
		{
			for(int ic=0; ic<ui->treeFrags->columnCount(); ++ic)
			{
				twi=ui->treeFrags->takeItem(ir, ic);
				ui->treeFrags->setItem(ir+1, ic, twi);
			}
		}
		const QFont & fo=( (frag->getFunction()==Function::CHAPTER)?boldFont:\
							((frag->getFunction()==Function::PROLOG || frag->getFunction()==Function::EPILOG)?itFont:\
							normFont) );
		Function fun=frag->getFunction();
		twi=new QTableWidgetItem(frag->getTitle());
		twi->setData(Qt::UserRole, frag->getPk());
		twi->setData(Qt::UserRole+1, frag->getSequence());
		twi->setBackground(bc);
		twi->setFont(fo);
		ui->treeFrags->setItem(row, 0, twi);
		if(fun==Function::FRAGMENT || fun==Function::EVENT)
		{
			twi=new QTableWidgetItem(frag->getStart().toString(UI_DATETIME_FORMAT));
			twi->setBackground(bc);
			twi->setFont(fo);
			ui->treeFrags->setItem(row, 1, twi);
			twi=new QTableWidgetItem(tDiffFormat(abs(ui->dateRef->dateTime().secsTo(frag->getStart()))));
			twi->setBackground(bc);
			twi->setFont(fo);
			ui->treeFrags->setItem(row, 2, twi);
		}
		twi=new QTableWidgetItem(frag->getLocation());
		twi->setBackground(bc);
		twi->setFont(fo);
		ui->treeFrags->setItem(row, 3, twi);
		if(fun==Function::FRAGMENT || fun==Function::PROLOG || fun==Function::EPILOG || fun==Function::COMMENT)
		{
			twi=new QTableWidgetItem(frag->getFilename());
			twi->setBackground(bc);
			twi->setFont(fo);
			ui->treeFrags->setItem(row, 4, twi);
		}
		twi=new QTableWidgetItem(frag->getComment());
		twi->setBackground(bc);
		twi->setFont(fo);
		ui->treeFrags->setItem(row, 5, twi);
		check_EventsComments();
		break;
	}
	case FragmentAction::DELETED:
	{
		freezeFragSigs=true;
		int row=tabFrag2Row(pk);
		if(row>=0)
		{
			//qDebug()<<"FragmentAction::DELETED rows before"<<ui->treeFrags->rowCount();
			ui->treeFrags->removeRow(row);
			//qDebug()<<"FragmentAction::DELETED rows after"<<ui->treeFrags->rowCount();
		}
		break;
	}
	case FragmentAction::CHANGED:
	{
		freezeFragSigs=true;
		QColor bc=cellBgColor(*frag);
		int row=tabFrag2Row(pk);
		const QFont & fo=( (frag->getFunction()==Function::CHAPTER)?boldFont:\
							((frag->getFunction()==Function::PROLOG || frag->getFunction()==Function::EPILOG)?itFont:\
							normFont) );
		Function fun=frag->getFunction();
		QTableWidgetItem *twi=ui->treeFrags->item(row, 0); // title
		twi->setText(frag->getTitle());
		twi->setBackground(bc);
		twi->setFont(fo);
		if(fun==Function::FRAGMENT || fun==Function::EVENT)
		{
			twi=ui->treeFrags->item(row, 1); // start
			twi->setText(frag->getStart().toString(UI_DATETIME_FORMAT));
			twi->setBackground(bc);
			twi->setFont(fo);
			twi=ui->treeFrags->item(row, 2); // t-diff
			twi->setText(tDiffFormat(abs(ui->dateRef->dateTime().secsTo(frag->getStart()))));
			twi->setBackground(bc);
			twi->setFont(fo);
		}
		twi=ui->treeFrags->item(row, 3); // location
		twi->setText(frag->getLocation());
		twi->setBackground(bc);
		twi->setFont(fo);
		if(fun==Function::FRAGMENT || fun==Function::PROLOG || fun==Function::EPILOG || fun==Function::COMMENT)
		{
			twi=ui->treeFrags->item(row, 4); // file
			twi->setText(frag->getFilename());
			twi->setBackground(bc);
			twi->setFont(fo);
		}
		twi=ui->treeFrags->item(row, 5); // comment
		twi->setText(frag->getComment());
		twi->setBackground(bc);
		twi->setFont(fo);
		check_EventsComments();
		break;
	}
	case FragmentAction::SEQCHANGED:
	{
		if(!pkOther.size())
			break; // nothing to do if effective sequence not changed
		int r1=tabFrag2Row(pk);
		int r2=tabFrag2Row(pkOther.at(0));
		for(int ic=0; ic<ui->treeFrags->columnCount(); ++ic)
		{
			QTableWidgetItem *twi1=ui->treeFrags->takeItem(r1, ic);
			QTableWidgetItem *twi2=ui->treeFrags->takeItem(r2, ic);
			ui->treeFrags->setItem(r1, ic, twi2);
			ui->treeFrags->setItem(r2, ic, twi1);
		}
		ui->treeFrags->selectionModel()->select(ui->treeFrags->model()->index(r1,0), QItemSelectionModel::ClearAndSelect);
		ui->treeFrags->setCurrentCell(r1,0);
		check_EventsComments();
		break;
	}
	case FragmentAction::ATTENTION:
	{
		freezeFragSigs=true;
		int r=tabFrag2Row(pk);
		if(r>=0)
		{
			ui->treeFrags->selectionModel()->select(ui->treeFrags->model()->index(r,0), QItemSelectionModel::ClearAndSelect | QItemSelectionModel::Rows);
			ui->treeFrags->setCurrentCell(r,0);
			foreach(int pkx, pkOther)
			{
				int rx=tabFrag2Row(pkx);
				ui->treeFrags->selectionModel()->select(ui->treeFrags->model()->index(rx,0), QItemSelectionModel::Select | QItemSelectionModel::Rows);
			}
			buttonState(r, pkOther.size());
		}
		break;
	}
	case FragmentAction::ALLGONE:
		freezeFragSigs=true;
		ui->treeFrags->clearContents();
		ui->treeFrags->setRowCount(0);
		break;
	}
	freezeFragSigs=false;
}

void DWidFragments::minMaxDateSl(const QDateTime &start, const QDateTime &end)
{
	ui->labelMinDate->setText(start.toString(UI_DATE_FORMAT));
	ui->labelMaxDate->setText(end.toString(UI_DATE_FORMAT));
}

void DWidFragments::refDateSl(const QDateTime &refDate)
{
	freezeFragSigs=true; // from IfActor -> don't re-emit
	ui->dateRef->setDateTime(refDate);
	freezeFragSigs=false;
}

void DWidFragments::on_treeFrags_itemDoubleClicked(QTableWidgetItem *item)
{
	Q_UNUSED(item)
	on_toolEdit_clicked();
}

void DWidFragments::on_treeFrags_currentItemChanged(QTableWidgetItem *current, QTableWidgetItem *previous)
{
	Q_UNUSED(previous)
	if(freezeFragSigs)
		return;
	int r=(current?current->row():-1);
	int pk=-1;
	QVector<int> ms;
	QModelIndexList mil=ui->treeFrags->selectionModel()->selectedRows();
	foreach(QModelIndex idx, mil)
	{
		int pkx=tabRow2pk(idx.row());
		if(idx.row()==r)
			pk=pkx;
		else
			ms<<pkx;
	}
	buttonState(r, ms.size());
	emit fragmentSig(pk, FragmentAction::ATTENTION, ms);
}

void DWidFragments::treeFrags_selectionChanged(const QItemSelection &selected, const QItemSelection &deselected)
{
	Q_UNUSED(selected)
	Q_UNUSED(deselected)
	if(freezeFragSigs)
		return;
	QModelIndexList mil=ui->treeFrags->selectionModel()->selectedRows();
	if(!mil.size())
	{
		buttonState(-1, false);
		emit fragmentSig(-1, FragmentAction::ATTENTION, QVector<int>());
		return;
	}
	int pk=-1;
	QVector<int> ms;
	foreach(QModelIndex idx, mil)
	{
		int pkx=tabRow2pk(idx.row());
		if(pk<0)
			pk=pkx;
		else
			ms<<pkx;
	}
	buttonState(mil.at(0).row(), ms.size());
	emit fragmentSig(pk, FragmentAction::ATTENTION, ms);
}

void DWidFragments::on_treeFrags_customContextMenuRequested(const QPoint &pos)
{
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QAction * act=menu->addAction(QIcon(":/res/icons8-clock-48.png"), tr("use as time reference"));
	connect(act, &QAction::triggered, this, &DWidFragments::timeRefSl);
	act=menu->addAction(QIcon(":/res/icons8-description-48.png"), tr("edit meta data"));
	connect(act, &QAction::triggered, this, &DWidFragments::on_toolEdit_clicked);
	act=menu->addAction(QIcon(":/res/icons8-edit-image-48.png"), tr("edit text"));
	connect(act, &QAction::triggered, this, &DWidFragments::on_toolFullEdit_clicked);
	menu->popup(ui->treeFrags->viewport()->mapToGlobal(pos));
}

void DWidFragments::on_toolNew_clicked()
{
	Fragment *refFrag=tabRow2Frag();
	if(!refFrag)
		return;
	DlgFragment dlg(*refFrag, false, this);
	dlg.exec(); // table update through IfActor signal, sequence shifting in data container
}

void DWidFragments::on_toolUp_clicked()
{
	int r=ui->treeFrags->currentRow();
	int r2=upperVisibleLine(r);
	if(r2<0)
	{
		qCritical()<<"DWidFragments::on_toolUp_clicked with no visible line above";
		return;
	}
	Fragment *f1=tabRow2Frag(r);
	Fragment *f2=tabRow2Frag(r2);
	if(!f1 || !f2)
		return;
	if(f1->getFunction()==Function::EPILOG &&
			(f2->getFunction()==Function::CHAPTER || f2->getFunction()==Function::FRAGMENT || f2->getFunction()==Function::PROLOG))
	{
		QApplication::beep();
		QMessageBox::warning(this, tr("Invalid Move"), tr("The epilog has to be the last printable fragment in the book!"));
		return;
	}
	if(f2->getFunction()==Function::PROLOG &&
			(f1->getFunction()==Function::CHAPTER || f1->getFunction()==Function::FRAGMENT || f1->getFunction()==Function::EPILOG))
	{
		QApplication::beep();
		QMessageBox::warning(this, tr("Invalid Move"), tr("The prolog has to be the first printable fragment in the book!"));
		return;
	}
	check_EventsComments(true);
	DCON.fragSwapSequ(f1->getPk(), f2->getPk());
	ui->treeFrags->selectionModel()->select(ui->treeFrags->model()->index(r2,0), QItemSelectionModel::ClearAndSelect);
	ui->treeFrags->setCurrentCell(r2,0);
	check_EventsComments();
}

void DWidFragments::on_toolDown_clicked()
{
	int r=ui->treeFrags->currentRow();
	int r2=lowerVisibleLine(r);
	if(r2<0)
	{
		qCritical()<<"DWidFragments::on_toolDown_clicked with no visible line below";
		return;
	}
	Fragment *f1=tabRow2Frag(r);
	Fragment *f2=tabRow2Frag(r2);
	if(!f1 || !f2)
		return;
	if(f1->getFunction()==Function::PROLOG &&
			(f2->getFunction()==Function::CHAPTER || f2->getFunction()==Function::FRAGMENT || f2->getFunction()==Function::PROLOG))
	{
		QApplication::beep();
		QMessageBox::warning(this, tr("Invalid Move"), tr("The prolog has to be the first printable fragment in the book!"));
		return;
	}
	if(f2->getFunction()==Function::EPILOG &&
			(f1->getFunction()==Function::CHAPTER || f1->getFunction()==Function::FRAGMENT || f1->getFunction()==Function::EPILOG))
	{
		QApplication::beep();
		QMessageBox::warning(this, tr("Invalid Move"), tr("The epilog has to be the last printable fragment in the book!"));
		return;
	}
	check_EventsComments(true);
	DCON.fragSwapSequ(f1->getPk(), f2->getPk());
	ui->treeFrags->selectionModel()->select(ui->treeFrags->model()->index(r2,0), QItemSelectionModel::ClearAndSelect);
	ui->treeFrags->setCurrentCell(r2,0);
	check_EventsComments();
}

void DWidFragments::on_toolEdit_clicked()
{
	Fragment *f=tabRow2Frag();
	if(!f)
		return;
	DlgFragment dlg(*f, true, this);
	dlg.exec();
}

void DWidFragments::on_toolFullEdit_clicked()
{
	Fragment *f=tabRow2Frag();
	if(!f)
		return;
	f->editFile();
}

void DWidFragments::on_toolDelete_clicked()
{
	Fragment *f=tabRow2Frag();
	if(!f)
		return;
	if(QMessageBox::question(this, tr("Delete Fragment"), tr("You are about to delete fragemnt %1 '%2'. Really?").arg(f->getFilename(), f->getTitle()), QMessageBox::Yes | QMessageBox::No, QMessageBox::No)!=QMessageBox::Yes)
		return;
	DCON.deleteFragment(f, true);
}

void DWidFragments::on_dateRef_dateTimeChanged(const QDateTime &datetime)
{
	if(!freezeFragSigs)
		emit refDateSig(datetime);
	for(int r=0, rm=ui->treeFrags->rowCount(); r<rm; ++r)
	{
		Fragment *f=tabRow2Frag(r);
		if(!f)
			continue;
		QTableWidgetItem *twi=ui->treeFrags->item(r, 2); // t-diff
		if(twi)
			twi->setText(tDiffFormat(abs(datetime.secsTo(f->getStart()))));
	}
}

void DWidFragments::on_checkEvents_stateChanged(int state)
{
	bool hide=(state==Qt::Unchecked);
	ui->treeFrags->selectionModel()->select(QModelIndex(), QItemSelectionModel::Clear);
	for(int r=0, rm=ui->treeFrags->rowCount(); r<rm; ++r)
	{
		Fragment *f=tabRow2Frag(r);
		if(!f)
			continue;
		if(f->getFunction()==Function::EVENT)
		{
			if(hide)
				ui->treeFrags->hideRow(r);
			else
				ui->treeFrags->showRow(r);
		}
	}
	buttonState(-1, false);
}

void DWidFragments::on_checkComments_stateChanged(int state)
{
	bool hide=(state==Qt::Unchecked);
	ui->treeFrags->selectionModel()->select(QModelIndex(), QItemSelectionModel::Clear);
	for(int r=0, rm=ui->treeFrags->rowCount(); r<rm; ++r)
	{
		Fragment *f=tabRow2Frag(r);
		if(!f)
			continue;
		if(f->getFunction()==Function::COMMENT)
		{
			if(hide)
				ui->treeFrags->hideRow(r);
			else
				ui->treeFrags->showRow(r);
		}
	}
	buttonState(-1, false);
}

void DWidFragments::check_EventsComments(bool allShown)
{
	bool ehide=allShown?false:(ui->checkEvents->checkState()==Qt::Unchecked);
	bool chide=allShown?false:(ui->checkComments->checkState()==Qt::Unchecked);
	for(int r=0, rm=ui->treeFrags->rowCount(); r<rm; ++r)
	{
		Fragment *f=tabRow2Frag(r);
		if(!f)
			continue;
		if(f->getFunction()==Function::EVENT)
		{
			if(ehide)
				ui->treeFrags->hideRow(r);
			else
				ui->treeFrags->showRow(r);
		}
		else if(f->getFunction()==Function::COMMENT)
		{
			if(chide)
				ui->treeFrags->hideRow(r);
			else
				ui->treeFrags->showRow(r);
		}
	}
}

void DWidFragments::timeRefSl()
{
	Fragment *f=tabRow2Frag();
	if(f)
	{
		ui->dateRef->setDateTime(f->getStart());
	}
}

int DWidFragments::upperVisibleLine(int currentLine)
{
	if(currentLine<=0 || currentLine>=ui->treeFrags->rowCount())
		return -1;
	int res=currentLine-1;
	while(res>=0 && ui->treeFrags->isRowHidden(res))
		--res;
	return res;
}

int DWidFragments::lowerVisibleLine(int currentLine)
{
	if(currentLine<0 || currentLine>=ui->treeFrags->rowCount()-1)
		return -1;
	int res=currentLine+1;
	while(ui->treeFrags->isRowHidden(res))
	{
		if(++res>=ui->treeFrags->rowCount())
			return -1;
	}
	return res;
}

