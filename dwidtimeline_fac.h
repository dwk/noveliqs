#ifndef DWIDTIMELINE_FAC_H
#define DWIDTIMELINE_FAC_H

#include "dockwidfactory.h"

namespace
{
QWidget * DockCreator(QWidget * parent)
{
	return new DWidTimeline(parent);
}
const char *techname="DWidTimeline";
bool dockWidRegistered=DockWidFactory::get().registerDockWid("Timeline", techname, Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea | Qt::BottomDockWidgetArea | Qt::TopDockWidgetArea, Qt::BottomDockWidgetArea, DockCreator);
}
#endif // DWIDFRAGMENTS_FAC_H
