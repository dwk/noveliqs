#include "ifactor.h"
#include "nvli.h"

QByteArray IfActor::fragmentSigNorm=QMetaObject::normalizedSignature("fragmentSig(int, FragmentAction, QVector<int>)");
QByteArray IfActor::fragmentSlNorm=QMetaObject::normalizedSignature("fragmentSl(int, FragmentAction, QVector<int>)");

QByteArray IfActor::personSigNorm=QMetaObject::normalizedSignature("personSig(int, PersonAction, int)");
QByteArray IfActor::personSlNorm=QMetaObject::normalizedSignature("personSl(int, PersonAction, int)");

QByteArray IfActor::attendSigNorm=QMetaObject::normalizedSignature("attendSig(int, AttendAction, int, int)");
QByteArray IfActor::attendSlNorm=QMetaObject::normalizedSignature("attendSl(int, AttendAction, int, int)");

QByteArray IfActor::minMaxDateSigNorm=QMetaObject::normalizedSignature("minMaxDateSig(const QDateTime &, const QDateTime &)");
QByteArray IfActor::minMaxDateSlNorm=QMetaObject::normalizedSignature("minMaxDateSl(const QDateTime &, const QDateTime &)");

QByteArray IfActor::refDateSigNorm=QMetaObject::normalizedSignature("refDateSig(const QDateTime &)");
QByteArray IfActor::refDateSlNorm=QMetaObject::normalizedSignature("refDateSl(const QDateTime &)");

bool IfActor::connectX(QObject * sender, const QByteArray & signal, QObject * actor, const QByteArray &slot)
{
	if(!sender || !actor)
		return false;
	const QMetaObject* mo = sender->metaObject();
	int index=mo->indexOfSignal(signal);
	if(index<0)
		return false;
	qDebug()<<"    "<<sender->objectName()<<signal;
	QMetaMethod signalM = mo->method(index);
	int myIndex=actor->metaObject()->indexOfSlot(slot);
	if(myIndex<0)
	{
		qCritical()<<"    "<<actor->objectName()<<"NO LOCAL SLOT"<<slot;
		return false;
	}
	QMetaMethod slotM=actor->metaObject()->method(myIndex);
	return actor->connect(sender, signalM, actor, slotM);
}
