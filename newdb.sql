BEGIN TRANSACTION;
DROP TABLE IF EXISTS "persons";
CREATE TABLE IF NOT EXISTS "persons" (
	"pk"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"first"	TEXT,
	"middle"	TEXT,
	"last"	TEXT NOT NULL,
	"birth"	TEXT,
	"death"	TEXT,
	"role"	TEXT,
	"bio"	TEXT,
	"detail"	INTEGER NOT NULL,
	"editorhint"	TEXT
);
DROP TABLE IF EXISTS "settings";
CREATE TABLE IF NOT EXISTS "settings" (
	"key"	TEXT NOT NULL UNIQUE,
	"value"	TEXT,
	PRIMARY KEY("key")
);
DROP TABLE IF EXISTS "fragments";
CREATE TABLE IF NOT EXISTS "fragments" (
	"pk"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"sequence"	INTEGER NOT NULL,
	"file"	TEXT,
	"title"	TEXT,
	"time"	TEXT,
	"duration"	INTEGER,
	"location"	TEXT,
	"maturity"	INTEGER,
	"function"	INTEGER	NOT NULL,
	"comment"	TEXT,
	"editorhint"	TEXT
);
DROP TABLE IF EXISTS "attends";
CREATE TABLE "attends" (
	"pk"	INTEGER NOT NULL PRIMARY KEY AUTOINCREMENT UNIQUE,
	"fragment"	INTEGER NOT NULL,
	"person"	INTEGER NOT NULL,
	"relation"	INTEGER NOT NULL,
	FOREIGN KEY("fragment") REFERENCES "fragments"("pk") on delete cascade,
	FOREIGN KEY("person") REFERENCES "persons"("pk") on delete cascade
);
COMMIT;
BEGIN TRANSACTION;
INSERT INTO "settings" ("key","value") VALUES ('author','Dietmar W. Kieslinger');
INSERT INTO "settings" ("key","value") VALUES ('title','Title of Book');
INSERT INTO "settings" ("key","value") VALUES ('fragPath','./fragments');
INSERT INTO "settings" ("key","value") VALUES ('bioPath','./persons');
INSERT INTO "settings" ("key","value") VALUES ('generatorPath','./LaTeX');
INSERT INTO "settings" ("key","value") VALUES ('outPath','./book');
INSERT INTO "fragments" ("pk","sequence","file","title","time","duration","location","maturity","comment","editorhint") VALUES (1,1,NULL,'Title','2030-05-13T05:01:23',3600,NULL,1,'your first chapter','cy:-42');
COMMIT;
