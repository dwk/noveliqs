#include "editorperson.h"
#include <QPainter>
#include <QMenu>
#include <QGraphicsSceneContextMenuEvent>
#include "qmath.h"
#include "fragment.h"
#include "editorscene.h"
#include "dwidtimeline.h"
#include "dlgperson.h"
#include "nvli.h"

Q_LOGGING_CATEGORY(editorperson, "editorperson")

double EditorPerson::unknownBirthOffset=100.;

EditorPerson::EditorPerson(int persPk, QObject * parent) :
	QObject(parent), baseColor(60,80,120), backgroundBrush(Qt::gray),
	normalPen(baseColor), overPen(Qt::red), selPen(Qt::red), seloverPen(Qt::red)

{
	normalPen.setWidth(2.);
	overPen.setWidth(3.);
	selPen.setWidth(3.); // used when no birth date available
	seloverPen.setWidth(5.);
	p=DCON.getPerson(persPk);
	setFlags(ItemIsSelectable | ItemIsMovable | ItemSendsGeometryChanges);
    setAcceptHoverEvents(true);
	setZValue(1000.);
	if(!p)
	{
		qCritical()<<"EditorPerson no person"<<persPk;
		return;
	}
	el=new EditorLabel(p->getName(Person::NameStyle::SEMI_HUMAN), DCON.getMainDlg()->font(), EditorLabel::Alignment::VCENT, this);
	setupGraphic();
}

EditorPerson::~EditorPerson()
{
}


QRectF EditorPerson::boundingRect() const
{
	return boundingBox;
}

QPainterPath EditorPerson::shape() const
{
	QPen p;
	p.setWidth(5.);
	QPainterPathStroker pps(p);
	return pps.createStroke(lifeLine);
}

void EditorPerson::paint(QPainter *painter, const QStyleOptionGraphicsItem *option, QWidget *widget)
{
	Q_UNUSED(widget);
	if(!p)
		return;
	if(doSetup)
		setupGraphic();
	painter->save();
	painter->setPen((option->state & QStyle::State_Selected)?\
						((option->state & QStyle::State_MouseOver)?seloverPen:selPen):\
						((option->state & QStyle::State_MouseOver)?overPen:normalPen));
	painter->setBrush(QBrush()); // backgroundBrush); //
	painter->drawPath(lifeLine);
	if((option->state & QStyle::State_Selected) || (option->state & QStyle::State_MouseOver))
	{
		int size=4;
		if(option->state & QStyle::State_MouseOver)
		{
			painter->setPen(seloverPen);
			size=3;
		}
		else
		{
			painter->setPen(normalPen); //QPen(Qt::black)
		}
		painter->setPen((option->state & QStyle::State_MouseOver)?seloverPen:normalPen); //QPen(Qt::black)
		for(int i=0, im=assigns.size(); i<im; ++i)
			painter->drawEllipse(assigns.at(i), size, size);
	}
	painter->restore();
}

int EditorPerson::getPk() const
{
	if(p)
		return p->getPk();
	return -1;
}

void EditorPerson::setupGraphic()
{
	if(birthStar)
	{
		delete birthStar;
		birthStar=nullptr;
	}
	if(deathCross)
	{
		delete deathCross;
		deathCross=nullptr;
	}
	if(!p)
		return;
	EditorScene *es=qobject_cast<EditorScene*>(scene());
	if(!es)
	{
		//qCritical()<<"EditorPerson::setupGraphic no EditorScene";
		return;
	}
	//qDebug()<<"pers setup"<<p->getName(Person::NameStyle::SEMI_HUMAN);
	prepareGeometryChange();
	createLifeline(es);
	if(p->getBirth().isValid()) // && deathCross->getSymbolType()==EditorSymbol::Symbol::CROSS)
	{
		double visAge=(double)(p->getBirth().daysTo(getDWid()->coord2Date(deathCross->pos().x())))/365.25;
		if(visAge<.1)
			visAge=.1;
		if(visAge>100.)
			visAge=100.;
		QLinearGradient lg(QPointF(0., 0.), QPointF(1., 0.));
		lg.setColorAt(0., QColor(255, 200, 235));
		if(visAge>15.)
		{
			lg.setColorAt(15./visAge, QColor(255, 255, 180));
			if(visAge>30.)
			{
				lg.setColorAt(30./visAge, QColor(180, 250, 200));
				if(visAge>50.)
				{
					lg.setColorAt(50./visAge, QColor(120, 150, 255));
					if(visAge>70.)
					{
						lg.setColorAt(70./visAge, QColor(190, 190, 200));
						lg.setColorAt(1., QColor(140, 150, 170));
					}
					else
						lg.setColorAt(1., QColor(190, 190, 200)); // 70.
				}
				else
					lg.setColorAt(1., QColor(120, 150, 255)); // 50.
			}
			else
				lg.setColorAt(1., QColor(180, 250, 200)); // 30.
		}
		else
			lg.setColorAt(1., QColor(255, 255, 180)); // 15.
		lg.setCoordinateMode(QGradient::ObjectMode);
		overPen=QPen(QBrush(lg), 4.);
		selPen=QPen(QBrush(lg), 7.);
	}
	else
	{
		overPen=QPen(Qt::red);
		overPen.setWidth(3.);
		selPen=QPen(Qt::red);
		selPen.setWidth(3.);
	}
	boundingBox=lifeLine.boundingRect();
	boundingBox|=birthStar->boundingRect();
	boundingBox|=deathCross->boundingRect();
	boundingBox.adjust(-1., -1, 1., 1.);
	doSetup=false;
	//qDebug()<<boundingBox<<lifeLine;
}

void EditorPerson::fastRepos()
{
	if(!p || doSetup)
		return;
	EditorScene *es=qobject_cast<EditorScene*>(scene());
	if(!es)
	{
		//qCritical()<<"EditorPerson::setupGraphic no EditorScene";
		return;
	}
	//qDebug()<<"pers repos"<<p->getName(Person::NameStyle::SEMI_HUMAN);
	prepareGeometryChange();
	createLifeline(es);
/*	double refx=getDWid()->datetime2Coord(p->getBirth());
	double refy=p->getEditorhint("cy").toDouble();
	auto fs=DCON.getAttendFragPks(p->getPk());
	// sort chronologically
	map<double,QPointF> fss;
	for(auto it=fs.begin(); it!=fs.end(); ++it)
	{
		QGraphicsItem *gi=es->getFrag(*it);
		if(gi)
		{
			QPointF npnt=gi->pos(); //mapFromItem(gi, QPointF());
			fss.insert(map<double,QPointF>::value_type(npnt.x(), npnt));
		}
		else
			qCritical()<<"EditorPerson::setupGraphic NULL point for fragPk"<<*it;
	}
	if(isnan(refx)) // unknown birth date
	{
		if(fss.size()==0)
			refx=getDWid()->datetime2Coord(DCON.getStoryStart()); // start at story begin
		else
			refx=fss.begin()->first-unknownBirthOffset; // start some days before first appearance
	}
	//qDebug()<<"pr ref"<<refx<<refy;
	setPos(refx, refy);
	QPointF pnt(0., 0.);
	lifeLine.clear();
	assigns.clear();
	lifeLine.moveTo(pnt);
	for(auto it=fss.begin(); it!=fss.end(); ++it)
	{
		QPointF s(it->second.x()-refx, it->second.y()-refy);
		QPointF dist((s.x()-pnt.x())/3., 0.);
		lifeLine.cubicTo(pnt+dist, s-dist, s);
		assigns.push_back(s);
		pnt=s;
		//qDebug()<<"pr hop"<<s<<it->second;
	}
	double etc=getDWid()->datetime2Coord(p->getDeath());
	if(isnan(etc))
	{
		if(fss.size()==0)
			etc=getDWid()->datetime2Coord(DCON.getStoryEnd()); // person appears in no fragment - death at story end
		else
			etc=fss.rbegin()->first+unknownBirthOffset;
		//qDebug()<<"pers repos end"<<p->getName(Person::NameStyle::SEMI_HUMAN)<<etc<<fss.size()<<refx;
	}
	QPointF fin(etc-refx, 0.);
	QPointF distf((fin.x()-pnt.x())/3., 0.);
	//qDebug()<<"pr fin"<<fin<<distf;
	deathCross->setPos(fin);
	lifeLine.cubicTo(pnt+distf, fin-distf, fin); */
	boundingBox=lifeLine.boundingRect();
	boundingBox|=birthStar->boundingRect();
	boundingBox|=deathCross->boundingRect();
	boundingBox.adjust(-1., -1, 1., 1.);
}

void EditorPerson::setDecoVisbility(bool visible)
{
	if(el)
		el->setVisible(visible);
	if(birthStar && birthStar->getSymbolType()==EditorSymbol::Symbol::RING)
		birthStar->setVisible(visible);;
	if(deathCross && deathCross->getSymbolType()==EditorSymbol::Symbol::RING)
		deathCross->setVisible(visible);;
}

void EditorPerson::persChangeSl()
{
	//qDebug()<<(f?f->getPk():0)<<pk;
	if(!p)
		return;
	setupGraphic();
	el->setText(p->getName(Person::NameStyle::SEMI_HUMAN));
	update();
}

void EditorPerson::contextMenuEvent(QGraphicsSceneContextMenuEvent *event)
{
	if(!p)
		return;
	QMenu *menu=new QMenu;
	menu->setAttribute(Qt::WA_DeleteOnClose);
	QAction * act=menu->addAction(QIcon(":/res/star.png"), QStringLiteral("Edit"));
	connect(act, &QAction::triggered, this, &EditorPerson::editObjSl);
	act=menu->addAction(QIcon(":/res/cross_red.png"), tr("Delete"));
	act->setEnabled(false);
	// todo delete?
	//connect(act, &QAction::triggered, ce, [ce, this](){ce->deleteObjSl(id);});
	menu->popup(event->screenPos());
}

QVariant EditorPerson::itemChange(QGraphicsItem::GraphicsItemChange change, const QVariant &value)
{
	if(change==ItemPositionHasChanged)
	{
		wasMoved=true;
		//qDebug()<<"EditorPerson::itemChange"<<value.toPointF();
	}
	else if(change==ItemSelectedChange)
	{
		//qDebug()<<"EditorPerson::itemChange"<<change<<value.toBool()<<static_cast<QGraphicsItem*>(this);
		emit selectedSig(p?p->getPk():-1, value.toBool());
	}
	return QGraphicsItem::itemChange(change, value);
}

void EditorPerson::mouseDoubleClickEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj dblclk at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseDoubleClickEvent(event);
	if(event->button()==Qt::LeftButton)
		editObjSl();
}

void EditorPerson::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj press at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mousePressEvent(event);
	wasMoved=false;
}

void EditorPerson::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
	//qDebug()<<"obj release at"<<(int)event->button()<<event->scenePos();
	QGraphicsItem::mouseReleaseEvent(event);
	if(wasMoved)
	{
		wasMoved=false;
		if(!p)
			return;
		double ncy=scenePos().y();
		setPos(0, ncy);
		p->setEditorhint("cy", QString::number(ncy));
		p->commit();
	}
}

void EditorPerson::editObjSl()
{
	if(!p)
		return;
	DlgPerson dlg(*p, DCON.getMainDlg());
	dlg.exec();
}

DWidTimeline *EditorPerson::getDWid() const
{
	return qobject_cast<DWidTimeline *>(parent()->parent()); // EditorPerson -> EditorScene -> DWidTimeline
}

void EditorPerson::createLifeline(EditorScene *es)
{
	lifeLine.clear();
	assigns.clear();
	double refx=getDWid()->datetime2Coord(p->getBirth());
	double refy=p->getEditorhint("cy").toDouble();
	if(!birthStar)
	{
		if(isnan(refx))
			birthStar=new EditorSymbol(EditorSymbol::Symbol::RING, 5., this);
		else
			birthStar=new EditorSymbol(EditorSymbol::Symbol::STAR, 20., this);
	}
	setToolTip(p->getName(Person::NameStyle::SEMI_HUMAN));
	auto fs=DCON.getAttendFragPks(p->getPk());
	// sort chronologically
	map<double,QPointF> fss;
	for(auto it=fs.begin(); it!=fs.end(); ++it)
	{
		EditorFragment *gi=es->getFrag(*it); // is a QGraphicsItem
		if(gi)
		{
			QPointF npnt=gi->pos(); //mapFromItem(gi, QPointF());
			set<int> ps=DCON.getAttendPersPks(gi->getPk());
			int pos=-1, ipos=0;
			for(auto itp=ps.cbegin(); itp!=ps.cend(); ++itp, ++ipos)
			{
				if(*itp==p->getPk())
				{
					pos=ipos;
					break;
				}
			}
			qDebug()<<"EditorPerson::setupGraphic"<<p->getName(Person::NameStyle::SEMI_HUMAN)<<gi->getFrag()->getTitle()<<pos<<npnt;
			npnt.ry()+=gi->getRenderUnit()*pos;
			qDebug()<<npnt;
			fss.insert(map<double,QPointF>::value_type(npnt.x(), npnt));
		}
		else
			qCritical()<<"EditorPerson::setupGraphic NULL point for fragPk"<<*it;
	}
	if(fss.size()==0)
	{ // person appears in no fragment - the usual thing during startup
		// qWarning()<<"person appears in no fragment"<<p->getName(Person::NameStyle::SEMI_HUMAN);
		if(isnan(refx))
			refx=getDWid()->datetime2Coord(DCON.getStoryStart()); // start at story begin
	}
	if(isnan(refx))
		refx=fss.begin()->first-unknownBirthOffset; // unknown birth date - start some days before first appearance
	//qDebug()<<"ps ref"<<refx<<refy;
	setPos(refx, refy); // birth symbol and label are at 0,0 anyway
	QPointF pnt(0., 0.);
	lifeLine.moveTo(pnt);
	for(auto it=fss.begin(); it!=fss.end(); ++it)
	{
		QPointF s(it->second.x()-refx, it->second.y()-refy);
		QPointF dist((s.x()-pnt.x())/3., 0.);
		lifeLine.cubicTo(pnt+dist, s-dist, s);
		assigns.push_back(s);
		pnt=s;
		//qDebug()<<"ps hop"<<s<<dist;
	}
	double etc=getDWid()->datetime2Coord(p->getDeath());
	if(!deathCross)
	{
		if(isnan(etc))
			deathCross=new EditorSymbol(EditorSymbol::Symbol::RING, 5., this);
		else
			deathCross=new EditorSymbol(EditorSymbol::Symbol::CROSS, 10., this);
	}
	if(isnan(etc))
	{
		if(fss.size()==0)
			etc=getDWid()->datetime2Coord(DCON.getStoryEnd()); // person appears in no fragment - death at story end
		else
			etc=fss.rbegin()->first+unknownBirthOffset;
	}
	QPointF fin(etc-refx, 0.);
	QPointF distf((fin.x()-pnt.x())/3., 0.);
	//qDebug()<<"ps fin"<<fin<<distf;
	deathCross->setPos(fin);
	lifeLine.cubicTo(pnt+distf, fin-distf, fin);
}
